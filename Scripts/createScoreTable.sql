CREATE TABLE `score` (
  `sno` int(11) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) NOT NULL,
  `vignette` varchar(100) NOT NULL,
  `qno` varchar(100) NOT NULL,
  `timestamp` varchar(100) NOT NULL,
  `attempts` int(11) NOT NULL,
  `matrix` varchar(1000) NOT NULL,
  `gameid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='This table will store results into database.';
