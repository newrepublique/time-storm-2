-- Datebase selected is Timestorm
CREATE TABLE `userdetails` (
  `sno` int(11) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `user_details` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8 COMMENT='This table will store results into database.';

