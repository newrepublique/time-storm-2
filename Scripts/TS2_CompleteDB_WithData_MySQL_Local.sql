CREATE DATABASE  IF NOT EXISTS `timestorm2` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `timestorm2`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: timestorm2
-- ------------------------------------------------------
-- Server version	5.5.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `vignette5score`
--

DROP TABLE IF EXISTS `vignette5score`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vignette5score` (
  `sno` int(11) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) NOT NULL,
  `vignette` varchar(100) NOT NULL,
  `qno` varchar(100) NOT NULL,
  `timestamp` varchar(100) NOT NULL,
  `answer` varchar(1000) NOT NULL,
  `gameid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COMMENT='This table will store results into database.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vignette5score`
--

LOCK TABLES `vignette5score` WRITE;
/*!40000 ALTER TABLE `vignette5score` DISABLE KEYS */;
INSERT INTO `vignette5score` VALUES (1,32323234,'5','1','2014-2-20 18:20:14','1','TS2'),(2,15456,'5','1','2014-2-21 12:2:34','2','TS2'),(3,15456,'5','2','2014-2-21 12:2:36','2','TS2'),(4,15456,'5','3','2014-2-21 12:4:3','3','TS2'),(5,15456,'5','4','2014-2-21 12:4:13','1','TS2'),(6,15456,'5','5','2014-2-21 12:4:19','1','TS2'),(7,15456,'5','6','2014-2-21 12:4:25','3','TS2'),(8,15456,'5','7','2014-2-21 12:5:25','2','TS2'),(9,15456,'5','8','2014-2-21 12:5:34','1','TS2'),(10,15456,'5','9','2014-2-21 12:5:40','3','TS2'),(11,15456,'5','10','2014-2-21 12:6:55','2','TS2'),(12,15456,'5','11','2014-2-21 12:7:0','1','TS2'),(13,15456,'5','12','2014-2-21 12:7:5','2','TS2'),(14,154,'5','1','2014-2-21 12:17:19','2','TS2'),(15,154,'5','2','2014-2-21 12:17:21','2','TS2'),(16,154,'5','3','2014-2-21 12:34:37','2','TS2'),(17,154,'5','4','2014-2-21 12:34:39','3','TS2'),(18,154,'5','5','2014-2-21 12:34:41','2','TS2'),(19,154,'5','6','2014-2-21 12:34:42','1','TS2'),(20,154,'5','7','2014-2-21 12:49:26','1','TS2'),(21,154,'5','8','2014-2-21 12:49:28','2','TS2'),(22,154,'5','9','2014-2-21 12:49:29','1','TS2'),(23,656,'5','1','2014-2-21 14:17:35','1','TS2'),(24,656,'5','2','2014-2-21 14:17:37','2','TS2'),(25,656,'5','3','2014-2-21 14:19:2','2','TS2'),(26,656,'5','4','2014-2-21 14:19:3','2','TS2'),(27,656,'5','5','2014-2-21 14:19:5','2','TS2'),(28,656,'5','6','2014-2-21 14:19:7','3','TS2'),(29,656,'5','7','2014-2-21 14:31:17','2','TS2'),(30,656,'5','8','2014-2-21 14:31:19','2','TS2'),(31,656,'5','9','2014-2-21 14:31:21','1','TS2'),(32,656,'5','10','2014-2-21 14:38:33','2','TS2'),(33,656,'5','11','2014-2-21 14:38:34','1','TS2'),(34,656,'5','12','2014-2-21 14:38:35','2','TS2'),(35,989,'5','1','2014-2-21 14:48:20','2','TS2'),(36,989,'5','2','2014-2-21 14:48:23','3','TS2'),(37,989,'5','1','2014-2-21 15:31:50','2','TS2'),(38,989,'5','2','2014-2-21 15:31:54','2','TS2'),(39,154,'5','1','2014-2-28 10:54:0','2','TS2'),(40,154,'5','2','2014-2-28 10:54:2','2','TS2'),(41,154,'5','3','2014-2-28 10:55:24','2','TS2'),(42,154,'5','4','2014-2-28 10:55:25','1','TS2'),(43,154,'5','5','2014-2-28 10:55:28','2','TS2'),(44,154,'5','6','2014-2-28 10:55:37','2','TS2'),(45,154,'5','7','2014-2-28 11:3:26','2','TS2'),(46,154,'5','8','2014-2-28 11:3:30','2','TS2'),(47,154,'5','9','2014-2-28 11:3:31','2','TS2'),(48,154,'5','10','2014-2-28 11:5:54','2','TS2'),(49,154,'5','11','2014-2-28 11:5:56','2','TS2'),(50,154,'5','12','2014-2-28 11:5:59','2','TS2');
/*!40000 ALTER TABLE `vignette5score` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userdetails`
--

DROP TABLE IF EXISTS `userdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userdetails` (
  `sno` int(11) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `user_details` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userdetails`
--

LOCK TABLES `userdetails` WRITE;
/*!40000 ALTER TABLE `userdetails` DISABLE KEYS */;
INSERT INTO `userdetails` VALUES (1,1,'{\"vignette\":\"v4\",\"gender\":\"male\",\"phonetext\":\"&lt;li class=&quot;listIcon1&quot;&gt;You are in The Archive.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;Retrieving map from cache.&lt;/li&gt;\",\"mutesound\":\"false\",\"progress\":\"010\",\"scene\":\"scene2\",\"section\":\"scene1_2\"}'),(2,2,'{\"vignette\":\"v4\",\"gender\":\"male\",\"phonetext\":\"&lt;li class=&quot;listIcon1&quot;&gt;You are in The Archive.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;Retrieving map from cache.&lt;/li&gt;\",\"mutesound\":\"false\",\"progress\":\"020\",\"scene\":\"scene3\",\"section\":\"scene3_1\"}'),(3,32323234,'{\"vignette\":\"v5\",\"gender\":\"male\",\"phonetext\":\"&lt;li class=&quot;listIcon1&quot;&gt;You are in The Archive.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;Retrieving map from cache.&lt;/li&gt;\",\"mutesound\":\"false\",\"progress\":\"000\",\"scene\":\"scene1\",\"section\":\"scene1_1\"}'),(4,9960,'{\"vignette\":\"v5\",\"gender\":\"male\",\"phonetext\":\" \",\"mutesound\":\"null\",\"progress\":\"000\",\"scene\":\"scene1\",\"section\":\"scene8_3\"}'),(5,15456,'{\"vignette\":\"v5\",\"gender\":\"male\",\"phonetext\":\"&lt;li class=&quot;listIcon1&quot;&gt;You are in The Archive.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;Retrieving map from cache.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;You’ve found the Future Environment section.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;You’ve found the Civil Rights section.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;You’ve found The Franklin River Dam section.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;You’ve found The Mabo Decision section.&lt;/li&gt;\",\"mutesound\":\"false\",\"progress\":\"100\",\"scene\":\"scene5\",\"section\":\"scene5_3\"}'),(6,656,'{\"vignette\":\"v5\",\"gender\":\"male\",\"phonetext\":\"&lt;li class=&quot;listIcon1&quot;&gt;You are in The Archive.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;Retrieving map from cache.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;You’ve found the Future Environment section.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;You’ve found the Civil Rights section.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;You’ve found The Franklin River Dam section.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;You’ve found The Mabo Decision section.&lt;/li&gt;\",\"mutesound\":\"false\",\"progress\":\"100\",\"scene\":\"scene5\",\"section\":\"scene5_3\"}'),(7,2356,'{\"vignette\":\"v5\",\"gender\":\"male\",\"phonetext\":\" \",\"mutesound\":\"false\",\"progress\":\"000\",\"scene\":\"scene1\",\"section\":\"scene8_3\"}'),(8,154,'{\"vignette\":\"v5\",\"gender\":\"male\",\"phonetext\":\"&lt;li class=&quot;listIcon1&quot;&gt;You are in The Archive.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;Retrieving map from cache.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;You’ve found the Future Environment section.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;You’ve found the Civil Rights section.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;You’ve found The Franklin River Dam section.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;You’ve found The Mabo Decision section.&lt;/li&gt;\",\"mutesound\":\"false\",\"progress\":\"100\",\"scene\":\"scene5\",\"section\":\"scene5_3\",\"game\":\"TS2true\"}'),(9,132052,'{\"vignette\":\"v5\",\"gender\":\"male\",\"phonetext\":\" \",\"mutesound\":\"false\",\"progress\":\"000\",\"scene\":\"scene1\",\"section\":\"scene8_3\"}'),(10,2000,'{\"vignette\":\"v2\",\"timer\":\"44\",\"gender\":\"male\",\"phonetext\":\"&lt;li class=&quot;listIcon1&quot;&gt;You are in The Archive.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;Retrieving map from cache.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;You’ve found the Future Environment section.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;You’ve found the Civil Rights section.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;You’ve found The Franklin River Dam section.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;You’ve found The Mabo Decision section.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;You’re on the Freedom Ride bus.&lt;/li&gt;&lt;li class=&quot;listIcon1&quot;&gt;You’re in the restroom.&lt;/li&gt;\",\"mutesound\":\"false\",\"progress\":\"040\",\"scene\":\"scene3\",\"section\":\"scene3_2\",\"quotes\":\"&lt;li id=&quot;4_3&quot; class=&quot;four&quot;&gt;That the fruit and veg in country NSW tastes great. &lt;span&gt;Delete Quote&lt;/span&gt;&lt;/li&gt;&lt;li id=&quot;2_3&quot; class=&quot;two&quot;&gt;That there were no protests in Wellington. &lt;span&gt;Delete Quote&lt;/span&gt;&lt;/li&gt;\",\"saved\":\"true\"}');
/*!40000 ALTER TABLE `userdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `score`
--

DROP TABLE IF EXISTS `score`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `score` (
  `sno` int(11) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) NOT NULL,
  `vignette` varchar(100) NOT NULL,
  `qno` varchar(100) NOT NULL,
  `timestamp` varchar(100) NOT NULL,
  `attempts` int(11) NOT NULL,
  `matrix` varchar(1000) NOT NULL,
  `gameid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8 COMMENT='This table will store results into database.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `score`
--

LOCK TABLES `score` WRITE;
/*!40000 ALTER TABLE `score` DISABLE KEYS */;
INSERT INTO `score` VALUES (1,32323234,'5','1','2014-2-20 18:20:14',1,'[3220]','TS2'),(2,9960,'4','1','2014-2-21 11:13:33',1,'[3760]','TS2'),(3,9960,'4','2','2014-2-21 11:13:35',1,'[1217]','TS2'),(4,9960,'4','3','2014-2-21 11:13:37',1,'[686]','TS2'),(5,9960,'4','4','2014-2-21 11:16:3',3,'[2996:4134:4914]','TS2'),(6,9960,'4','5','2014-2-21 11:16:5',1,'[671]','TS2'),(7,9960,'4','6','2014-2-21 11:16:8',1,'[1950]','TS2'),(8,9960,'4','7','2014-2-21 11:20:54',1,'[3211]','TS2'),(9,9960,'4','8','2014-2-21 11:20:58',1,'[3447]','TS2'),(10,9960,'4','9','2014-2-21 11:21:2',1,'[3245]','TS2'),(11,9960,'3','1','2014-2-21 12:0:34',4,'[2106:3230:3744:4228]','TS2'),(12,9960,'3','2','2014-2-21 12:0:37',2,'[952:1841]','TS2'),(13,9960,'3','3','2014-2-21 12:0:41',4,'[2028:2527:2886:3183]','TS2'),(14,9960,'3','4','2014-2-21 12:0:43',3,'[733:1076:1404]','TS2'),(15,15456,'5','1','2014-2-21 12:2:34',1,'[10910]','TS2'),(16,15456,'5','2','2014-2-21 12:2:36',1,'[712]','TS2'),(17,15456,'5','3','2014-2-21 12:4:3',1,'[6629]','TS2'),(18,15456,'5','4','2014-2-21 12:4:13',1,'[8811]','TS2'),(19,15456,'5','5','2014-2-21 12:4:19',1,'[5891]','TS2'),(20,15456,'5','6','2014-2-21 12:4:25',1,'[4499]','TS2'),(21,15456,'5','7','2014-2-21 12:5:25',1,'[3678]','TS2'),(22,15456,'5','8','2014-2-21 12:5:34',1,'[7853]','TS2'),(23,15456,'5','9','2014-2-21 12:5:40',1,'[4734]','TS2'),(24,15456,'5','10','2014-2-21 12:6:55',1,'[9566]','TS2'),(25,15456,'5','11','2014-2-21 12:7:0',1,'[3507]','TS2'),(26,15456,'5','12','2014-2-21 12:7:5',1,'[4593]','TS2'),(27,9960,'4','1','2014-2-21 12:13:15',1,'[2262]','TS2'),(28,9960,'4','2','2014-2-21 12:13:17',1,'[655]','TS2'),(29,9960,'4','3','2014-2-21 12:13:19',1,'[639]','TS2'),(30,9960,'4','4','2014-2-21 12:15:20',4,'[3557:4431:4790:5148]','TS2'),(31,9960,'4','5','2014-2-21 12:15:23',3,'[499:1092:1560]','TS2'),(32,9960,'4','6','2014-2-21 12:15:25',1,'[827]','TS2'),(33,154,'5','1','2014-2-21 12:17:19',1,'[4667]','TS2'),(34,154,'5','2','2014-2-21 12:17:21',1,'[1410]','TS2'),(35,9960,'4','7','2014-2-21 12:19:59',1,'[10312]','TS2'),(36,9960,'4','8','2014-2-21 12:20:1',1,'[858]','TS2'),(37,9960,'4','9','2014-2-21 12:20:3',1,'[1419]','TS2'),(38,9960,'4','7','2014-2-21 12:22:31',2,'[1123:1934]','TS2'),(39,9960,'4','8','2014-2-21 12:22:32',1,'[640]','TS2'),(40,9960,'4','9','2014-2-21 12:22:34',1,'[936]','TS2'),(41,9960,'4','10','2014-2-21 12:22:36',1,'[453]','TS2'),(42,154,'5','3','2014-2-21 12:34:37',1,'[923]','TS2'),(43,154,'5','4','2014-2-21 12:34:39',1,'[899]','TS2'),(44,154,'5','5','2014-2-21 12:34:41',1,'[880]','TS2'),(45,154,'5','6','2014-2-21 12:34:42',1,'[278]','TS2'),(46,154,'5','7','2014-2-21 12:49:26',1,'[8772]','TS2'),(47,154,'5','8','2014-2-21 12:49:28',1,'[672]','TS2'),(48,154,'5','9','2014-2-21 12:49:29',1,'[558]','TS2'),(49,9960,'4','1','2014-2-21 13:8:6',1,'[4009]','TS2'),(50,9960,'4','2','2014-2-21 13:8:8',1,'[947]','TS2'),(51,9960,'4','3','2014-2-21 13:8:10',1,'[933]','TS2'),(52,9960,'4','4','2014-2-21 13:12:28',4,'[37437:38092:38612:39020]','TS2'),(53,9960,'4','5','2014-2-21 13:12:30',1,'[932]','TS2'),(54,9960,'4','6','2014-2-21 13:12:34',4,'[501:1261:1893:2725]','TS2'),(55,656,'5','1','2014-2-21 14:17:35',1,'[311364]','TS2'),(56,656,'5','2','2014-2-21 14:17:37',1,'[676]','TS2'),(57,656,'5','3','2014-2-21 14:19:2',1,'[2159]','TS2'),(58,656,'5','4','2014-2-21 14:19:3',1,'[444]','TS2'),(59,656,'5','5','2014-2-21 14:19:5',1,'[659]','TS2'),(60,656,'5','6','2014-2-21 14:19:7',1,'[990]','TS2'),(61,656,'5','7','2014-2-21 14:31:17',1,'[145693]','TS2'),(62,656,'5','8','2014-2-21 14:31:19',1,'[863]','TS2'),(63,656,'5','9','2014-2-21 14:31:21',1,'[1373]','TS2'),(64,656,'5','10','2014-2-21 14:38:33',1,'[367024]','TS2'),(65,656,'5','11','2014-2-21 14:38:34',1,'[199]','TS2'),(66,656,'5','12','2014-2-21 14:38:35',1,'[239]','TS2'),(67,9960,'4','4','2014-2-21 14:47:31',2,'[4401:5583]','TS2'),(68,9960,'4','5','2014-2-21 14:47:34',1,'[1531]','TS2'),(69,9960,'4','6','2014-2-21 14:47:37',3,'[1112:1936:2424]','TS2'),(70,989,'5','1','2014-2-21 14:48:20',1,'[328724]','TS2'),(71,989,'5','2','2014-2-21 14:48:23',1,'[1460]','TS2'),(72,9960,'4','7','2014-2-21 14:57:51',4,'[40816:41639:42255:42919]','TS2'),(73,9960,'4','8','2014-2-21 14:57:54',3,'[745:1545:1873]','TS2'),(74,9960,'4','9','2014-2-21 14:57:57',4,'[616:1040:1560:2099]','TS2'),(75,9960,'4','10','2014-2-21 14:57:58',1,'[522]','TS2'),(76,989,'5','1','2014-2-21 15:31:50',1,'[11839]','TS2'),(77,989,'5','2','2014-2-21 15:31:54',1,'[3284]','TS2'),(78,2356,'4','1','2014-2-21 18:5:39',1,'[5330]','TS2'),(79,2356,'4','2','2014-2-21 18:5:46',1,'[5641]','TS2'),(80,2356,'4','3','2014-2-21 18:5:48',1,'[839]','TS2'),(81,2356,'4','4','2014-2-21 18:7:59',1,'[1655]','TS2'),(82,2356,'4','5','2014-2-21 18:8:1',1,'[836]','TS2'),(83,2356,'4','6','2014-2-21 18:8:3',1,'[718]','TS2'),(84,2356,'4','7','2014-2-21 18:11:38',1,'[1525]','TS2'),(85,2356,'4','8','2014-2-21 18:11:40',1,'[699]','TS2'),(86,2356,'4','9','2014-2-21 18:11:41',1,'[670]','TS2'),(87,2356,'4','10','2014-2-21 18:11:45',2,'[2305:2832]','TS2'),(88,154,'5','1','2014-2-28 10:54:0',1,'[1459]','TS2'),(89,154,'5','2','2014-2-28 10:54:2',1,'[1004]','TS2'),(90,154,'5','3','2014-2-28 10:55:24',1,'[1535]','TS2'),(91,154,'5','4','2014-2-28 10:55:25',1,'[643]','TS2'),(92,154,'5','5','2014-2-28 10:55:28',1,'[1514]','TS2'),(93,154,'5','6','2014-2-28 10:55:37',1,'[7709]','TS2'),(94,154,'5','7','2014-2-28 11:3:26',1,'[2394]','TS2'),(95,154,'5','8','2014-2-28 11:3:30',1,'[2673]','TS2'),(96,154,'5','9','2014-2-28 11:3:31',1,'[792]','TS2'),(97,154,'5','10','2014-2-28 11:5:54',1,'[61274]','TS2'),(98,154,'5','11','2014-2-28 11:5:56',1,'[1190]','TS2'),(99,154,'5','12','2014-2-28 11:5:59',1,'[1257]','TS2'),(100,132052,'3','1','2014-2-28 12:28:39',2,'[74770:75612]','TS2'),(101,132052,'3','2','2014-2-28 12:28:43',1,'[2325]','TS2'),(102,132052,'3','3','2014-2-28 12:28:55',1,'[10790]','TS2'),(103,132052,'3','4','2014-2-28 12:28:57',2,'[1310:1684]','TS2'),(104,132052,'4','1','2014-2-28 12:46:45',1,'[3385]','TS2'),(105,132052,'4','2','2014-2-28 12:46:47',1,'[1545]','TS2'),(106,132052,'4','3','2014-2-28 12:46:54',2,'[4580:5578]','TS2'),(107,132052,'4','4','2014-2-28 12:52:54',2,'[77983:79122]','TS2'),(108,132052,'4','5','2014-2-28 12:52:57',1,'[1607]','TS2'),(109,132052,'4','6','2014-2-28 12:53:1',3,'[1092:2153:2621]','TS2'),(110,132052,'4','4','2014-2-28 12:54:13',1,'[2636]','TS2'),(111,132052,'4','5','2014-2-28 12:54:19',2,'[4899:5585]','TS2'),(112,132052,'4','6','2014-2-28 12:54:47',4,'[25194:25803:26193:27051]','TS2'),(113,132052,'4','4','2014-2-28 14:19:38',1,'[70688]','TS2'),(114,132052,'4','5','2014-2-28 14:26:2',2,'[382974:383676]','TS2'),(115,132052,'4','6','2014-2-28 14:26:6',2,'[1856:3120]','TS2'),(116,132052,'4','7','2014-2-28 14:34:10',4,'[136818:137661:138144:138612]','TS2'),(117,132052,'4','8','2014-2-28 14:34:16',2,'[3963:4633]','TS2'),(118,132052,'4','9','2014-2-28 14:34:18',1,'[1279]','TS2'),(119,132052,'4','10','2014-2-28 14:34:22',4,'[1217:1638:2216:2621]','TS2');
/*!40000 ALTER TABLE `score` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-20 18:40:17
