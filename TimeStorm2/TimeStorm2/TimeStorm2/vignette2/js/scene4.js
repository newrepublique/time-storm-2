﻿$(document).ready(function () {
    sessionStorage.setItem(userId + '_lastScene', 'scene4');    //save game

    $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG02_background_12.jpg)').stop(true, true).hide().fadeIn(1000);
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    

    setTimeout(function () {

        $('#TScompleted,#TSprogress').show();
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG02_phonemap_03.png)');
    }, delay4);

    setTimeout(function () {
        

        setTimeout(function () {

            updatePhoneTxt('You’re at the restroom.', 'listIcon1');
        }, delay4);

        setTimeout(function () {
            fadeloop('#objectHighlight02_100', 500, 500, 1000);
            
        }, 1000 + delay4);


        $('#objectHighlight02_100').click(function () {
            playSound('0');
            $('#cover').hide();


            setTimeout(function () {
                $('#subTitle02_102').fadeIn(250);
                playSound(41);

            }, 1000);

            setTimeout(function () {
                $('#subTitle02_102').fadeOut(250);
                
            }, 4000);

            setTimeout(function () {
                $('#subTitle02_103').fadeIn(250);
                playSound(42);
            }, 5000);

            setTimeout(function () {
                $('#subTitle02_103').fadeOut(250);
                
            }, 8000);

            setTimeout(function () {
                $('#subTitle02_104').fadeIn(250);
                playSound(43);

            }, 9000);

            setTimeout(function () {
                $('#subTitle02_104').fadeOut(250);
            }, 17000);

            setTimeout(function () {
                $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG02_phonemap_04_.png)');
                $('#phoneMap02_105 , .phoneMap02_105_dot').show();
                fadeloop('.phoneMap02_105_dot', 200, 200, 500);
            }, 18000);

        });



        $('.phoneMap02_105_dot').click(function () {
            playSound('0');
            $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG02_phonemap_04.png)');
            $('#phoneMap02_105 , .phoneMap02_105_dot').hide();

            setTimeout(function () {
                setGameStatus('.per', '070', 'v2');
            }, 1000);

            setTimeout(function () {
                $("#TScontainer").fadeOut(1000).css('background-image', '');
                $("#TScontainer, #Dummy").fadeOut();
            }, 2000);

            setTimeout(function () {
                $("#newContainer").load("scene5child.html");
            }, 3000);
        });

    }, delay4);

});

