﻿$(document).ready(function () {
    var currentObj;
    sessionStorage.setItem(userId + '_lastScene', 'scene3');    //save game
    if (localStorage.getItem(userId + '_lastSceneSection') == 'scene3_2') {
        $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG02_background_09.jpg)').fadeIn(1000);
    }
    else {
        $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG02_background_08.jpg)').fadeIn(1000);
    }
    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG02_phonemap_04.png)');
        $('#TScompleted,#TSprogress').show();
    }, 1000);

    setTimeout(function () {

        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {


            case 'scene3_2':
                scene3_2();

                if (localStorage.getItem(userId + '_isZero') == 'false' || localStorage.getItem(userId + '_isZero') == null) {

                    $('#list1,#list2,#list3,#list4').html(localStorage.getItem(userId + '_quotes'));
                    setTimer(timerTime);
                }
                else {
                    setTimer(570);
                }

                break;
            case 'scene3_3': scene3_3();
                break;
            default: scene3_1(); $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG02_background_08.jpg)'); setTimer(600);

        }


    }, 1000);


    $('.notepadAns li span').click(function (event) {
        playSound('0');
        quoteSelect(event.target.parentNode);
    });

    //Delete Quote..............................................................................................................................................................

    $(document).on('click','li span.delete',function (event) {
        playSound('0');
        //console.log($(this).parent().attr('class'));

        $(this).parent().removeClass('incorrect');
        var quote = $(this).parent().attr('class');
        console.log($('.quotelist .' + quote));
        $('.quotelist .' + quote).remove();

        collected_quotes = ($('.quotelist li').length) / 4;
        if (collected_quotes != 4) {
            $('.toDennis').hide();
        }
        if (dennisClicked && !$('.incorrect').length) {
            $('.toDennis').text('Collect Remaining quotes').show();
        }
    });

    //Show to dennis.................................................................................................................................................................

    var dennisClicked = false;
    $('.toDennis').click(function (event) {
        debugger;
        playSound('0');
        if ($(this).text() == 'Collect Remaining quotes') {
            $(this).parents('.propOverlay').fadeOut();
            $('#sceneOver').hide();
            setTimeout(function () {
                $('.toDennis').text('Show quotes to Dennis').show();
            }, 1000);
            dennisClicked = false;
        }
        else {

            dennisClicked = true;
            var idSelector = $('#' + event.target.parentElement.id).find('.quotelist').attr('id');
            if (!game_complete) {
                showQuotesDennis(idSelector);

            }
        }
    });

});


function skip() {
    $('#button').hide();
    for (var i = 0; i < timeouts.length; i++) {
        clearTimeout(timeouts[i]);
    }
    timeouts = [];

    $('.orangeText,.whiteText').hide();

    for (i = 0; i < sounds.length; i++) {
        sounds[i].pause();
        sounds[i].currenttime = 0;
    }
    var number = currentObj;
    switch (number) {


        case 1:
            $('#propOverlayImage02_064').fadeOut(1000);

            setTimeout(function () {
                $('#propOverlayImage02_068').fadeIn(1000);
            }, 500);
            break;

        case 2:
            $('#propOverlayImage02_071').fadeOut(1000);

            setTimeout(function () {
                $('#propOverlayImage02_075').fadeIn(1000);
            }, 500);
            break;

        case 3:
            $('#propOverlayImage02_078').fadeOut(1000);

            setTimeout(function () {
                $('#propOverlayImage02_082').fadeIn(1000);
            }, 500);
            break;

        case 4:
            $('#propOverlayImage02_085').fadeOut(1000);

            setTimeout(function () {
                $('#propOverlayImage02_090').fadeIn(1000);
            }, 500);
            break;

    }


}


var time = timerTime;
var intervalID;
function setTimer(timerTime) {

    localStorage.setItem(userId + '_isZero', false);
    var currenttime = [];

    intervalID = setInterval(function () {
        timerTime = timerTime - 1;
        time = timerTime;

        localStorage.setItem(userId + '_timer', timerTime);
        currenttime = currentTimerValues(timerTime);


        $('#TStimer').html(" 00:" + currenttime[0] + ":" + currenttime[1] + "").show();
        if (timerTime == 0 && !game_complete) {
            localStorage.setItem(userId + '_isZero', true);
            $('#TStimer').hide();
            clearInterval(intervalID);
            for (var i = 0; i < timeouts.length; i++) {
                clearTimeout(timeouts[i]);
            }
            timeouts = [];
            $('#cover1, #cover2, #cover3, #cover4').hide();
            $("#TScontainer, #Dummy").fadeOut();
            $('.orangeText,.whiteText,.propOverlay').hide();
            $('audio').prop('volume', 0);

            for (i = 0; i < sounds.length; i++) {
                sounds[i].pause();
            }
            setTimeout(function () {
                currenttime = [];

                $('#back').fadeIn(500);

            }, 1000);
            setTimeout(function () {
                $('#back').fadeOut(500);
                localStorage.setItem(userId + '_lastSceneSection', 'scene3_2');

                $('#newContainer').load("scene3child.html");

            }, 3000);


        }

    }, 1000);



}


function scene3_1() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene3_1');
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    $('#propClose').click(function () {
        playSound('0');
        $('#propOverlayImage02_059').hide();
    });

    setTimeout(function () {
        $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG02_background_08.jpg)').fadeOut(1500);

        setTimeout(function () {
            $("#Dummy").hide().css('background-image', 'url(./images/timestorm_G02_VG02_background_09.jpg)').fadeIn(2000);
        }, 0);
        setTimeout(function () {
            $('#Dummy').attr('id', 'TScontainer');
            $("#TScontainer").attr('id', 'Dummy');
        }, 2000);
    }, delay3);


    setTimeout(function () {

        playSound(54);

    }, 3000 + delay3);

    setTimeout(function () {
        $('#subTitle02_058').fadeIn(250);
        playSound(22);

    }, 16000 + delay3);

    setTimeout(function () {
        $('#subTitle02_058').fadeOut(250);
    }, 19000 + delay3);

    setTimeout(function () {
        $('#propOverlayImage02_059').fadeIn(250);
        $('#sceneOver').show();


    }, 20000 + delay3);
    setTimeout(function () {
        $('#subTitle02_059_1').fadeIn(250);

        playSound(23);
    }, 21000 + delay3);

    setTimeout(function () {
        $('#subTitle02_059_1').fadeOut(250);

    }, 29000 + delay3);

    setTimeout(function () {
        $('#propOverlayImage02_059').fadeOut(250);
        $('#sceneOver').hide();
    }, 30000 + delay3);

    setTimeout(function () {
        scene3_2();

    }, 30000 + delay3);

}

function scene3_2() {

    sessionStorage.setItem(userId + '_lastSceneSection', 'scene3_2');
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));

    setGameStatus('.per', '040', 'v2');

    setTimeout(function () {
        $('#cover1, #cover2, #cover3, #cover4').show();
        fadeloop('#objectHighlight02_062_01', 500, 500, 1000);
        fadeloop('#objectHighlight02_062_02', 500, 500, 1000);
        fadeloop('#objectHighlight02_062_03', 500, 500, 1000);
        fadeloop('#objectHighlight02_062_04', 500, 500, 1000);
        $('audio').prop('volume', 1);
    }, delay3);
    //Johno..............................................................

    $('#objectHighlight02_062_01').click(function () {
        $('#propOverlayImage02_064').fadeIn(250);
        $('#sceneOver').show();
        //$('#cover1').hide();
        playSound('0');
        currentObj = 1; $('#button').show();
        timeouts.push(setTimeout(function () {
            $('#subTitle02_065_1').fadeIn(250);
            playSound(24);
        }, delay3));

        timeouts.push(setTimeout(function () {

            $('#subTitle02_065_1').fadeOut(250);


        }, 12000 + delay3));


        timeouts.push(setTimeout(function () {
            $('#subTitle02_066_1').fadeIn(250);

            playSound(25);
        }, 13000 + delay3));

        timeouts.push(setTimeout(function () {
            $('#subTitle02_066_1').fadeOut(250);
            $('#subTitle02_066_2').fadeIn(250);

        }, 23000 + delay3));

        timeouts.push(setTimeout(function () {
            $('#subTitle02_066_2').fadeOut(250);


        }, 26000 + delay3));

        timeouts.push(setTimeout(function () {
            $('#subTitle02_067').fadeIn(250);

            playSound(26);
        }, 27000 + delay3));

        timeouts.push(setTimeout(function () {
            $('#subTitle02_067').fadeOut(250);
        }, 35000 + delay3));

        //note function starts
        timeouts.push(setTimeout(function () {
            $('#propOverlayImage02_064').fadeOut(1000);
            $('#button').hide();
            setTimeout(function () {
                $('#propOverlayImage02_068').fadeIn(1000);
            }, 500);

        }, 36000 + delay3));   //37000


    });

    //Charles......................................................

    $('#objectHighlight02_062_02').click(function () {
        $('#sceneOver').show();
        $('#propOverlayImage02_071').fadeIn(250);
        //$('#cover2').hide();
        playSound('0');

        currentObj = 2; $('#button').show();
        timeouts.push(setTimeout(function () {
            $('#subTitle02_072_1').fadeIn(250);

            playSound(29);
        }, delay3));

        timeouts.push(setTimeout(function () {
            $('#subTitle02_072_1').fadeOut(250);
            $('#subTitle02_072_2').fadeIn(250);

        }, 9000 + delay3));

        timeouts.push(setTimeout(function () {
            $('#subTitle02_072_2').fadeOut(250);


        }, 17000 + delay3));

        timeouts.push(setTimeout(function () {
            $('#subTitle02_073_1').fadeIn(250);

            playSound(30);
        }, 18000 + delay3));

        timeouts.push(setTimeout(function () {

            $('#subTitle02_073_1').fadeOut(250);


        }, 30000 + delay3));

        timeouts.push(setTimeout(function () {
            $('#subTitle02_074_1').fadeIn(250);

            playSound(31);
        }, 31000 + delay3));

        timeouts.push(setTimeout(function () {

            $('#subTitle02_074_1').fadeOut(250);
        }, 39000 + delay3));

        //note function starts
        timeouts.push(setTimeout(function () {
            $('#propOverlayImage02_071').fadeOut(1000);
            $('#button').hide();
            setTimeout(function () {
                $('#propOverlayImage02_075').fadeIn(1000);
            }, 500);

        }, 40000 + delay3)); //41000

    });

    //Phyliss.................................................................

    $('#objectHighlight02_062_03').click(function () {
        $('#sceneOver').show();
        $('#propOverlayImage02_078').fadeIn(250);
        //$('#cover3').hide();
        playSound('0');

        currentObj = 3; $('#button').show();
        timeouts.push(setTimeout(function () {
            $('#subTitle02_079_1').fadeIn(250);

            playSound(33);
        }, delay3));

        timeouts.push(setTimeout(function () {
            $('#subTitle02_079_1').fadeOut(250);
            $('#subTitle02_079_2').fadeIn(250);

        }, 15000 + delay3));

        timeouts.push(setTimeout(function () {
            $('#subTitle02_079_2').fadeOut(250);


        }, 19000 + delay3));

        timeouts.push(setTimeout(function () {
            $('#subTitle02_080_1').fadeIn(250);

            playSound(34);
        }, 20000 + delay3));

        timeouts.push(setTimeout(function () {

            $('#subTitle02_080_1').fadeOut(250);


        }, 37000 + delay3));

        timeouts.push(setTimeout(function () {
            $('#subTitle02_081_1').fadeIn(250);

            playSound(35);
        }, 38000 + delay3));

        timeouts.push(setTimeout(function () {

            $('#subTitle02_081_1').fadeOut(250);
        }, 52000 + delay3));

        timeouts.push(setTimeout(function () {
            $('#propOverlayImage02_078').fadeOut(1000);
            $('#button').hide();
            setTimeout(function () {
                $('#propOverlayImage02_082').fadeIn(1000);
            }, 500);
        }, 53000 + delay3));  //54000

    });

    //Sue........................................................................

    $('#objectHighlight02_062_04').click(function () {
        $('#sceneOver').show();
        $('#propOverlayImage02_085').fadeIn(250);
        //$('#cover4').hide();
        playSound('0');

        $('#button').show();
        currentObj = 4;

        timeouts.push(setTimeout(function () {
            $('#subTitle02_086').fadeIn(250);

            playSound(36);

        }, delay3));

        timeouts.push(setTimeout(function () {
            $('#subTitle02_086').fadeOut(250);

        }, 8000 + delay3));

        timeouts.push(setTimeout(function () {
            $('#subTitle02_087_1').fadeIn(250);

            playSound(37);
        }, 9000 + delay3));

        timeouts.push(setTimeout(function () {

            $('#subTitle02_087_1').fadeOut(250);


        }, 18000 + delay3));

        timeouts.push(setTimeout(function () {
            $('#subTitle02_088_1').fadeIn(250);

            playSound(38);
        }, 19000 + delay3));

        timeouts.push(setTimeout(function () {
            $('#subTitle02_088_1').fadeOut(250);
            $('#subTitle02_088_2').fadeIn(250);

        }, 28000 + delay3));

        timeouts.push(setTimeout(function () {
            $('#subTitle02_088_2').fadeOut(250);


        }, 35000 + delay3));

        timeouts.push(setTimeout(function () {
            $('#subTitle02_089_1').fadeIn(250);
            playSound(39);
        }, 36000 + delay3));

        timeouts.push(setTimeout(function () {

            $('#subTitle02_089_1').fadeOut(250);
        }, 46000 + delay3));

        timeouts.push(setTimeout(function () {
            $('#propOverlayImage02_085').fadeOut(1000);
            $('#button').hide();
            setTimeout(function () {
                $('#propOverlayImage02_090').fadeIn(1000);
            }, 500);

        }, 47000 + delay3)); //48000

    });

}



function check_collected(currentli) {

    collected_quotes = ($('.quotelist li').length) / 4;

    if (collected_quotes === 4) {

        //display link - show quotes to dennis...
        $('.toDennis').show();
    }
    else {

        $(currentli).parents('.notepadQue').parent().fadeOut(500);
        $('#sceneOver').hide();

    }
}


function quoteSelect(currentli) {

    //Add/Replace new Quote..............................................................................................................................................................


    if ($('.quotelist').children().length > 0) {                        //not first quote

        $('.quotelist li').each(function () {

            if ($(currentli).parent().attr('id') == $(this).attr('class')) {            //already exists

                $(this).remove();
            }
        });

        $('.quotelist').append('<li id=' + currentli.id + ' class=' + currentli.parentNode.id + '>' + $(currentli).children().html() + ' <span <span class="delete">Delete Quote</span></li>');
        check_collected(currentli);
    } else {
        //first quote...
        $('.quotelist').append('<li id=' + currentli.id + ' class=' + currentli.parentNode.id + '>' + $(currentli).children().html() + ' <span class="delete">Delete Quote</span></li>');

        check_collected(currentli);

    }


}

function showQuotesDennis(idSelector) {

    console.log('called');
    $('#' + idSelector + ' li').each(function () {
        total_correct = total_correct + 1;

        if (total_correct < 5 && !game_complete) {
            console.log('showquotes')
            console.log($(this).parent().parent())
            if (this.id == '1_2') {

                flag1 = true;

                $('#cover1').hide();
            }

            else if (this.id == '2_1') {
                flag2 = true;

                $('#cover2').hide();
            }

            else if (this.id == '3_4') {
                flag3 = true;

                $('#cover3').hide();
            }
            else if (this.id == '4_1') {
                flag4 = true;

                $('#cover4').hide();

            }

        }
        if (total_correct == 4) {

            check_quotes(this);

            return false;
        }

    });


}

function check_quotes(currentli) {


    if (flag1 && flag2 && flag3 && flag4) {
        $('.notepadQue').parent().fadeOut(500);
        //all correct quotes
        game_complete = true;
        $('#TStimer').hide();
        clearInterval(intervalID);
        $('#sceneOver').hide();
        $('#cover1,#cover2,#cover3,#cover4').hide();


        //PlaySoundWithDelay($divElementRenderAudioTags, 0, voiceover27_sound_file_url_mp3, voiceover27_sound_file_url_ogg, voiceover27_sound_file_url_wav);
        setTimeout(function () {
            $('#subTitle02_076_01').fadeIn(250);
            playSound(27);
        }, 1000);

        setTimeout(function () {
            $('#subTitle02_076_01').fadeOut(250);

            localStorage.setItem(userId + '_lastSceneSection', 'scene3_3');
            setTimeout(function () {
                scene3_3();
            }, 1000);
        }, 4000);



    }
    else {

        flag1 ? $('.quotelist .one span').remove() : $('.quotelist .one').addClass('incorrect').css('color', 'red');
        flag2 ? $('.quotelist .two span').remove() : $('.quotelist .two').addClass('incorrect').css('color', 'red');
        flag3 ? $('.quotelist .three span').remove() : $('.quotelist .three').addClass('incorrect').css('color', 'red');
        flag4 ? $('.quotelist .four span').remove() : $('.quotelist .four').addClass('incorrect').css('color', 'red');
        $('.toDennis').hide();
        timeouts.push(setTimeout(function () {
            $('#subTitle02_076_02').fadeIn(250);
            playSound(28);
        }, 1000));

        timeouts.push(setTimeout(function () {
            $('#subTitle02_076_02').fadeOut(250);



            total_correct = 0;
        }, 3000));


    }

}

function scene3_3() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene3_3');
    $("#TScontainer").hide().css('background-image', 'url(./images/timestorm_G02_VG02_background_08.jpg)').fadeIn(1000);

    setTimeout(function () {
        setGameStatus('.per', '050', 'v2');
        playSound(55);

    }, delay3);

    setTimeout(function () {
        $('#subTitle02_094_1').fadeIn(250);
        playSound(40);

    }, 7000 + delay3);

    setTimeout(function () {
        $('#subTitle02_094_1').fadeOut(250);

    }, 16000 + delay3);

    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG02_phonemap_03_.png)');
        $('#phoneMap02_095 , .phoneMap02_095_dot').show();

        fadeloop('.phoneMap02_095_dot', 200, 200, 500);
    }, 17000 + delay3);

    $('.phoneMap02_095_dot').click(function () {
        $('#phoneMap02_095 , .phoneMap02_095_dot').hide();
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG02_phonemap_03.png)');

        setTimeout(function () {
            setGameStatus('.per', '060', 'v2');

        }, delay3);

        setTimeout(function () {
            $("#TScontainer").fadeOut(1000).css('background-image', '');
            $("#TScontainer, #Dummy").fadeOut();
        }, 1000 + delay3);

        setTimeout(function () {
            $("#newContainer").load("scene4child.html");
        }, 2000 + delay3);
    });
}