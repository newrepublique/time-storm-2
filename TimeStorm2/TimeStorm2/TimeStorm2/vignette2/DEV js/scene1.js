﻿var currentdiv = '#TScontainer';
var nextdiv = '#Dummy';

$(document).ready(function () {

    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG02_background_01.jpg)').stop(true, true).hide().fadeIn(1000);
    sessionStorage.setItem(userId + '_lastScene', 'scene1');    //save game

    
    setTimeout(function () {
        
        $('#TScompleted,#TSprogress').fadeIn(1000);
    }, 1000 + delay1);

    setTimeout(function () {
        
        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {


            case 'scene1_2': scene1_2();
                break;

            default: scene1_1();

        }


    }, 3000);



    $('#objectHighlight02_016_2').click(function () {
        playSound('0');
        $('#cover2').hide();
        $('#sceneOver').fadeTo(500,0.1).show();
        
        setTimeout(function () {
            $('#subTitle02_018').fadeIn(250);
            playSound(6);
        }, delay1);

        setTimeout(function () {
            $('#subTitle02_018').fadeOut(250);
            $('#sceneOver').hide();
        }, 8000 + delay1);
    });

    $('#objectHighlight02_016_1').click(function () {
        playSound('0');
        $('#cover2').hide();
        $('#cover1').hide();
        setGameStatus('.per', '010', 'v2');

        setTimeout(function () {
            $("#TScontainer, #Dummy").fadeOut();
        }, 1000 + delay1);
        

        setTimeout(function () {
            $("#newContainer").load("scene2child.html");
        }, 2000 + delay1);
    });
});


function scene1_1() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene1_1');

    setTimeout(function () {
        $('#subTitle02_003').fadeIn(250);
        playSound(1);
       
    }, delay1);

    setTimeout(function () {
        $('#subTitle02_003').fadeOut(250);
        
    }, 2000 + delay1);


    setTimeout(function () {
        $('#subTitle02_004').fadeIn(250);
        playSound(2);
       
    }, 3000 + delay1);

    setTimeout(function () {
        $('#subTitle02_004').fadeOut(250);
        
    }, 9000 + delay1);


    setTimeout(function () {
        $('#subTitle02_005').fadeIn(250);
        playSound(47);
       
    }, 10000 + delay1);

    setTimeout(function () {
        $('#subTitle02_005').fadeOut(250);
        
    }, 12000 + delay1);


    setTimeout(function () {
        //playsound
        playSound(49);
        
    }, 13000 + delay1);

    setTimeout(function () {
        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG02_background_02.jpg)');

        setTimeout(function () {
            $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG02_background_01.jpg)');
        }, 2000);
        
    }, 13000 + delay1);

    setTimeout(function () {
        $('#subTitle02_008').fadeIn(250);
        
        playSound(3);
    }, 16000 + delay1);
    setTimeout(function () {
        $('#subTitle02_008').fadeOut(250);
    }, 22000 + delay1);


    setTimeout(function () {
        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG02_background_01.jpg)').fadeOut(1500);
        
        setTimeout(function () {
            $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG02_background_03.jpg)').fadeIn(2000);
        }, 0);
        setTimeout(function () {
            currentdiv = '#Dummy';
            nextdiv = '#TScontainer';
        }, 2000);
    }, 23000 + delay1);

    setTimeout(function () {
        
        playSound(50);
    }, 25000 + delay1);

    setTimeout(function () {
        scene1_2();

    }, 33000 + delay1);

}


function scene1_2() {
    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG02_background_03.jpg)');
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene1_2');
    setTimeout(function () {
        $('#subTitle02_011').fadeIn(250);
        
    }, delay1);
    setTimeout(function () {
        $('#subTitle02_011').fadeOut(250);
        
    }, 7000 + delay1);

    setTimeout(function () {
        $('#subTitle02_012').fadeIn(250);
        playSound(4);
       
    }, 8000 + delay1);
    setTimeout(function () {
        $('#subTitle02_012').fadeOut(250);
        
    }, 15000 + delay1);

    setTimeout(function () {
        $('#subTitle02_013').fadeIn(250);
        
        playSound(5);
    }, 16000 + delay1);
    setTimeout(function () {
        $('#subTitle02_013').fadeOut(250);
    }, 17000 + delay1);

    setTimeout(function () {
        $('#subTitle02_014').fadeIn(250);

    }, 18000 + delay1);
    setTimeout(function () {
        $('#subTitle02_014').fadeOut(250);
    }, 25000 + delay1);

    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG02_phonemap_01.png)');
    }, 26000 + delay1);


    setTimeout(function () {
        fadeloop('#objectHighlight02_016_1', 500, 500, 1000);
        fadeloop('#objectHighlight02_016_2', 500, 500, 1000);
    }, 27000 + delay1);


}