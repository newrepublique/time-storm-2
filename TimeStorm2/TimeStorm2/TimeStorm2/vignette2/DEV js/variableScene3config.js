﻿var gender = 'male';
if (localStorage.getItem(userId + '_gender') != null) {
    gender = localStorage.getItem(userId + '_gender');
}
var delay3 = 1000;
var game_complete = false;
var timerTime = 600;
if (localStorage.getItem(userId + '_timer') != null) {
    timerTime = localStorage.getItem(userId + '_timer');

}
var timeouts = [];
var exists = 0;
var collected_quotes = 0;
var flag1 = false, flag2 = false, flag3 = false, flag4 = false;
var total_correct = 0;

var $divElementRenderAudioTags = $('#soundForAllBrowsers');
var $divElementRenderAudioTags2 = $('#soundForAllBrowsers2');

var voiceover22_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_22.wav";
var voiceover22_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_22.mp3";
var voiceover22_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_22.ogg";

var voiceover23_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_23.wav";
var voiceover23_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_23.mp3";
var voiceover23_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_23.ogg";

var voiceover24_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_24.wav";
var voiceover24_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_24.mp3";
var voiceover24_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_24.ogg";

var voiceover25_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_25.wav";
var voiceover25_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_25.mp3";
var voiceover25_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_25.ogg";

var voiceover26_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_26.wav";
var voiceover26_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_26.mp3";
var voiceover26_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_26.ogg";

var voiceover27_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_27.wav";
var voiceover27_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_27.mp3";
var voiceover27_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_27.ogg";

var voiceover28_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_28.wav";
var voiceover28_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_28.mp3";
var voiceover28_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_28.ogg";

var voiceover29_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_29.wav";
var voiceover29_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_29.mp3";
var voiceover29_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_29.ogg";

var voiceover30_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_30.wav";
var voiceover30_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_30.mp3";
var voiceover30_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_30.ogg";

var voiceover31_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_31.wav";
var voiceover31_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_31.mp3";
var voiceover31_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_31.ogg";

var voiceover32_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_32.wav";
var voiceover32_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_32.mp3";
var voiceover32_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_32.ogg";

var voiceover33_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_33.wav";
var voiceover33_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_33.mp3";
var voiceover33_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_33.ogg";

var voiceover34_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_34.wav";
var voiceover34_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_34.mp3";
var voiceover34_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_34.ogg";

var voiceover35_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_35.wav";
var voiceover35_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_35.mp3";
var voiceover35_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_35.ogg";

var voiceover36_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_36.wav";
var voiceover36_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_36.mp3";
var voiceover36_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_36.ogg";

var voiceover37_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_37.wav";
var voiceover37_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_37.mp3";
var voiceover37_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_37.ogg";

var voiceover38_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_38.wav";
var voiceover38_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_38.mp3";
var voiceover38_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_38.ogg";

var voiceover39_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_39.wav";
var voiceover39_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_39.mp3";
var voiceover39_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_39.ogg";

var voiceover40_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_40.wav";
var voiceover40_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_40.mp3";
var voiceover40_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_40.ogg";

var chatting_sound_file_url_wav = "";//sounds/timestorm_G02_VG02_soundfx_07.wav";
var chatting_sound_file_url_mp3 = "sounds/timestorm_G02_VG02_soundfx_07.mp3";
var chatting_sound_file_url_ogg = "sounds/timestorm_G02_VG02_soundfx_07.ogg";


var flush_sound_file_url_wav = "";//sounds/timestorm_G02_VG02_soundfx_08.wav";
var flush_sound_file_url_mp3 = "sounds/timestorm_G02_VG02_soundfx_08.mp3";
var flush_sound_file_url_ogg = "sounds/timestorm_G02_VG02_soundfx_08.ogg";