﻿$(document).ready(function () {
    $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG02_background_08.jpg)').stop(true, true).hide().fadeIn(1000);
    sessionStorage.setItem(userId + '_lastScene', 'scene5');    //save game
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));

    setTimeout(function () {

        $('#TScompleted,#TSprogress').show();
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG02_phonemap_04.png)');
    }, 1000 + delay5);

    setTimeout(function () {

        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {


            case 'scene5_2': scene5_2();
                break;
            case 'scene5_3': postQuestion();
                break;
            default: scene5_1();

        }


    }, 1000 + delay5);

});

function scene5_1() {
   
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene5_1');
    setTimeout(function () {
        $('#subTitle02_108_1').fadeIn(250);
        playSound(44);

    }, delay5);

    setTimeout(function () {
       
        $('#subTitle02_108_1').fadeOut(250);

    }, 12000 + delay5);

    setTimeout(function () {
        showQuestion1();
    }, 13000 + delay5);
}


function showQuestion1() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene5_2');
    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q1radio2');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion02_109 , #QueAnsDiv1').fadeIn(250);
    $('#sceneOver').show();

    $('#QueAnsDiv1 ul li span').click(function (event) {

        
        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(1, result[0], result[1]);
                $('#QueAnsDiv1 ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion02_109 , #QueAnsDiv1').fadeOut(250);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion2();

                }, 1000);
            }

        }
    });
}


function showQuestion2() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q2radio3');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion02_112 , #QueAnsDiv2').fadeIn(250);
    $('#sceneOver').show();

    $('#QueAnsDiv2 ul li span').click(function (event) {

        
        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(2, result[0], result[1]);
                $('#QueAnsDiv2 ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion02_112 , #QueAnsDiv2').fadeOut(250);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion3();

                }, 1000);
            }

        }
    });
}

function showQuestion3() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q3radio1');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion02_115 , #QueAnsDiv3').fadeIn(250);
    $('#sceneOver').show();

    $('#QueAnsDiv3 ul li span').click(function (event) {

        
        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(3, result[0], result[1]);
                $('#QueAnsDiv3 ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion02_115 , #QueAnsDiv3').fadeOut(250);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion4();

                }, 1000);
            }

        }
    });
}

function showQuestion4() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q4radio4');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion02_118 , #QueAnsDiv4').fadeIn(250);
    $('#sceneOver').show();

    $('#QueAnsDiv4 ul li span').click(function (event) {

        
        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(4, result[0], result[1]);
                $('#QueAnsDiv4 ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion02_118 , #QueAnsDiv4').fadeOut(250);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion5();

                }, 1000);
            }

        }
    });
}

function showQuestion5() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q5radio4');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion02_121 , #QueAnsDiv5').fadeIn(250);
    $('#sceneOver').show();

    $('#QueAnsDiv5 ul li span').click(function (event) {

        
        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(5, result[0], result[1]);
                $('#QueAnsDiv5 ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion02_121 , #QueAnsDiv5').fadeOut(250);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion6();

                }, 1000);
            }

        }
    });
}

function showQuestion6() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q6radio1');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion02_124 , #QueAnsDiv6').fadeIn(250);
    $('#sceneOver').show();

    $('#QueAnsDiv6 ul li span').click(function (event) {

        
        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(6, result[0], result[1]);
                $('#QueAnsDiv6 ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion02_124 , #QueAnsDiv6').fadeOut(250);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion7();

                }, 1000);
            }

        }
    });
}

function showQuestion7() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q7radio3');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion02_127 , #QueAnsDiv7').fadeIn(250);
    $('#sceneOver').show();
  
    $('#QueAnsDiv7 ul li span').click(function (event) {

        
        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(7, result[0], result[1]);
                $('#QueAnsDiv7 ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion02_127 , #QueAnsDiv7').fadeOut(250);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion8();

                }, 1000);
            }

        }
    });
}
function showQuestion8() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q8radio2');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion02_130 , #QueAnsDiv8').fadeIn(250);
    $('#sceneOver').show();
   
    $('#QueAnsDiv8 ul li span').click(function (event) {

        
        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(8, result[0], result[1]);
                $('#QueAnsDiv8 ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion02_130 , #QueAnsDiv8').fadeOut(250);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion9();

                }, 1000);
            }

        }
    });
}

function showQuestion9() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q9radio4');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion02_133 , #QueAnsDiv9').fadeIn(250);
    $('#sceneOver').show();

    $('#QueAnsDiv9 ul li span').click(function (event) {

        
        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(9, result[0], result[1]);
                $('#QueAnsDiv9 ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion02_133 , #QueAnsDiv9').fadeOut(250);
                    $('#sceneOver').hide();
                    result = '';

                    postQuestion();

                }, 1000);
            }

        }
    });
}

function sendResults(quesNo, NumberOfAttemps, Matrix) {
    userid = getQueryStringValue("userId");


    qno = quesNo;

    currentTime = new Date();
    timestamp = formatDate(currentTime);

    attempts = NumberOfAttemps;
    matrix = Matrix;


    setTimeout(function () {
        sendScore(userid, vignette, qno, timestamp, attempts, matrix, gameid, handlerURL);
    }, 1000);


}

function postQuestion() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    setGameStatus('.per', '080', 'v2');
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene5_3');
    $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG02_background_11.jpg)');

    setTimeout(function () {
        fadeloop('#objectHighlight02_137', 500, 500, 1000);
        
    }, delay5);

    $('#objectHighlight02_137').click(function () {
        playSound('0');
        $('#cover').hide();

        setTimeout(function () {
            updatePhoneTxt('You now have the shilling coin.', 'listIcon1');
            //PlaySoundWithDelay($divElementRenderAudioTags, 0, flushes_sound_file_url_mp3, flushes_sound_file_url_ogg, flushes_sound_file_url_wav);
        }, 1000);

        setTimeout(function () {

            playSound(56);
            //PlayAnotherSound($divElementRenderAudioTags2, 0, voiceover45_sound_file_url_mp3, voiceover45_sound_file_url_ogg, voiceover45_sound_file_url_wav);       //Another sound
        }, 2000);

        setTimeout(function () {
            $('#subTitle02_141_1').fadeIn(250);
            playSound(45);
        }, 9000);

        setTimeout(function () {
            $('#subTitle02_141_1').fadeOut(250);
        }, 17000);

        setTimeout(function () {
            $('#subTitle02_141_2').fadeIn(250);

        }, 17000);

        setTimeout(function () {
            $('#subTitle02_141_2').fadeOut(250);
        }, 24000);

        setTimeout(function () {
            $('#subTitle02_142').fadeIn(250);

        }, 25000);

        setTimeout(function () {
            $('#subTitle02_142').fadeOut(250);
        }, 31000);

        setTimeout(function () {
            $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG02_phonemap_03_.png)');
            $('#phoneMap02_143 , .phoneMap02_143_dot').show();
            fadeloop('.phoneMap02_143_dot', 200, 200, 500);
        }, 32000);

    });

    $('.phoneMap02_143_dot').click(function () {

        playSound('0');
        $('#phoneMap02_143 , .phoneMap02_143_dot').hide();
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG02_phonemap_03.png)');

        setTimeout(function () {
        setGameStatus('.per', '090', 'v2');
        }, 1000);
       
        setTimeout(function () {
            $("#TScontainer, #Dummy").fadeOut();
            $("#TScontainer").fadeOut(1000).css('background-image', '');
        }, 2000);

        setTimeout(function () {
            $("#newContainer").load("scene6child.html");
        }, 3000);
        

    });
}