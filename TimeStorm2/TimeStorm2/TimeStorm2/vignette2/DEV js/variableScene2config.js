﻿var gender = 'male';
if (localStorage.getItem(userId + '_gender') != null) {
    gender = localStorage.getItem(userId + '_gender');
}

var $divElementRenderAudioTags = $('#soundForAllBrowsers');

var delay2 = 1000;
var voiceover07_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_07.wav";
var voiceover07_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_07.mp3";
var voiceover07_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_07.ogg";



var voiceover08_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_08.wav";
var voiceover08_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_08.mp3";
var voiceover08_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_08.ogg";

var voiceover09_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_09.wav";
var voiceover09_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_09.mp3";
var voiceover09_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_09.ogg";


var voiceover10_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_10.wav";
var voiceover10_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_10.mp3";
var voiceover10_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_10.ogg";

var voiceover11_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_11.wav";
var voiceover11_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_11.mp3";
var voiceover11_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_11.ogg";

var voiceover12_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_12.wav";
var voiceover12_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_12.mp3";
var voiceover12_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_12.ogg";

var voiceover13_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_13.wav";
var voiceover13_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_13.mp3";
var voiceover13_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_13.ogg";

var voiceover14_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_14.wav";
var voiceover14_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_14.mp3";
var voiceover14_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_14.ogg";

var voiceover15_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_15.wav";
var voiceover15_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_15.mp3";
var voiceover15_sound_file_url_ogg = "soundsvoiceovers//timestorm_G02_VG02_VO_15.ogg";

var voiceover16_sound_file_url_wav = "";//sounds/" + gender + "/timestorm_G02_VG02_VO_16.wav";
var voiceover16_sound_file_url_mp3 = "sounds/" + gender + "/timestorm_G02_VG02_VO_16.mp3";
var voiceover16_sound_file_url_ogg = "sounds/" + gender + "/timestorm_G02_VG02_VO_16.ogg";

var voiceover17_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_17.wav";
var voiceover17_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_17.mp3";
var voiceover17_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_17.ogg";

var voiceover18_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_18.wav";
var voiceover18_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_18.mp3";
var voiceover18_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_18.ogg";

var voiceover19_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_19.wav";
var voiceover19_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_19.mp3";
var voiceover19_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_19.ogg";

var voiceover20_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_20.wav";
var voiceover20_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_20.mp3";
var voiceover20_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_20.ogg";

var voiceover21_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_21.wav";
var voiceover21_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_21.mp3";
var voiceover21_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_21.ogg";

var engine_sound_file_url_wav = "";//sounds/timestorm_G02_VG02_soundfx_04.wav";
var engine_sound_file_url_mp3 = "sounds/timestorm_G02_VG02_soundfx_04.mp3";
var engine_sound_file_url_ogg = "sounds/timestorm_G02_VG02_soundfx_04.ogg";

var knocking_sound_file_url_wav = "";//sounds/timestorm_G02_VG02_soundfx_05.wav";
var knocking_sound_file_url_mp3 = "sounds/timestorm_G02_VG02_soundfx_05.mp3";
var knocking_sound_file_url_ogg = "sounds/timestorm_G02_VG02_soundfx_05.ogg";

var thunder_sound_file_url_wav = "";//sounds/timestorm_G02_VG02_soundfx_06.wav";
var thunder_sound_file_url_mp3 = "sounds/timestorm_G02_VG02_soundfx_06.mp3";
var thunder_sound_file_url_ogg = "sounds/timestorm_G02_VG02_soundfx_06.ogg";
