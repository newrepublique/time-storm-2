﻿//Javascript file for index page


var userId ;

var isiPad = false;
//var userId = getQueryStringValue("userId");
//localStorage.setItem('userId', userId);

sessionStorage.setItem('status-line-li','');
var scene = '';
var section = '';
var phonetextdata = '';
var gendervalue;
var mutesoundvalue;
var progressvalue;
var scene;
var section;
var currentscene = '';
var muteSound;
var currentvignette = '';
var timer;
var quotes = '';
var isSaved = false;
$(document).ready(function () {
    localStorage.setItem(userId + '_isSaved', false);
    $('#TSframe2').css('display', 'none');
    $('#pangoText a').css('display', 'none');
    $('#phoneText ul').css('display', 'none');
    //$('#clicksound').html("<audio id='audioControlClick'><source src='' type='audio/wav'><source src='sounds/timestorm_G02_VG01_click_01.mp3' type='audio/mpeg'><source src='sounds/timestorm_G02_VG01_click_01.ogg' type='audio/ogg'><embed height='50' width='100' hidden=true autostart=true loop=false src='sounds/timestorm_G02_VG01_click_01.mp3'></audio>");

    userId = localStorage.getItem('userId');

    if (userId == null) {
        if (navigator && navigator.platform && navigator.userAgent.match('MSIE')) {
            $('body').append('<div id="continueIE" class="stretchBG" style="width: 21%; height: 3.76%; font-size: 30px; position: fixed; z-index: 6; top: 44%; color: #f7dac2; cursor: pointer; text-align: center;left: 40%;border: 4px solid #f7dac2;">Click here to Continue</div>');
            $('#continueIE').click(function () {
                window.location.href = 'index.html';
            });

        }
        $('#Apple').hide();
        //login code..
        signIn('v2');
       // window.stop();
    } else {

        
        getPlatform();
        
       

        $('#Apple').click(function () {
            $('#Apple').hide();
            getResults();
            //initVignette();
        });

        var firstTime = true;
        $('#TSmute').click(function () {
            playSound('0');
            if (firstTime) {
                for (i = 0; i < sounds.length; i++) {
                    sounds[i].volume = 0;
                }
                sessionStorage.setItem('muteSound', true);
                firstTime = false;
            }
            else {
                setAudio(!(JSON.parse(sessionStorage.getItem('muteSound'))));
                sessionStorage.setItem('muteSound', !(JSON.parse(sessionStorage.getItem('muteSound'))));
            }
        });

        $('#TSsave').click(function () {
            playSound('0');
            localStorage.setItem(userId + '_isSaved', true);
            sendData();
        });
    }
});

function getPlatform() {


    macCSS();
    if (navigator && navigator.platform && navigator.userAgent.match(/iPad/i)) {
        isiPad = true;

    }
    if (!isiPad) {
        getResults();
        $('#Apple').hide();
        //initVignette();
    }
}


function sendData() {


    localStorage.setItem('userId', userId);
    localStorage.setItem(userId + '_gender', gender);
    
    localStorage.setItem(userId + '_mutesound', sessionStorage.getItem('muteSound'));
    localStorage.setItem(userId + '_progress', $('.per').html());
    localStorage.setItem(userId + '_lastScene', sessionStorage.getItem(userId + '_lastScene'));
    localStorage.setItem(userId + '_lastSceneSection', sessionStorage.getItem(userId + '_lastSceneSection'));
    localStorage.setItem(userId + '_vignette', currentvignette);

    localStorage.setItem(userId + '_quotes', $('#list1').html());

    gendervalue = localStorage.getItem(userId + '_gender');
    mutesoundvalue = sessionStorage.getItem('muteSound');
    localStorage.setItem(userId + '_phonetext', localStorage.getItem('phonetext'));
    phonetextdata = localStorage.getItem(userId + '_phonetext');
    isSaved = localStorage.getItem(userId + '_isSaved');

    if (localStorage.getItem(userId + '_phonetext') == null) {
        phonetextdata = '';

    }
    phonetextdata = htmlEscape(phonetextdata);

    progressvalue = $('.per').html();
    scene = sessionStorage.getItem(userId + '_lastScene');
    section = sessionStorage.getItem(userId + '_lastSceneSection');

    timer = localStorage.getItem(userId + '_timer');
    if (timer == null) {
        timer = 600;
    }

    quotes = $('#list1').html();
    if (quotes == undefined) {
        quotes = '';
    }
    quotes = $.trim(quotes);
    quotes = htmlEscape(quotes);

    var userdetails = {

        vignette: currentvignette,
        timer: timer,
        gender: gendervalue,
        phonetext: phonetextdata,
        mutesound: mutesoundvalue,
        progress: progressvalue,
        scene: scene,
        section: section,
        quotes: quotes,
        saved: isSaved

    }

    var details = JSON.stringify(userdetails);

    $.ajax({
        type: 'GET',
        url: '../Handlers/UserDetails.ashx',
        data: {
            userid: userId,
            details: details,
            action: "save"
        },
        context: this,
        dataType: 'text',
        contentType: "application/json; charset=utf-8",
        success: function (msg) {

        },
        error: function () {

        }
    });

}

function getResults() {

    gendervalue = localStorage.getItem(userId + '_gender');
    mutesoundvalue = localStorage.getItem(userId + '_mutesound');
    phonetextdata = localStorage.getItem(userId + '_phonetext');
    if (phonetextdata == 'null') {
        phonetextdata = null;
    }

    progressvalue = localStorage.getItem(userId + '_progress');
    scene = localStorage.getItem(userId + '_lastScene');
    section = localStorage.getItem(userId + '_lastSceneSection');
    currentvignette = localStorage.getItem(userId + '_vignette');
    timer = localStorage.getItem(userId + '_timer');

    quotes = localStorage.getItem(userId + '_quotes');

    isSaved = localStorage.getItem(userId + '_isSaved');

    if (gendervalue != null && mutesoundvalue != null && phonetextdata != null && progressvalue != null && scene != null && section != null && quotes != 'null' && quotes != undefined) {

        currentscene = scene;

        ///value to be set here....................................

        setGameStatus('.per', progressvalue, 'v1');
        phonetextdata = htmlUnescape(phonetextdata);
        $('#list').html(phonetextdata);
        sessionStorage.setItem('status-line-li', phonetextdata);
        //setAudio(mutesoundvalue);
        sessionStorage.setItem('muteSound', mutesoundvalue);
        localStorage.setItem(userId + '_timer', timer);
        localStorage.setItem(userId + '_quotes', quotes);
        initVignette();

    }
    else {

        $.ajax({
            type: 'GET',
            url: '../Handlers/UserDetails.ashx',
            data: {
                userid: userId,
                action: "get"

            },
            context: this,
            dataType: 'text',
            contentType: "application/json; charset=utf-8",
            success: function (detailsdata) {


                if (detailsdata != undefined && detailsdata != null && !jQuery.isEmptyObject(detailsdata)) {

                    var parsedData = JSON.parse(detailsdata);

                    localStorage.setItem(userId + '_gender', parsedData.gender);
                    localStorage.setItem(userId + '_phonetext', parsedData.phonetext);
                    localStorage.setItem(userId + '_mutesound', parsedData.mutesound);
                    localStorage.setItem(userId + '_progress', parsedData.progress);
                    localStorage.setItem(userId + '_lastScene', parsedData.scene);
                    currentscene = parsedData.scene;
                    localStorage.setItem(userId + '_lastSceneSection', parsedData.section);

                    localStorage.setItem(userId + '_vignette', parsedData.vignette);
                    currentvignette = parsedData.vignette;

                    localStorage.setItem(userId + '_timer', parsedData.timer);

                    setGameStatus('.per', parsedData.progress, 'v1');
                    if (parsedData.phonetext == 'null') {
                        parsedData.phonetext = '';
                    }
                    parsedData.phonetext = htmlUnescape(parsedData.phonetext);
                    $('#list').html(parsedData.phonetext);
                    sessionStorage.setItem('status-line-li', parsedData.phonetext);

                    //setAudio(parsedData.mutesound);
                    sessionStorage.setItem('muteSound', parsedData.mutesound);

                    parsedData.quotes = htmlUnescape(parsedData.quotes);
                    localStorage.setItem(userId + '_quotes', parsedData.quotes);

                    localStorage.setItem(userId + '_isSaved', parsedData.saved);
                    initVignette();

                } else {
                    initVignette();
                }

            },
            error: function () {

            }
        });

    }

}

function htmlEscape(str) {
    return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
}

function htmlUnescape(value) {
    return String(value)
        .replace(/&quot;/g, '"')
        .replace(/&#39;/g, "'")
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&amp;/g, '&');
}


var imgArray = Array(77);
var filesArray = Array(132);
var sounds = Array(59);
var sndArray = Array(59);

function preload() {
    var gender = 'male';
    if (localStorage.getItem(userId + '_gender') != null) {
        gender = localStorage.getItem(userId + '_gender');
    }
    document.getElementById('loading').style.display = 'block';

    var path = 'sounds/';
    var ext = '.mp3';
    if (BrowserDetect.browser == 'Safari') ext = '.mp3';
    if (BrowserDetect.browser == 'Firefox') ext = '.ogg';
    //filling in the sndArray
    sndArray = [
    path + 'timestorm_G02_VG01_click_01' + ext,
	path + 'voiceovers/timestorm_G02_VG02_VO_01' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_02' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_03' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_04' + ext,
    path + gender + '/timestorm_G02_VG02_VO_05' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_06' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_07' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_08' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_09' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_10' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_11' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_12' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_13' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_14' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_15' + ext,
    path + gender + '/timestorm_G02_VG02_VO_16' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_17' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_18' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_19' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_20' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_21' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_22' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_23' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_24' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_25' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_26' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_27' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_28' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_29' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_30' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_31' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_32' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_33' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_34' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_35' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_36' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_37' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_38' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_39' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_40' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_41' + ext,
    path + gender + '/timestorm_G02_VG02_VO_42' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_43' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_44' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_45' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_46' + ext,
    path + 'voiceovers/timestorm_G02_VG02_VO_47' + ext, //47
    path + 'timestorm_G02_VG02_soundfx_01' + ext,
    path + 'timestorm_G02_VG02_soundfx_02' + ext,   //49
    path + 'timestorm_G02_VG02_soundfx_03' + ext,
    path + 'timestorm_G02_VG02_soundfx_04' + ext,
    path + 'timestorm_G02_VG02_soundfx_05' + ext,
    path + 'timestorm_G02_VG02_soundfx_06' + ext,
    path + 'timestorm_G02_VG02_soundfx_07' + ext,
    path + 'timestorm_G02_VG02_soundfx_08' + ext,
    path + 'timestorm_G02_VG02_soundfx_09' + ext,
    path + 'timestorm_G02_VG02_soundfx_10' + ext,
    path + 'timestorm_G02_VG02_soundfx_11' + ext


    ];


    path = 'images/';
    imgArray = [
    path + 'greenDot.png',
    path + 'hand.png',
	path + 'icon.png',
	path + 'icon1.png',
	path + 'icon2.png',
	path + 'icon3.png',
	path + 'iconAns.png',
	path + 'loading.gif',
	path + 'mute-1.png',
	path + 'mute-2.png',
	path + 'save-1.png',
	path + 'save-2.png',
    path + 'save.png',
	path + 'scroll.png',
	path + 'scroll1.png',
	path + 'scroll2.png',
	path + 'scroll3.png',
	path + 'timer.png',
	path + 'timestorm_G02_VG01_answer_01.png',
    path + 'timestorm_G02_VG01_bulletpoint_01.png',
	path + 'timestorm_G02_VG01_questiontitle_01.png',
    path + 'timestorm_G02_VG01_speaker_01.png',
    path + 'timestorm_G02_VG02_timer_01.png',
	path + 'timestorm_G02_VG02_background_01.jpg',
	path + 'timestorm_G02_VG02_background_02.jpg',
    path + 'timestorm_G02_VG02_background_03.jpg',
    path + 'timestorm_G02_VG02_background_04.jpg',
    path + 'timestorm_G02_VG02_background_06.jpg',
    path + 'timestorm_G02_VG02_background_07.jpg',
    path + 'timestorm_G02_VG02_background_08.jpg',
    path + 'timestorm_G02_VG02_background_09.jpg',
    path + 'timestorm_G02_VG02_background_10.jpg',
    path + 'timestorm_G02_VG02_background_11.jpg',
    path + 'timestorm_G02_VG02_background_12.jpg',
    path + 'timestorm_G02_VG02_background_13.jpg',
    path + 'timestorm_G02_VG02_background_14.jpg',
    path + 'timestorm_G02_VG02_background_15.jpg',
    path + 'timestorm_G02_VG02_background_16.jpg',
    path + 'timestorm_G02_VG02_background_17.png',
    path + 'timestorm_G02_VG02_bulletpoint_01.png',
    path + 'timestorm_G02_VG02_blanknotepad_01.png',
	path + 'timestorm_G02_VG02_headsupdisplay_01.png',
    path + 'timestorm_G02_VG02_objecthighlight_01.png',
    path + 'timestorm_G02_VG02_objecthighlight_02.png',
    path + 'timestorm_G02_VG02_objecthighlight_03.png',
    path + 'timestorm_G02_VG02_objecthighlight_04.png',
    path + 'timestorm_G02_VG02_objecthighlight_05.png',
    path + 'timestorm_G02_VG02_objecthighlight_06.png',
    path + 'timestorm_G02_VG02_objecthighlight_07.png',
    path + 'timestorm_G02_VG02_objecthighlight_08.png',
    path + 'timestorm_G02_VG02_objecthighlight_09.png',
    path + 'timestorm_G02_VG02_objecthighlight_10.png',
    path + 'timestorm_G02_VG02_miniquestion_01.png',
    path + 'timestorm_G02_VG02_notepad_01.png',
    path + 'timestorm_G02_VG02_notepad_02.png',
    path + 'timestorm_G02_VG02_notepad_03.png',
    path + 'timestorm_G02_VG02_notepad_04.png',
	path + 'timestorm_G02_VG02_phonemap_01.png',
    path + 'timestorm_G02_VG02_phonemap_02.png',
    path + 'timestorm_G02_VG02_phonemap_02_.png',
    path + 'timestorm_G02_VG02_phonemap_03.png',
    path + 'timestorm_G02_VG02_phonemap_03_.png',
    path + 'timestorm_G02_VG02_phonemap_04.png',
    path + 'timestorm_G02_VG02_phonemap_04_.png',
	path + 'timestorm_G02_VG02_propoverlay_01.png',
    path + 'timestorm_G02_VG02_propoverlay_02.png',
    path + 'timestorm_G02_VG02_propoverlay_04.png',
    path + 'timestorm_G02_VG02_propoverlay_06.png',
    path + 'timestorm_G02_VG02_propoverlay_08.png',
	path + 'timestorm_G02_VG02_question_01.png',
	path + 'timestorm_G02_VG02_timer_01.png',
    path + 'timestorm_G02_VG02_time_01.png',
    path + 'timestorm_G02_VG02speaker_01.png'
    ];
    //loading sounds
    for (i = 0; i < 59; i++) {
        filesArray[i] = sndArray[i];

        sounds[i] = loadSound(filesArray[i]);
    }


    for (i = 59; i < 132; i++) {
        filesArray[i] = imgArray[i - 59];
        //console.log('loading '+filesArray[i]);
        loadImg(filesArray[i]);
    }


}


function loadImg(which) {
    var img = new Image();
    img.src = which;
    img.onload = function () {
        rm4Array(which);
    }
}

var count = filesArray.length;

function loadSound(which) {

    var snd = new Audio();
    snd.src = which;

    if (navigator.userAgent.match(/iPad/i)) {
        document.getElementById('loadingPercent').innerHTML = Math.floor((132 - count) * 100 / 132);
        count--;
        if (which == 'sounds/timestorm_G02_VG02_soundfx_11.mp3') {

            document.getElementById('loading').style.display = 'none';
            init();
        }
    }
    else {
        snd.addEventListener('canplaythrough', function () {
            rm4Array(which);
        });
        snd.onerror = function (e) {//document.getElementById('temp').innerHTML=snd.src+' gave error';
            console.log(snd.src + ' gave an error');
        }
    } return snd;
}

function rm4Array(what) {
    for (i = 0; i < filesArray.length; i++) {
        if (filesArray[i] == what) {
            filesArray.splice(i, 1);

            document.getElementById('loadingPercent').innerHTML = Math.floor((132 - filesArray.length) * 100 / 132);
        }
        if (filesArray.length < 1) {
            init();
            document.getElementById('loading').style.display = 'none';
        }
    }

}

function initVignette() {

    switch (currentvignette) {

        case 'v2':
            preload();
            break;
        case 'v3': window.location.href = '../vignette3/index.html';
            break;
        case 'v4': window.location.href = '../vignette4/index.html';
            break;
        case 'v5': window.location.href = '../vignette5/index.html';
            break;
        default:
            window.location.href = '../vignette1/index.html';
    }
}


function init() {
    setTimeout(function () {

                currentvignette = 'v2';
                $('#startScreen').fadeIn(1000);
                setTimeout(function () {
                    $('#startScreen').fadeOut(1000);

                }, 5000);

                setTimeout(function () {
                    currentvignette = 'v2';
                    switch (currentscene) {

                        case 'scene2': $('#newContainer').load('scene2child.html');
                            break;
                        case 'scene3': $('#newContainer').load('scene3child.html');
                            break;
                        case 'scene4': $('#newContainer').load('scene4child.html');
                            break;
                        case 'scene5': $('#newContainer').load('scene5child.html');
                            break;
                        case 'scene6': $('#newContainer').load('scene6child.html');
                            break;

                        default: $('#newContainer').load('scene1child.html');

                    }
                }, 6000);
                setTimeout(function () {

                    $('#TSframe2').css('display', 'block');
                    $('#pangoText a').css('display', 'block');
                    $('#phoneText ul').css('display', 'block');
                    $('#TSscene').css('opacity', '1');
                }, 7000);
        
    }, 1000);
}

var BrowserDetect = {
    init: function () {
        this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
        this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
        this.OS = this.searchString(this.dataOS) || "an unknown OS";
    },
    searchString: function (data) {
        for (var i = 0; i < data.length; i++) {
            var dataString = data[i].string;
            var dataProp = data[i].prop;
            this.versionSearchString = data[i].versionSearch || data[i].identity;
            if (dataString) {
                if (dataString.indexOf(data[i].subString) != -1)
                    return data[i].identity;
            }
            else if (dataProp)
                return data[i].identity;
        }
    },
    searchVersion: function (dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index == -1) return;
        return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
    },
    dataBrowser: [
		{
		    string: navigator.userAgent,
		    subString: "Chrome",
		    identity: "Chrome"
		},
		{
		    string: navigator.userAgent,
		    subString: "OmniWeb",
		    versionSearch: "OmniWeb/",
		    identity: "OmniWeb"
		},
		{
		    string: navigator.vendor,
		    subString: "Apple",
		    identity: "Safari",
		    versionSearch: "Version"
		},
		{
		    prop: window.opera,
		    identity: "Opera",
		    versionSearch: "Version"
		},
		{
		    string: navigator.vendor,
		    subString: "iCab",
		    identity: "iCab"
		},
		{
		    string: navigator.vendor,
		    subString: "KDE",
		    identity: "Konqueror"
		},
		{
		    string: navigator.userAgent,
		    subString: "Firefox",
		    identity: "Firefox"
		},
		{
		    string: navigator.vendor,
		    subString: "Camino",
		    identity: "Camino"
		},
		{		// for newer Netscapes (6+)
		    string: navigator.userAgent,
		    subString: "Netscape",
		    identity: "Netscape"
		},
		{
		    string: navigator.userAgent,
		    subString: "MSIE",
		    identity: "Explorer",
		    versionSearch: "MSIE"
		},
		{
		    string: navigator.userAgent,
		    subString: "Gecko",
		    identity: "Mozilla",
		    versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
		    string: navigator.userAgent,
		    subString: "Mozilla",
		    identity: "Netscape",
		    versionSearch: "Mozilla"
		}
    ],
    dataOS: [
		{
		    string: navigator.platform,
		    subString: "Win",
		    identity: "Windows"
		},
		{
		    string: navigator.platform,
		    subString: "Mac",
		    identity: "Mac"
		},
		{
		    string: navigator.userAgent,
		    subString: "iPhone",
		    identity: "iPhone/iPod"
		},
		{
		    string: navigator.platform,
		    subString: "Linux",
		    identity: "Linux"
		}
    ]

};
BrowserDetect.init();

function playSound(which) {
    if (which == '' || typeof (which) == 'undefined') { return; }
    sounds[which].play();
}

function macCSS() {

    if (navigator.userAgent.toUpperCase().indexOf('MAC') >= 0) {
        console.log('mac');
        $('body').addClass('mac');
    }

}