﻿var gender = 'male';
if (localStorage.getItem(userId + '_gender') != null) {
    gender = localStorage.getItem(userId + '_gender');
}
var delay1 = 1000;
var $divElementRenderAudioTags = $('#soundForAllBrowsers');
var $divElementRenderAudioTags2 = $('#soundForAllBrowsers2');
var voiceover01_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_01.wav";
var voiceover01_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_01.mp3";
var voiceover01_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_01.ogg";

var voiceover02_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_02.wav";
var voiceover02_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_02.mp3";
var voiceover02_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_02.ogg";

var voiceover47_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_47.wav";
var voiceover47_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_47.mp3";
var voiceover47_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_47.ogg";

var voiceover03_sound_file_url_wav = "";//sounds/voiceovers/voiceovers/timestorm_G02_VG02_VO_03.wav";
var voiceover03_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_03.mp3";
var voiceover03_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_03.ogg";

var voiceover04_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_04.wav";
var voiceover04_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_04.mp3";
var voiceover04_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_04.ogg";

var voiceover05_sound_file_url_wav = "";//sounds/" + gender + "/timestorm_G02_VG02_VO_05.wav";
var voiceover05_sound_file_url_mp3 = "sounds/" + gender + "/timestorm_G02_VG02_VO_05.mp3";
var voiceover05_sound_file_url_ogg = "sounds/" + gender + "/timestorm_G02_VG02_VO_05.ogg";

var voiceover06_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_06.wav";
var voiceover06_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_06.mp3";
var voiceover06_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_06.ogg";

var shutter_sound_file_url_wav = "";//sounds/timestorm_G02_VG02_soundfx_02.wav";
var shutter_sound_file_url_mp3 = "sounds/timestorm_G02_VG02_soundfx_02.mp3";
var shutter_sound_file_url_ogg = "sounds/timestorm_G02_VG02_soundfx_02.ogg";

var bus_sound_file_url_wav = "";//sounds/timestorm_G02_VG02_soundfx_03.wav";
var bus_sound_file_url_mp3 = "sounds/timestorm_G02_VG02_soundfx_03.mp3";
var bus_sound_file_url_ogg = "sounds/timestorm_G02_VG02_soundfx_03.ogg";