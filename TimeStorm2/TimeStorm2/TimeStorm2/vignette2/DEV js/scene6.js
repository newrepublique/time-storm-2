﻿var currentdiv = '#TScontainer';
var nextdiv = '#Dummy';
$(document).ready(function () {


    sessionStorage.setItem(userId + '_lastScene', 'scene6');    //save game

    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG02_background_12.jpg)').fadeIn(1000);
    


    setTimeout(function () {

        $('#TScompleted,#TSprogress').show();
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG02_phonemap_03.png)');
    }, 1000);

    setTimeout(function () {
        scene6_1();

    }, 1000);

});

function scene6_1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    setTimeout(function () {
        fadeloop('#objectHighlight02_146', 500, 500, 1000);
    }, delay6);


    $('#objectHighlight02_146').click(function () {
        $('#cover1').hide();
        playSound('0');
       
            $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG02_background_12.jpg)').fadeOut(1500);

            setTimeout(function () {
                $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG02_background_13.jpg)').fadeIn(2000);
            }, 0);
            setTimeout(function () {
                swap();
            }, 2000);
            setTimeout(function () {
                fadeloop('#objectHighlight02_148', 500, 500, 1000);
            }, 3000);


       

    });

    $('#objectHighlight02_148').click(function () {
        playSound('0');
        $('#cover2').hide();
        
        setTimeout(function () {
            
            playSound(57);
        }, 1000);

        setTimeout(function () {

            $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG02_background_13.jpg)').fadeOut(1500);

            setTimeout(function () {
                $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG02_background_17.png)').fadeIn(2000);
            }, 0);
            setTimeout(function () {
                swap();
            }, 2000);
        }, 4000);

        setTimeout(function () {

            $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG02_background_17.png)').fadeOut(1500);

            setTimeout(function () {
                $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG02_background_14.jpg)').fadeIn(2000);
            }, 0);
            setTimeout(function () {
                swap();
            }, 2000);
        }, 7000);

        setTimeout(function () {
            $('#subTitle02_153').fadeIn(250);
        }, 10000);

        setTimeout(function () {
            $('#subTitle02_153').fadeOut(250);

        }, 17000);

        setTimeout(function () {
            $('#subTitle02_154').fadeIn(250);
            playSound(46);
        }, 18000);

        setTimeout(function () {
            $('#subTitle02_154').fadeOut(250);
        }, 23000);


        setTimeout(function () {

            updatePhoneTxt('You’ve found the audio clips.', 'listIcon1');
        }, 24000);


        setTimeout(function () {
            $('#subTitle02_156').fadeIn(250);
        }, 26000);

        setTimeout(function () {
            $('#subTitle02_156').fadeOut(250);
            setGameStatus('.per', '100', 'v2');
        }, 30000);

        setTimeout(function () {
            $("#TSscene").fadeOut(1000);
        }, 31000);

        setTimeout(function () {
            $("#TSscene").fadeOut(500);
            sessionStorage.setItem(userId + '_lastScene', 'scene1');    //save game
            $('.per').html('000');
            sessionStorage.setItem('status-line-li', ' ');
            localStorage.setItem('phonetext', ' ');
            localStorage.setItem(userId + '_phonetext',' ');
            currentvignette = 'v3';                                             //change to v3 after deploying rest of the chapters.
            localStorage.setItem(userId + '_vignette', currentvignette);
            sessionStorage.setItem(userId + '_lastSceneSection', '');
        }, 32000);

        setTimeout(function () {
            sendData();
        }, 33000);

  
        setTimeout(function () {
            $("#endScreen").fadeIn(500);
        }, 34000);

        setTimeout(function () {
            $("#endScreen").fadeOut(500);
            $("#preloaderGif").fadeOut(500);
           
            //add link to next vignette after its deployment.
        }, 37000);

        setTimeout(function () {
            window.location.href = '../vignette3/index.html';
        }, 38000);
    });



}

function swap() {
    if (currentdiv == '#TScontainer') {
        currentdiv = '#Dummy';
        nextdiv = '#TScontainer';
    }
    else {
        nextdiv = '#Dummy';
        currentdiv = '#TScontainer';
    }
}