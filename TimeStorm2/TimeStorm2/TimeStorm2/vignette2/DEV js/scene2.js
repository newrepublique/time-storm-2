﻿var currentdiv = '#TScontainer';
var nextdiv = '#Dummy';
$(document).ready(function () {
    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG02_background_04.jpg)').stop(true, true).hide().fadeIn(1000);
    sessionStorage.setItem(userId + '_lastScene', 'scene2');    //save game
    

    setTimeout(function () {

        $('#TScompleted,#TSprogress').show();
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG02_phonemap_01.png)');
    }, 1000);

    setTimeout(function () {

        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {


            case 'scene2_2': scene2_2();
                break;
            case 'scene2_3': scene2_3();
                break;
            default: scene2_1();

        }


    }, 1000);

});

function scene2_1(){

    sessionStorage.setItem(userId + '_lastSceneSection', 'scene2_1');
    setTimeout(function () {

        updatePhoneTxt('You’re on the Freedom Ride bus.', 'listIcon1');
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG02_phonemap_01b.png)');
    }, delay2);

    setTimeout(function () {
        $('#subTitle02_023').fadeIn(250);
        
        playSound(7);
    }, 1000 + delay2);

    setTimeout(function () {
        $('#subTitle02_023').fadeOut(250);
        
    }, 4000 + delay2);

    setTimeout(function () {
        playSound(51);
    }, 5000 + delay2);

    setTimeout(function () {

        
    }, 14500 + delay2);
    setTimeout(function () {
        $('#subTitle02_025').fadeIn(250);
        
        playSound(8);
    }, 15000 + delay2);

    setTimeout(function () {
        $('#subTitle02_025').fadeOut(250);
        
    }, 18000 + delay2);

    setTimeout(function () {
        playSound(52);
    }, 19000 + delay2);


    setTimeout(function () {
        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG02_background_04.jpg)').fadeOut(1500);

        setTimeout(function () {
            $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG02_background_06.jpg)').fadeIn(2000);
        }, 0);
        setTimeout(function () {
            currentdiv = '#Dummy';
            nextdiv = '#TScontainer';
        }, 2000);
    }, 23000 + delay2);

    setTimeout(function () {
        $('#subTitle02_028').fadeIn(250);
        playSound(9);
    }, 26000 + delay2);

    setTimeout(function () {
        $('#subTitle02_028').fadeOut(250);
        
    }, 29000 + delay2);

    setTimeout(function () {
        $('#subTitle02_029').fadeIn(250);
        playSound(10);
    }, 30000 + delay2);

    setTimeout(function () {
        $('#subTitle02_029').fadeOut(250);
        
    }, 33000 + delay2);

    setTimeout(function () {
        $('#subTitle02_030').fadeIn(250);
        playSound(11);
    }, 34000 + delay2);

    setTimeout(function () {
        $('#subTitle02_030').fadeOut(250);
        
    }, 39000 + delay2);

    setTimeout(function () {
        $('#subTitle02_031').fadeIn(250);
        playSound(12);
    }, 40000 + delay2);
    setTimeout(function () {
        $('#subTitle02_031').fadeOut(250);
    }, 44000 + delay2);

    setTimeout(function () {
        scene2_2();
        setGameStatus('.per', '020', 'v2');
    }, 44000 + delay2);
}

function scene2_2() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG02_phonemap_01b.png)');
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene2_2');
    setTimeout(function () {
        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG02_background_06.jpg)').fadeOut(1500);

        setTimeout(function () {
            $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG02_background_16.jpg)').fadeIn(2000);
        }, 0);
        setTimeout(function () {

            swap();
        }, 2000);
    }, delay2);
    setTimeout(function () {
        $('#subTitle02_033').fadeIn(250);
        playSound(13);
    }, 3000 + delay2);

    setTimeout(function () {
        $('#subTitle02_033').fadeOut(250);
        
    }, 7000 + delay2);

    setTimeout(function () {
        $('#subTitle02_034').fadeIn(250);
        
        playSound(14);
    }, 8000 + delay2);

    setTimeout(function () {
        $('#subTitle02_034').fadeOut(250);
    }, 10000 + delay2);

    setTimeout(function () {
        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG02_background_16.jpg)').fadeOut(1500);

        setTimeout(function () {
            $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG02_background_07.jpg)').fadeIn(2000);
        }, 0);
        setTimeout(function () {
            swap();
        }, 2000);
        
    }, 11000 + delay2);

    setTimeout(function () {
        playSound(53);
    }, 13000 + delay2);

    setTimeout(function () {
        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG02_background_07.jpg)').fadeOut(1500);

        setTimeout(function () {
            $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG02_background_15.jpg)').fadeIn(2000);
        }, 0);
        setTimeout(function () {
            swap();
        }, 2000);
    }, 23000 + delay2);

    setTimeout(function () {
        $('#subTitle02_038').fadeIn(250);
        playSound(15);
    }, 26000 + delay2);

    setTimeout(function () {
        $('#subTitle02_038').fadeOut(250);
       
    }, 32000 + delay2);

    setTimeout(function () {
        $('#subTitle02_039').fadeIn(250);
        playSound(16);
    }, 33000 + delay2);

    setTimeout(function () {
        $('#subTitle02_039').fadeOut(250);
    }, 35000 + delay2);

    
    setTimeout(function () {
        scene2_3();
    }, 35000 + delay2);

   
}

function scene2_3(){
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene2_3');
    var clicked = false;
    var clicked_1 = false;
    localStorage.setItem(userId + '_phonetext', sessionStorage.getItem('status-line-li'));
    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG02_phonemap_02_.png)');
        $('#phoneMap02_040 , .phoneMap02_040_dot').show();
        fadeloop('.phoneMap02_040_dot', 200, 200, 500);
    }, delay2);



    $('.phoneMap02_040_dot').click(function () {
        playSound('0');

        $('#phoneMap02_040 , .phoneMap02_040_dot').hide();
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG02_phonemap_02.png)');

        if(!clicked){
            clicked = true;
            setTimeout(function () {
                $("#TScontainer").fadeOut(1000);
            }, delay2);

            setTimeout(function () {
                $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG02_background_10.jpg)').fadeIn(1000);

            }, 1000 + delay2);

            setTimeout(function () {
                updatePhoneTxt('You’re in the restroom.', 'listIcon1');
                
            }, 3000 + delay2);

            setTimeout(function () {
                $('#subTitle02_044').fadeIn(250);
                playSound(17);
            }, 4000 + delay2);

            setTimeout(function () {
                $('#subTitle02_044').fadeOut(250);
            }, 9000 + delay2);

            setTimeout(function () {
                $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG02_phonemap_04_.png)');
                $('#phoneMap02_045 , .phoneMap02_045_dot').show();
                fadeloop('.phoneMap02_045_dot', 200, 200, 500);
            }, 10000 + delay2);
    }

    });

    $('.phoneMap02_045_dot').click(function () {
        playSound('0');
        $('#phoneMap02_045 , .phoneMap02_045_dot').hide();
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG02_phonemap_04.png)');
        if (!clicked_1) {
            clicked_1 = true;
            setTimeout(function () {
                    $("#TScontainer").fadeOut(1000);
                }, delay2);

            setTimeout(function () {
                    $("#TScontainer").fadeIn(1000).css('background-image', 'url(./images/timestorm_G02_VG02_background_08.jpg)');
                    
                }, 2000 + delay2);

                setTimeout(function () {
                    $('#subTitle02_048_1').fadeIn(250);
                    playSound(18);
                }, 4000 + delay2);

                setTimeout(function () {
                     $('#subTitle02_048_1').fadeOut(250);
                }, 13000 + delay2);

                setTimeout(function () {
                    fadeloop('#objectHighlight02_049', 500, 500, 1000);
                }, 14000 + delay2);
        }
        
    });


    $('#objectHighlight02_049').click(function () {
        playSound('0');
        $('#cover').hide();
        
        setTimeout(function () {
            $('#subTitle02_051').fadeIn(250);
            playSound(19);
        }, delay2);

        setTimeout(function () {
            $('#subTitle02_051').fadeOut(250);
            //PlaySoundWithDelay($divElementRenderAudioTags, 0, voiceover20_sound_file_url_mp3, voiceover20_sound_file_url_ogg, voiceover20_sound_file_url_wav);
        }, 3000 + delay2);

        setTimeout(function () {
            $('#subTitle02_052').fadeIn(250);
            playSound(20);
        }, 4000 + delay2);

        setTimeout(function () {
            $('#subTitle02_052').fadeOut(250);
            //PlaySoundWithDelay($divElementRenderAudioTags, 0, voiceover21_sound_file_url_mp3, voiceover21_sound_file_url_ogg, voiceover21_sound_file_url_wav);
        }, 8000 + delay2);

        setTimeout(function () {
            $('#subTitle02_053_1').fadeIn(250);
            playSound(21);
        }, 9000 + delay2);

        setTimeout(function () {
            $('#subTitle02_053_1').fadeOut(250);
        
            $('#subTitle02_053_2').fadeIn(250);

        }, 18000 + delay2);

        setTimeout(function () {
            $('#subTitle02_053_2').fadeOut(250);
        }, 23000 + delay2);

        setTimeout(function () {
            $('#subTitle02_054_1').fadeIn(250);

        }, 24000 + delay2);

        setTimeout(function () {
            
            $('#subTitle02_054_1').fadeOut(250);
        }, 34000 + delay2);

        setTimeout(function () {
            setGameStatus('.per', '030', 'v2');
            
        }, 35000 + delay2);

        setTimeout(function () {
           // $("#TScontainer, #Dummy").fadeOut();
            $("#newContainer").load("scene3child.html");
        }, 36000 + delay2);
    });
}

function swap() {
    if (currentdiv == '#TScontainer') {
        currentdiv = '#Dummy';
        nextdiv = '#TScontainer';
    }
    else {
        nextdiv = '#Dummy';
        currentdiv = '#TScontainer';
    }
}