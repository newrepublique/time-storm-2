﻿var gender = 'male';
if (localStorage.getItem(userId + '_gender') != null) {
    gender = localStorage.getItem(userId + '_gender');
}

var $divElementRenderAudioTags = $('#soundForAllBrowsers');

var delay6 = 1000;
var voiceover46_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG02_VO_46.wav";
var voiceover46_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG02_VO_46.mp3";
var voiceover46_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG02_VO_46.ogg";

var vortex_sound_file_url_wav = "";//sounds/timestorm_G02_VG02_soundfx_10.wav";
var vortex_sound_file_url_mp3 = "sounds/timestorm_G02_VG02_soundfx_10.mp3";
var vortex_sound_file_url_ogg = "sounds/timestorm_G02_VG02_soundfx_10.ogg";
