﻿$(document).ready(function () {
    $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG05_background_07.jpg)').stop(true, true).hide().fadeIn(1000);
    sessionStorage.setItem(userId + '_lastScene', 'scene4');    //save game


    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG05_phonemap_04.png)');
        $('#TScompleted,#TSprogress').show();
    }, 1000 + delay4);

    setTimeout(function () {

        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {

            case 'scene4_2': showQuestion1();
                break;
            case 'scene4_3': postQuestion();
                break;

            default: scene4_1();
        }

    }, 1000 + delay4);


});


function scene4_1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene4_1');

    setTimeout(function () {
        updatePhoneTxt('You’ve found The Franklin River Dam section.', 'listIcon1');
    }, 500);


    setTimeout(function () {
        $('#subTitle05_058').fadeIn(250);
        playSound(20);
    }, delay4);


    setTimeout(function () {
        $('#subTitle05_058').fadeOut(250);

    }, 8000 + delay4);

    setTimeout(function () {
        $('#propOverlayImage05_059').fadeIn(250);
    }, 9000 + delay4);

    setTimeout(function () {
        playSound(21);
   
    }, 10000 + delay4);


    setTimeout(function () {
        $('#propOverlayImage05_059').fadeOut(250);
        showQuestion7();

    }, 33000 + delay4);


}



function showQuestion7() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene4_2');
    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector;

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion05_061').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion05_061 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#propOverlayQuestion05_061 #" + targetClickedId);
            correctAnswerSelector = targetClickedSelector;
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(7, result[0], result[1]);
                sendAnswer(7, targetClickedId);
                $('#propOverlayQuestion05_061 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion05_061').fadeOut(250);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion8();

                }, 1000);
            }

        }
    });
}


function showQuestion8() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector;

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion05_062').fadeIn(250);
    $('#sceneOver').show();

    $('#propOverlayQuestion05_062 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#propOverlayQuestion05_062 #" + targetClickedId);
            correctAnswerSelector = targetClickedSelector;
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(8, result[0], result[1]);
                sendAnswer(8, targetClickedId);
                $('#propOverlayQuestion05_062 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion05_062').fadeOut(250);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion9();

                }, 1000);
            }

        }
    });
}


function showQuestion9() {
   
    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector;

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion05_064').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion05_064 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#propOverlayQuestion05_064 #" + targetClickedId);
            correctAnswerSelector = targetClickedSelector;
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(9, result[0], result[1]);
                sendAnswer(9, targetClickedId);
                $('#propOverlayQuestion05_064 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion05_064').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    postQuestion();

                }, 1000);
            }

        }
    });
}


function sendResults(quesNo, NumberOfAttemps, Matrix) {
    userid = getQueryStringValue("userId");


    qno = quesNo;

    currentTime = new Date();
    timestamp = formatDate(currentTime);

    attempts = NumberOfAttemps;
    matrix = Matrix;


    setTimeout(function () {
        sendScore(userid, vignette, qno, timestamp, attempts, matrix, gameid, handlerURL);
    }, 1000);


}

function sendAnswer(quesNo, answer) {

    userid = getQueryStringValue("userId");

    answer = answer;
    qno = quesNo;

    currentTime = new Date();
    timestamp = formatDate(currentTime);

    setTimeout(function () {
        $.ajax({
            type: 'GET',
            url: '../Handlers/vignette5Results.ashx',
            data: {
                userid: userid,
                vignette: vignette,
                qno: qno,
                timestamp: timestamp,
                answer: answer,
                gameid: gameid
            },
            context: this,
            dataType: 'text',
            contentType: "application/json; charset=utf-8",
            success: function (msg) {
                //Below line needs to be removed
                //alert("Your score is added to database");
            },
            error: function () {
                //Below line needs to be removed
                //alert('error');
            }
        });
    }, 1000);
}


function postQuestion() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene4_3');
    setTimeout(function () {
        $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG05_background_07.jpg)').fadeOut(1500);

        setTimeout(function () {
            $("#Dummy").hide().css('background-image', 'url(./images/timestorm_G02_VG05_background_08.jpg)').fadeIn(2000);
        }, 0);
        $('#Dummy').attr('id', 'TScontainer');
        $("#TScontainer").attr('id', 'Dummy');
    }, delay4);

    setTimeout(function () {
        setGameStatus('.per', '075', 'v5');

    }, 2000 + delay4);

    setTimeout(function () {
        $('#subTitle05_068').fadeIn(250);
        playSound(22);
    }, 3000 + delay4);

    setTimeout(function () {
        $('#subTitle05_068').fadeOut(250);
        
    }, 12500 + delay4);

    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG05_phonemap_05_.png)');
        $('#phoneMap05_069 , .phoneMap05_069_dot').show();
        fadeloop('.phoneMap05_069_dot', 200, 200, 500);
    }, 12500 + delay4);
    

    $('.phoneMap05_069_dot').click(function () {
        playSound('0');
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG05_phonemap_05.png)');
        $('#phoneMap05_069 , .phoneMap05_069_dot').hide();
        $('#TScontainer, #Dummy').fadeOut();
        setTimeout(function () {
            playSound(30);
        }, delay4);


        setTimeout(function () {
            $("#newContainer").load("scene5child.html");
        }, 2000 + delay4);
    });
}