﻿$(document).ready(function () {
    $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG05_background_09.jpg)').stop(true, true).hide().fadeIn(1000);
    sessionStorage.setItem(userId + '_lastScene', 'scene5');    //save game


    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG05_phonemap_05.png)');
        $('#TScompleted,#TSprogress').show();
    }, 1000 + delay5);

    setTimeout(function () {

        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {

            case 'scene5_2': showQuestion1();
                break;
            case 'scene5_3': postQuestion();
                break;

            default: scene5_1();
        }

    }, 1000 + delay5);


});


function scene5_1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene5_1');

    setTimeout(function () {
        updatePhoneTxt('You’ve found The Mabo Decision section.', 'listIcon1');
    }, delay5);


    setTimeout(function () {
        $('#subTitle05_075_1').fadeIn(250);
        playSound(23);
    }, 1000 + delay5);


    setTimeout(function () {
        $('#subTitle05_075_1').fadeOut(250);
        $('#subTitle05_075_2').fadeIn(250);
    }, 9000 + delay5);

    setTimeout(function () {
        $('#subTitle05_075_2').fadeOut(250);
        
    }, 15000 + delay5);
    setTimeout(function () {
        $('#propOverlayImage05_076').fadeIn(250);
    }, 16000 + delay5);

    setTimeout(function () {
        playSound(24);
   
    }, 17000 + delay5);
    setTimeout(function () {
        playSound(25);

    }, 22000 + delay5);

    setTimeout(function () {
        $('#propOverlayImage05_076').fadeOut(250);
        showQuestion10();

    }, 41000 + delay5);


}



function showQuestion10() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene5_2');
    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector;

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion05_078').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion05_078 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#propOverlayQuestion05_078 #" + targetClickedId);
            correctAnswerSelector = targetClickedSelector;
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(10, result[0], result[1]);
                sendAnswer(10, targetClickedId);
                $('#propOverlayQuestion05_078 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion05_078').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion11();

                }, 1000);
            }

        }
    });
}


function showQuestion11() {
    
    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector;

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion05_079').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion05_079 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#propOverlayQuestion05_079 #" + targetClickedId);
            correctAnswerSelector = targetClickedSelector;
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(11, result[0], result[1]);
                sendAnswer(11, targetClickedId);
                $('#propOverlayQuestion05_079 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion05_079').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion12();

                }, 1000);
            }

        }
    });
}


function showQuestion12() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector;

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion05_082').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion05_082 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#propOverlayQuestion05_082 #" + targetClickedId);
            correctAnswerSelector = targetClickedSelector;
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(12, result[0], result[1]);
                sendAnswer(12, targetClickedId);
                $('#propOverlayQuestion05_082 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion05_082').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    postQuestion();

                }, 1000);
            }

        }
    });
}


function sendResults(quesNo, NumberOfAttemps, Matrix) {
    userid = getQueryStringValue("userId");


    qno = quesNo;

    currentTime = new Date();
    timestamp = formatDate(currentTime);

    attempts = NumberOfAttemps;
    matrix = Matrix;


    setTimeout(function () {
        sendScore(userid, vignette, qno, timestamp, attempts, matrix, gameid, handlerURL);
    }, 1000);


}

function sendAnswer(quesNo, answer) {

    userid = getQueryStringValue("userId");

    answer = answer;
    qno = quesNo;

    currentTime = new Date();
    timestamp = formatDate(currentTime);

    setTimeout(function () {
        $.ajax({
            type: 'GET',
            url: '../Handlers/vignette5Results.ashx',
            data: {
                userid: userid,
                vignette: vignette,
                qno: qno,
                timestamp: timestamp,
                answer: answer,
                gameid: gameid
            },
            context: this,
            dataType: 'text',
            contentType: "application/json; charset=utf-8",
            success: function (msg) {
                //Below line needs to be removed
                //alert("Your score is added to database");
            },
            error: function () {
                //Below line needs to be removed
                //alert('error');
            }
        });
    }, 1000);
}


function postQuestion() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene5_3');
    setTimeout(function () {
        $("#TScontainer").fadeOut(2000);
        $("#Dummy").hide().css('background-image', 'url(./images/timestorm_G02_VG05_background_10.jpg)').fadeIn(2000);
        
        $('#Dummy').attr('id', 'TScontainer');
        $("#TScontainer").attr('id', 'Dummy');
    }, delay5);

    setTimeout(function () {
        setGameStatus('.per', '100', 'v5');

    }, 2000 + delay5);

    setTimeout(function () {
        $('#subTitle05_086_1').fadeIn(250);
        playSound(26);
    }, 3000 + delay5);

    setTimeout(function () {
        $('#subTitle05_086_1').fadeOut(250);
        $('#subTitle05_086_2').fadeIn(250);
    }, 7000 + delay5);

    setTimeout(function () {
        $('#subTitle05_086_2').fadeOut(250);
        
    }, 16000 + delay5);

    setTimeout(function () {
        playSound(31);
        
    }, 17000 + delay5);

    setTimeout(function () {
        localStorage.setItem(userId + '_TS2complete', 'TS2true');
        $("#TSscene, #TScontainer, #Dummy").fadeOut(500);
    }, 20000 + delay5);

    setTimeout(function () {
        sendData();
    }, 21000 + delay5);


    setTimeout(function () {
        $("#endScreen").fadeIn(500);
    }, 21000 + delay5);


}