﻿$(document).ready(function () {
    $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG05_background_03.jpg)').stop(true, true).hide().fadeIn(1000);
    sessionStorage.setItem(userId + '_lastScene', 'scene2');    //save game


    setTimeout(function () {

        $('#TScompleted,#TSprogress').show();
    }, 1000 + delay2);

    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG05_phonemap_02.png)');
        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {

            case 'scene2_2': showQuestion1();
                break;
            case 'scene2_3': postQuestion();
                break;

            default: scene2_1();
        }

    }, 1000 + delay2);


});


function scene2_1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene2_1');

    setTimeout(function () {
        updatePhoneTxt('You’ve found the Future Environment section.', 'listIcon1');
    }, 500);


    setTimeout(function () {
        $('#subTitle05_023').fadeIn(250);
        playSound(7);
    }, delay2);

    setTimeout(function () {
        $('#subTitle05_023').fadeOut(250);

    }, 2500 + delay2);

    setTimeout(function () {
        $('#subTitle05_024').fadeIn(250);
        playSound(8);
    }, 3000 + delay2);

    setTimeout(function () {
        $('#subTitle05_024').fadeOut(250);

    }, 15000 + delay2);

    setTimeout(function () {
        $('#propOverlayImage05_025').fadeIn(250);
    }, 16000 + delay2);

    setTimeout(function () {
        playSound(9);
    }, 17000 + delay2);

    setTimeout(function () {
        playSound(10);
    }, 24000 + delay2);

    setTimeout(function () {
        playSound(11);
    }, 32000 + delay2);


    setTimeout(function () {
        $('#propOverlayImage05_025').fadeOut(500);
        showQuestion1();

    }, 42000 + delay2);


}



function showQuestion1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene2_2');
    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector;

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion05_027').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion05_027 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#propOverlayQuestion05_027 #" + targetClickedId);
            correctAnswerSelector = targetClickedSelector;
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(1, result[0], result[1]);
                sendAnswer(1, targetClickedId);
                $('#propOverlayQuestion05_027 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion05_027').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion2();

                }, 1000);
            }

        }
    });
}


function showQuestion2() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector;

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion05_029').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion05_029 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#propOverlayQuestion05_029 #" + targetClickedId);
            correctAnswerSelector = targetClickedSelector;
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(2, result[0], result[1]);
                sendAnswer(2, targetClickedId);
                $('#propOverlayQuestion05_029 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion05_029').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    postQuestion();

                }, 1000);
            }

        }
    });
}

function sendResults(quesNo, NumberOfAttemps, Matrix) {
    userid = getQueryStringValue("userId");


    qno = quesNo;

    currentTime = new Date();
    timestamp = formatDate(currentTime);

    attempts = NumberOfAttemps;
    matrix = Matrix;


    setTimeout(function () {
        sendScore(userid, vignette, qno, timestamp, attempts, matrix, gameid, handlerURL);
    }, 1000);


}

function sendAnswer(quesNo, answer) {

    userid = getQueryStringValue("userId");

    answer = answer;
    qno = quesNo;

    currentTime = new Date();
    timestamp = formatDate(currentTime);

    setTimeout(function () {
        $.ajax({
            type: 'GET',
            url: '../Handlers/vignette5Results.ashx',
            data: {
                userid: userid,
                vignette: vignette,
                qno: qno,
                timestamp: timestamp,
                answer: answer,
                gameid: gameid
            },
            context: this,
            dataType: 'text',
            contentType: "application/json; charset=utf-8",
            success: function (msg) {
                //Below line needs to be removed
                //alert("Your score is added to database");
            },
            error: function () {
                //Below line needs to be removed
                //alert('error');
            }
        });
    }, 1000);
}


function postQuestion() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene2_3');
    setTimeout(function () {
        $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG05_background_03.jpg)').fadeOut(1500);

        setTimeout(function () {
            $("#Dummy").hide().css('background-image', 'url(./images/timestorm_G02_VG05_background_04.jpg)').fadeIn(2000);
            $('#Dummy').attr('id', 'TScontainer');
            $("#TScontainer").attr('id', 'Dummy');
        }, 0);

    }, delay2);

    setTimeout(function () {
        setGameStatus('.per', '025', 'v5');

    }, 2000 + delay2);

    setTimeout(function () {
        $('#subTitle05_033').fadeIn(250);
        playSound(12);
    }, 3000 + delay2);

    setTimeout(function () {
        $('#subTitle05_033').fadeOut(250);

    }, 8000 + delay2);

    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG05_phonemap_03_.png)');
        $('#phoneMap05_034 , .phoneMap05_034_dot').show();
        fadeloop('.phoneMap05_034_dot', 200, 200, 500);
    }, 9000 + delay2);


    $('.phoneMap05_034_dot').click(function () {
        playSound('0');
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG05_phonemap_03.png)');
        $('#TScontainer, #Dummy').fadeOut();
        $('#phoneMap05_034 , .phoneMap05_034_dot').hide();
        setTimeout(function () {
            playSound(30);
        }, delay2);

        setTimeout(function () {
            $('#subTitle05_038').fadeIn(250);
            playSound(13);
        }, 3000 + delay2);

        setTimeout(function () {
            $('#subTitle05_038').fadeOut(250);

        }, 12000 + delay2);

        setTimeout(function () {
            $("#newContainer").load("scene3child.html");
        }, 13000 + delay2);
    });
}