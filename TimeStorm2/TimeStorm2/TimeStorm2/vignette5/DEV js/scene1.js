﻿$(document).ready(function () {
    if (localStorage.getItem(userId + '_lastSceneSection') == 'scene1_2') {
        $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG05_background_02.jpg)').stop(true, true).hide().fadeIn(1000);
    }
    else {
        $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG05_background_01.jpg)').stop(true, true).hide().fadeIn(1000);
    }

    sessionStorage.setItem(userId + '_lastScene', 'scene1');    //save game


    setTimeout(function () {

        $('#TScompleted,#TSprogress').fadeIn(1000);
    }, 1000 + delay1);

    setTimeout(function () {

        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {

            case 'scene1_2': scene1_2();
                break;

            default: scene1_1();
        }

    }, 2000 + delay1);


});


function scene1_1() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene1_1');

    setTimeout(function () {
        playSound(28);
    }, 0);

    setTimeout(function () {
        updatePhoneTxt('You are in The Archive.', 'listIcon1');
    }, 7000 + delay1);
    setTimeout(function () {
        updatePhoneTxt('Retrieving map from cache.', 'listIcon1');
    }, 8500 + delay1);

    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG05_phonemap_01.png)');

    }, 10000 + delay1);


    setTimeout(function () {
        $('#subTitle05_007').fadeIn(250);
        playSound(3);
    }, 11000 + delay1);

    setTimeout(function () {
        $('#subTitle05_007').fadeOut(250);

    }, 22000 + delay1);

    setTimeout(function () {
        $('#subTitle05_008').fadeIn(250);
        playSound(2);
    }, 23000 + delay1);

    setTimeout(function () {
        $('#subTitle05_008').fadeOut(250);

    }, 25000 + delay1);

    setTimeout(function () {
        playSound(30);
    }, 26000 + delay1);

    setTimeout(function () {
        $('#subTitle05_011_1').fadeIn(250);
        playSound(4);
    }, 28000 + delay1);

    setTimeout(function () {
        $('#subTitle05_011_1').fadeOut(250);
        $('#subTitle05_011_2').fadeIn(250);
    }, 37000 + delay1);

    setTimeout(function () {
        $('#subTitle05_011_2').fadeOut(250);
        $('#subTitle05_011_3').fadeIn(250);
    }, 49000 + delay1);

    setTimeout(function () {
        $('#subTitle05_011_3').fadeOut(250);

    }, 63000 + delay1);

    setTimeout(function () {
        scene1_2();

    }, 63000 + delay1);


}


function scene1_2() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene1_2');
    if (localStorage.getItem(userId + '_lastSceneSection') == 'scene1_1') {
        setTimeout(function () {
            $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG05_background_01.jpg)').fadeOut(1500);

            setTimeout(function () {
                $("#Dummy").hide().css('background-image', 'url(./images/timestorm_G02_VG05_background_02.jpg)').fadeIn(2000);
                $('#Dummy').attr('id', 'TScontainer');
                $("#TScontainer").attr('id', 'Dummy');
            }, 0);

        }, delay1);
    }
    else {
        setTimeout(function () {
            $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG05_background_02.jpg)').fadeIn(1000);

        }, delay1);
    }

    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG05_phonemap_02.png)');

    }, 2000 + delay1);



    setTimeout(function () {
        $('#subTitle05_016').fadeIn(500);
        playSound(5);
    }, 3000 + delay1);

    setTimeout(function () {
        $('#subTitle05_016').fadeOut(500);

    }, 10000 + delay1);

    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG05_phonemap_02_.png)');
        $('#phoneMap05_015 , .phoneMap05_015_dot').show();
        fadeloop('.phoneMap05_015_dot', 200, 200, 500);
    }, 10000 + delay1);


    $('.phoneMap05_015_dot').click(function () {
        playSound('0');
        $('#phoneMap05_015 , .phoneMap05_015_dot').hide();
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG05_phonemap_02.png)');

        $("#TScontainer, #Dummy").fadeOut(500);
        setTimeout(function () {
            playSound(30);
        }, delay1);


        setTimeout(function () {
            $('#subTitle05_020_1').fadeIn(500);
            playSound(6);
        }, 3000 + delay1);

        setTimeout(function () {
            $('#subTitle05_020_1').fadeOut(500);
            $('#subTitle05_020_2').fadeIn(500);
        }, 12000 + delay1);

        setTimeout(function () {
            $('#subTitle05_020_2').fadeOut(500);
            $('#subTitle05_020_3').fadeIn(500);
        }, 24000 + delay1);

        setTimeout(function () {
            $('#subTitle05_020_3').fadeOut(500);

        }, 31000 + delay1);

        setTimeout(function () {
            $("#newContainer").load("scene2child.html");
        }, 31000 + delay1);

    });


}

