﻿$(document).ready(function () {
    $("#TScontainer").hide().css('background-image', 'url(./images/timestorm_G02_VG05_background_05.jpg)').stop(true, true).hide().fadeIn(1000);
    sessionStorage.setItem(userId + '_lastScene', 'scene3');    //save game


    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG05_phonemap_03.png)');
        $('#TScompleted,#TSprogress').show();
    }, 1000 + delay3);

    setTimeout(function () {

        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {

            case 'scene3_2': showQuestion1();
                break;
            case 'scene3_3': postQuestion();
                break;

            default: scene3_1();
        }

    }, 1000 + delay3);


});


function scene3_1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene3_1');

    setTimeout(function () {
        updatePhoneTxt('You’ve found the Civil Rights section.', 'listIcon1');
    }, 500);


    setTimeout(function () {
        $('#subTitle05_041_1').fadeIn(250);
        playSound(14);
    }, delay3);

    setTimeout(function () {
        $('#subTitle05_041_1').fadeOut(250);

        $('#subTitle05_041_2').fadeIn(250);
    }, 3000 + delay3);

    setTimeout(function () {
        $('#subTitle05_041_2').fadeOut(250);

    }, 14000 + delay3);


    setTimeout(function () {
        $('#subTitle05_042').fadeIn(250);
        playSound(15);
    }, 15000 + delay3);

    setTimeout(function () {
        $('#subTitle05_042').fadeOut(250);

    }, 22000 + delay3);

    setTimeout(function () {
        $('#propOverlayImage05_043').fadeIn(250);
    }, 23000 + delay3);

    setTimeout(function () {
        playSound(16);
    }, 24000 + delay3);

    setTimeout(function () {
        playSound(17);
    }, 38000 + delay3);

    setTimeout(function () {
        playSound(18);
    }, 46000 + delay3);


    setTimeout(function () {
        $('#propOverlayImage05_043').fadeOut(250);
        showQuestion3();

    }, 48000 + delay3);


}



function showQuestion3() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene3_2');
    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector;

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion05_043').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion05_043 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#propOverlayQuestion05_043 #" + targetClickedId);
            correctAnswerSelector = targetClickedSelector;
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(3, result[0], result[1]);
                sendAnswer(3, targetClickedId);
                $('#propOverlayQuestion05_043 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion05_043').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion4();

                }, 1000);
            }

        }
    });
}


function showQuestion4() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene3_2');
    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector;

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion05_044').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion05_044 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#propOverlayQuestion05_044 #" + targetClickedId);
            correctAnswerSelector = targetClickedSelector;
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(4, result[0], result[1]);
                sendAnswer(4, targetClickedId);
                $('#propOverlayQuestion05_044 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion05_044').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion5();

                }, 1000);
            }

        }
    });
}

function showQuestion5() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene3_2');
    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector;

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion05_046').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion05_046 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#propOverlayQuestion05_046 #" + targetClickedId);
            correctAnswerSelector = targetClickedSelector;
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(5, result[0], result[1]);
                sendAnswer(5, targetClickedId);
                $('#propOverlayQuestion05_046 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion05_046').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion6();

                }, 1000);
            }

        }
    });
}

function showQuestion6() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene3_2');
    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector;

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion05_047').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion05_047 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#propOverlayQuestion05_047 #" + targetClickedId);
            correctAnswerSelector = targetClickedSelector;
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(6, result[0], result[1]);
                sendAnswer(6, targetClickedId);
                $('#propOverlayQuestion05_047 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion05_047').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    postQuestion();

                }, 1000);
            }

        }
    });
}


function sendResults(quesNo, NumberOfAttemps, Matrix) {
    userid = getQueryStringValue("userId");


    qno = quesNo;

    currentTime = new Date();
    timestamp = formatDate(currentTime);

    attempts = NumberOfAttemps;
    matrix = Matrix;


    setTimeout(function () {
        sendScore(userid, vignette, qno, timestamp, attempts, matrix, gameid, handlerURL);
    }, 1000);


}

function sendAnswer(quesNo, answer) {

    userid = getQueryStringValue("userId");

    answer = answer;
    qno = quesNo;

    currentTime = new Date();
    timestamp = formatDate(currentTime);

    setTimeout(function () {
        $.ajax({
            type: 'GET',
            url: '../Handlers/vignette5Results.ashx',
            data: {
                userid: userid,
                vignette: vignette,
                qno: qno,
                timestamp: timestamp,
                answer: answer,
                gameid: gameid
            },
            context: this,
            dataType: 'text',
            contentType: "application/json; charset=utf-8",
            success: function (msg) {
                //Below line needs to be removed
                //alert("Your score is added to database");
            },
            error: function () {
                //Below line needs to be removed
                //alert('error');
            }
        });
    }, 1000);
}


function postQuestion() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene3_3');
    setTimeout(function () {
        $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG05_background_05.jpg)').fadeOut(1500);

        setTimeout(function () {
            $("#Dummy").hide().css('background-image', 'url(./images/timestorm_G02_VG05_background_06.jpg)').fadeIn(2000);
            $('#Dummy').attr('id', 'TScontainer');
            $("#TScontainer").attr('id', 'Dummy');
        }, 0);
    }, delay3);

    setTimeout(function () {
        setGameStatus('.per', '050', 'v5');

    }, 2000 + delay3);

    setTimeout(function () {
        $('#subTitle05_051').fadeIn(250);
        playSound(19);
    }, 3000 + delay3);

    setTimeout(function () {
        $('#subTitle05_051').fadeOut(250);
        
    }, 12000 + delay3);

    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG05_phonemap_04_.png)');
        $('#phoneMap05_052 , .phoneMap05_052_dot').show();
        fadeloop('.phoneMap05_052_dot', 200, 200, 500);
    }, 13000 + delay3);
    

    $('.phoneMap05_052_dot').click(function () {
        playSound('0');
        $('#phoneMap05_052 , .phoneMap05_052_dot').hide();
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG05_phonemap_04.png)');
        $('#TScontainer, #Dummy').fadeOut();
        setTimeout(function () {
            playSound(30);
        }, delay3);


        setTimeout(function () {
            $("#newContainer").load("scene4child.html");
        }, 2000 + delay3);
    });
}