﻿<%@ WebHandler Language="C#" Class="vignette5Results" %>

using System;
using System.Web;

public class vignette5Results : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        try
        {
            long userid;
            long.TryParse(context.Request["userid"], out userid);
            string vignette = context.Request["vignette"];
            string qno = context.Request["qno"];
         
            string gameid = context.Request["gameid"];

            string timestamp = context.Request["timestamp"];
            
            int answer;
            int.TryParse(context.Request["answer"], out answer);

            if (userid > 0)
            {
                string strConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Timestorm2.DbConnection"].ConnectionString;
                MySql.Data.MySqlClient.MySqlConnection cn = new MySql.Data.MySqlClient.MySqlConnection(strConnString);
                cn.Open();
                string insertCommand = "INSERT INTO `vignette5score`(`userid`,`vignette`,`qno`,`timestamp`,`answer`,`gameid`)VALUES (" + userid + ",'" + vignette + "','" + qno + "', '" + timestamp + "', " + answer + ",'" + gameid + "');";
                MySql.Data.MySqlClient.MySqlCommand cmd =
                      new MySql.Data.MySqlClient.MySqlCommand(insertCommand, cn);

                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }
        catch (Exception ex)
        {


        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}