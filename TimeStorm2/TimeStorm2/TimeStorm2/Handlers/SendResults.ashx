﻿<%@ WebHandler Language="C#" Class="SendResults" %>

using System;
using System.Web;
using System.Text;
using System.IO;

public class SendResults : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
		long userid;
		long.TryParse(context.Request["userid"], out userid);
        try
        {
            
            
            string vignette = context.Request["vignette"];
            string qno = context.Request["qno"];
            string matrix = context.Request["matrix"];
            string gameid = context.Request["gameid"];

            string timestamp = context.Request["timestamp"];
            
            int attempts;
            int.TryParse(context.Request["attempts"], out attempts);

            if (userid > 0)
            {
                string strConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Timestorm2.DbConnection"].ConnectionString;
                MySql.Data.MySqlClient.MySqlConnection cn = new MySql.Data.MySqlClient.MySqlConnection(strConnString);
                cn.Open();
                string insertCommand = "INSERT INTO `score`(`userid`,`vignette`,`qno`,`timestamp`,`attempts`,`matrix`,`gameid`)VALUES (" + userid + ",'" + vignette + "','" + qno + "', '" + timestamp + "', " + attempts + ",'" + matrix + "','" + gameid + "');";
                MySql.Data.MySqlClient.MySqlCommand cmd =
                      new MySql.Data.MySqlClient.MySqlCommand(insertCommand, cn);

                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }
        catch (Exception ex)
        {
			WriteLog(ex, userid);

        }
    }
	public void WriteLog(Exception ex, long userId)
    {
        WriteLogToFile("\n ------------------------------------------------- \n");
        WriteLogToFile("DateTime = " + DateTime.Now.ToString("dd-MM-yy hh:mm:ss") + " for userId: " + userId + "\n");
        WriteLogToFile("Message: " + ex.Message + "\n");
        WriteLogToFile(ex.StackTrace);
    }
    
    public void WriteLogToFile(string user_message)
    {
        string log_file = @"TS2_log.txt";
        string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\TS2log\";

        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);

        try
        {
            //Opens a new file stream which allows asynchronous reading and writing
            using (StreamWriter sw = new StreamWriter(new FileStream(path + log_file, FileMode.Append, FileAccess.Write, FileShare.ReadWrite)))
            {
                //Writes the method name with the exception and writes the exception underneath
                sw.WriteLine(String.Format("{0} ({1}) {2}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), user_message));
                sw.WriteLine("");
            }
        }
        catch (IOException)
        {
            if (!System.IO.File.Exists(path + log_file))
                System.IO.File.Create(path + log_file);
        }
    }
    public bool IsReusable {
        get {
            return false;
        }
    }

}