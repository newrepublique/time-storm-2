﻿<%@ WebHandler Language="C#" Class="UserDetails" %>

using System;
using System.Web;
using System.Text;
using System.IO;

public class UserDetails : IHttpHandler {

    static string strConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Timestorm2.DbConnection"].ConnectionString;
    MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(strConnString);
    
    public void ProcessRequest (HttpContext context) {
                
        long userId;
        long.TryParse(context.Request["userid"], out userId);
        if (userId <= 0)
        {
            return;
        }

        var requestAction = context.Request["action"];
        
        switch (requestAction)
        {
            case "get":
                GetUserDetails(userId, context);
                break;
            case "save":
                SaveUpdateUserDetails(userId, context);
                break;           
            default:
                break;
        }
    }

    public void GetUserDetails(long userId, HttpContext context)
    {
        try
        {
            string selectCommand = "SELECT `user_details` FROM `userdetails` WHERE `userId` = " + userId + ";";
            MySql.Data.MySqlClient.MySqlCommand cmd =
                  new MySql.Data.MySqlClient.MySqlCommand(selectCommand, con);
            con.Open();
            MySql.Data.MySqlClient.MySqlDataReader dataReader = cmd.ExecuteReader();
            if (dataReader.HasRows)
            {
                while (dataReader.Read())
                {
                    string details = dataReader["user_details"].ToString();
                    context.Response.Write(details);
                }

                dataReader.Close();
            }
        }
        catch (Exception ex)
        {
            WriteLog(ex, userId);
        }
        finally
        {
            con.Close();
        }
    }

    public void SaveUpdateUserDetails(long userId, HttpContext context)
    {
        try
        {
            string selectCommand = "SELECT `user_details` FROM `userdetails` WHERE `userId` = " + userId + ";";
            MySql.Data.MySqlClient.MySqlCommand cmd =
                  new MySql.Data.MySqlClient.MySqlCommand(selectCommand, con);
            con.Open();
            MySql.Data.MySqlClient.MySqlDataReader dataReader = cmd.ExecuteReader();
            if (dataReader.HasRows)
            {
                con.Close();
                UpdateUserDetails(userId, context);
            }
            else
            {
                con.Close();
                SaveUserDetails(userId, context);
            }
        }
        catch (Exception ex)
        {
            WriteLog(ex, userId);
        }
    }

    public void UpdateUserDetails(long userId, HttpContext context)
    {        
        try
        {
            string details = context.Request["details"];
            string updateCommand = "UPDATE `userdetails`SET `user_details` = '" + details + "' WHERE `userId` = " + userId + ";";
            MySql.Data.MySqlClient.MySqlCommand cmd =
                  new MySql.Data.MySqlClient.MySqlCommand(updateCommand, con);
            con.Open();
            cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            WriteLog(ex, userId);
        }
        finally
        {
            con.Close();
        }
    }

    public void SaveUserDetails(long userId, HttpContext context)
    {
        try
        {
            string details = context.Request["details"];
            string insertCommand = "INSERT INTO `userdetails`(`userId`,`user_details`)VALUES (" + userId + ",'" + details + "');";
            MySql.Data.MySqlClient.MySqlCommand cmd =
                  new MySql.Data.MySqlClient.MySqlCommand(insertCommand, con);
            con.Open();
            cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            WriteLog(ex, userId);
        }
        finally
        {
            con.Close();
        }
    }

    public void WriteLog(Exception ex, long userId)
    {
        WriteLogToFile("\n ------------------------------------------------- \n");
        WriteLogToFile("DateTime = " + DateTime.Now.ToString("dd-MM-yy hh:mm:ss") + " for userId: " + userId + "\n");
        WriteLogToFile("Message: " + ex.Message + "\n");
        WriteLogToFile(ex.StackTrace);
    }
    
    public void WriteLogToFile(string user_message)
    {
        string log_file = @"TS2_log.txt";
        string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\TS2log\";

        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);

        try
        {
            //Opens a new file stream which allows asynchronous reading and writing
            using (StreamWriter sw = new StreamWriter(new FileStream(path + log_file, FileMode.Append, FileAccess.Write, FileShare.ReadWrite)))
            {
                //Writes the method name with the exception and writes the exception underneath
                sw.WriteLine(String.Format("{0} ({1}) {2}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), user_message));
                sw.WriteLine("");
            }
        }
        catch (IOException)
        {
            if (!System.IO.File.Exists(path + log_file))
                System.IO.File.Create(path + log_file);
        }
    }
    
    public bool IsReusable {
        get {
            return false;
        }
    }

}