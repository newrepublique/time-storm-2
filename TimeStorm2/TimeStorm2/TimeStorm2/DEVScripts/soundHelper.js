﻿
/* This is the helper file created for providing sound effects [Play-Mute] through out the application. */
/*---------------------------------------------------------------------- */

sessionStorage.setItem('muteSound', false);
//Play sound with delay
function PlaySoundWithDelay($divElementRenderAudioTags, delay, loading_sound_file_url, loading_sound_file_url_ogg, sound_file_url_wav) {
    if ($divElementRenderAudioTags != null && $divElementRenderAudioTags != "") {
        setTimeout(function () {
            PlaySound($divElementRenderAudioTags, loading_sound_file_url, loading_sound_file_url_ogg, sound_file_url_wav);
        }, delay);
    }
}

//Method to play sound.
function PlaySound($AudioTags, sound_file_url, sound_file_url_ogg, sound_file_url_wav) {
    $AudioTags.html("<audio id='audioControl'><source src='" + sound_file_url_wav + "' type='audio/wav'><source src='" + sound_file_url + "' type='audio/mpeg'><source src='" + sound_file_url_ogg + "' type='audio/ogg'><embed height='50' width='100' hidden=true autostart=true loop=false src='" + sound_file_url + "'></audio>");
    setAudio(JSON.parse(sessionStorage.getItem('muteSound')));
}

//Method to play another sound if required.
function PlayAnotherSound($AudioTags, delay, sound_file_url, sound_file_url_ogg, sound_file_url_wav) {
    if ($AudioTags != null && $AudioTags != "") {
        $AudioTags.html("<audio id='anotherAudioControl'><source src='" + sound_file_url_wav + "' type='audio/wav'><source src='" + sound_file_url + "' type='audio/mpeg'><source src='" + sound_file_url_ogg + "' type='audio/ogg'><embed height='50' width='100' hidden=true autostart=true loop=false src='" + sound_file_url + "'></audio>");
        setAudio(JSON.parse(sessionStorage.getItem('muteSound')));
    }
}

//Method to mute/un-mute sound
function muteSoundFunction(mute) {
    setAudio(mute);
    return !(mute);
}

function setAudio(mute) {
    if (mute) {
        $('audio').prop('volume', '0');
        for (i = 0; i < sounds.length; i++) {
            sounds[i].volume = 0;
        }
    }
    else {
        $('audio').prop('volume', '1');
        for (i = 0; i < sounds.length; i++) {
            sounds[i].volume = 1;
        }
    }
}

/*---------------------------------------------------------------------- */
