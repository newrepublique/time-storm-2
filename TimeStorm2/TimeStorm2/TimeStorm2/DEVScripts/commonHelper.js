﻿
/* This is the helper file created for providing common functionalities through out the application. */

/*Set game Status
  ----------------------------------------------------------------------*/
function setGameStatus(selector, value, vignette_completed) {
    $(selector).text(value);
    localStorage.setItem(vignette_completed, value);
}
/*----------------------------------------------------------------------*/

/*For Full screen:
----------------------------------------------------------------------*/
//Below methods need to be called for Timer within a set interval method of interval 1 second
var isFullScreen = false;
var isFirstTime = true;
if (document.fullscreen || document.mozFullScreen || document.webkitIsFullScreen)
    isFullScreen = true;

function attemptFullScreen() {

    if (isFullScreen) return;

    if (isFirstTime) {
        var elem = document.getElementById("i1");
        if (elem.requestFullScreen) {
            elem.requestFullScreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullScreen) {
            elem.webkitRequestFullScreen();
        }
        isFirstTime = false;
    }

    addListener();
}

function addListener() {
    document.addEventListener("fullscreenchange", function () {
        if (!document.fullscreen) { readjust(); }
    }, false);

    document.addEventListener("mozfullscreenchange", function () {
        if (!document.mozFullScreen) { readjust(); }
    }, false);

    document.addEventListener("webkitfullscreenchange", function () {
        if (!document.webkitIsFullScreen) { readjust(); }
    }, false);
}

function readjust() {
    isFullScreen = false;
}

/*----------------------------------------------------------------------*/



/*For Timer:
----------------------------------------------------------------------*/
//Below methods need to be called for Timer within a set interval method of interval 1 second

function currentTimerValues(timerTime) {
    var minutes = fixIntegers(Math.floor(timerTime / 60));
    var seconds = fixIntegers(timerTime % 60);
    return [minutes, seconds];
}

function fixIntegers(integer) {
    if (integer < 0)
        integer = 0;
    if (integer < 10)
        return "0" + integer;
    return "" + integer;
}
/*---------------------------------------------------------------------- */


/*Common method to update phone text on closing Flying Div
---------------------------------------------------------------------- */
function ShowNextFlyWithPhoneUpdate(elementToShow, textToUpdate, fadeOutTime, FadeInTime, delay) {
    setTimeout(function () {
        $(elementToShow).fadeIn(FadeInTime);
        if (textToUpdate != "") {
            updatePhoneTxt(textToUpdate);
        }
    }, delay);
}


var phoneData = '';
function updatePhoneTxt(textToUpdate, classToRender) {
    phoneData = storePhoneData("<li class=\"" + classToRender + "\">" + textToUpdate + "</li>");
    $('#list').html(phoneData);
    $('.scroll-pane').jScrollPane(
		{
		    autoReinitialise: true,
		    verticalDragMinHeight: 21,
        verticalDragMaxHeight: 21
		}
	);
   // fleXenv.fleXcrollMain("phoneText");
   // refreshPhoneTxt('phoneText');
}

function refreshPhoneTxt(elementId) {
    var maxi = document.getElementById(elementId).fleXdata.scrollPosition[1][1];
    document.getElementById(elementId).fleXcroll.setScrollPos(false, maxi);
    //document.getElementById(elementId).fleXcroll.scrollContent(false, 50);
}


function storePhoneData(textToStore) {
    var currentData = sessionStorage.getItem('status-line-li');

    if (currentData != null) {

        if (localStorage.getItem(userId + '_vignette') === 'v3') {
            currentData = currentData + textToStore;
        }
        else {
            if (currentData.indexOf(textToStore) === -1) {
                currentData = currentData + textToStore;
            }
        }
    }
    else {
        currentData = textToStore;
    }

    sessionStorage.setItem('status-line-li', currentData);
    return currentData;
}
/*---------------------------------------------------------------------- */


/*Common function to show hot spots
---------------------------------------------------------------------- */
function fadeloop(selector, timeout, timein, interval) {
    var $selector = $(selector)
    fn = function () {
        $selector.fadeOut(timeout).fadeIn(timein);
    };
    setInterval(fn, interval);
    return false;
}

/*---------------------------------------------------------------------- */


/*For Droppables:
---------------------------------------------------------------------- */
//common method for all droppables and to select one draggable. Method needs to be called from scene-x.html
function makeDroppables(droppableSelector, acceptableDraggableSelector) {
    $(droppableSelector).droppable({
        drop: function (event, ui) {
            var self = $(this);
            var dropped = ui.draggable;
            $(acceptableDraggableSelector).draggable({ disabled: true });
            dropmethodForDroppable(self, dropped); // This method needs to be implemented per scene and the below method needs to be removed
        },
        accept: acceptableDraggableSelector
    });
}

/*---------------------------------------------------------------------- */


/*Calculate Number of attempts and Matrix:
Variables start, matrix and Number of attempts need to be passed everytime this method is call for a Q&A 
---------------------------------------------------------------------- */
var start = new Date().getTime();
var matrix = "";
var NumberOfAttemps = 0;
function CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector) {
    var elapsed = new Date().getTime() - start;
    if (matrix == "") {
        matrix++;
        matrix = elapsed;
    }
    else {
        matrix = matrix + ":" + elapsed;
    }

    NumberOfAttemps++;

    if (correctAnswerSelector.is(targetClickedSelector)) {
        $(targetClickedSelector).addClass('rightAnswer');
        return [NumberOfAttemps, "[" + matrix + "]"];
    }
    else {
        $(targetClickedSelector).attr('readonly', 'readonly');
        $(targetClickedSelector).addClass('wrongAnswer');
    }
}

/*---------------------------------------------------------------------- */

/*Find quesry string value by passing key 
---------------------------------------------------------------------- */
function getQueryStringValue(key) {
    // Find the key and everything up to the ampersand delimiter
    var value = RegExp("" + key + "[^&]+").exec(window.location.search);
    // Return the unescaped value minus everything starting from the equals sign or an empty string
    //return unescape(!!value ? value.toString().replace(/^[^=]+./, "") : "");
	return localStorage.getItem('userId');
}
/*---------------------------------------------------------------------- */

/*Get formatted date that needs to be saved in the score table
  Pass date object to this
---------------------------------------------------------------------- */
function formatDate(d) {
    var formattedDate = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
    return formattedDate;
}
/*---------------------------------------------------------------------- */


/*Get formatted date that needs to be saved in the score table
  Pass date object to this
---------------------------------------------------------------------- */
function sendScore(userid, vignette, qno, timestamp, attempts, matrix, gameid, handlerURL) {

    $.ajax({
        type: 'GET',
        url: handlerURL,
        data: {
            userid: userid,
            vignette: vignette,
            qno: qno,
            timestamp: timestamp,
            attempts: attempts,
            matrix: matrix,
            gameid: gameid
        },
        context: this,
        dataType: 'text',
        contentType: "application/json; charset=utf-8",
        success: function (msg) {
            //Below line needs to be removed
            //alert("Your score is added to database");
        },
        error: function () {
            //Below line needs to be removed
            //alert('error');
        }
    });
}
/*---------------------------------------------------------------------- */

function signIn(v) {
    var returnurl = '';
    var actualurl = pangoUrl;
    var returnpath = returnUrlPath;
    var popup;
    switch (v) {

        case 'v1':
            returnurl = 'http://' + returnpath + '/vignette1/Login.aspx';
            //window.location.href = actualurl + 'returnUrl=' + returnurl;
            popup = open(actualurl + 'returnUrl=' + returnurl, 'yeah', 'left=220,top=220,width=407,height=223,toolbar=0,resizable=0,menubar=0,location=0,status=0,address=0');
            break;
        case 'v2':
            returnurl = 'http://' + returnpath + '/vignette2/Login.aspx';
            popup = open(actualurl + 'returnUrl=' + returnurl, 'yeah', 'left=220,top=220,width=407,height=223,toolbar=0,resizable=0,menubar=0,location=0,status=0,address=0');
            break;
        case 'v3':
            returnurl = 'http://' + returnpath + '/vignette3/Login.aspx';
            popup = open(actualurl + 'returnUrl=' + returnurl, 'yeah', 'left=220,top=220,width=407,height=223,toolbar=0,resizable=0,menubar=0,location=0,status=0,address=0');
            break;
        case 'v4':
            returnurl = 'http://' + returnpath + '/vignette4/Login.aspx';
            popup = open(actualurl + 'returnUrl=' + returnurl, 'yeah', 'left=220,top=220,width=407,height=223,toolbar=0,resizable=0,menubar=0,location=0,status=0,address=0');
            break;
        case 'v5':
            returnurl = 'http://' + returnpath + '/vignette5/Login.aspx';
            popup = open(actualurl + 'returnUrl=' + returnurl, 'yeah', 'left=220,top=220,width=407,height=223,toolbar=0,resizable=0,menubar=0,location=0,status=0,address=0');
            break;

    }
}