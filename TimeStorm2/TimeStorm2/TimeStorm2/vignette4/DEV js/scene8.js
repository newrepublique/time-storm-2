﻿$(document).ready(function () {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastScene', 'scene8');    //save game

    setTimeout(function () {
        playSound(43);
    }, delay8);

    setTimeout(function () {
        $('#subTitle04_139').fadeIn(250);
    }, 6000 + delay8);

    setTimeout(function () {
        $('#subTitle04_139').fadeOut(250);
    }, 11000 + delay8);

    setTimeout(function () {
        $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG04_background_16.jpg)').stop(true, true).hide().fadeIn(1000);

    }, 12000 + delay8);

    setTimeout(function () {

        $('#TScompleted,#TSprogress').show();
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_08.png)');

    }, 13000 + delay8);

    setTimeout(function () {
        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {

            case 'scene8_2': showQuestion1();
                break;
            case 'scene8_3': scene8_3();
                break;
            default: scene8_1();
        }
    }, 13000 + delay8);


});


function scene8_1() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene8_1');

    setTimeout(function () {

        $('#subTitle04_141').fadeIn(250);
        playSound(24);
    }, delay8);

    setTimeout(function () {
        $('#subTitle04_141').fadeOut(250);
    }, 12000 + delay8);

    setTimeout(function () {
        playSound(44);
    }, 13000 + delay8);

    setTimeout(function () {
        $('#propOverlayImage04_143').fadeIn(250);
        $('#sceneOver').show();
    }, 13000 + delay8);
    
    setTimeout(function () {
        $('#propOverlayImage04_143').fadeOut(250);
        $('#sceneOver').hide();

    }, 18000 + delay8);

    setTimeout(function () {
        showQuestion1();
    }, 19000 + delay8);

    
}


function showQuestion1() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene8_2');
    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q1radio3');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion04_144').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion04_144 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(7, result[0], result[1]);
                $('#propOverlayQuestion04_144 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion04_144').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion2();

                }, 1000);
            }

        }
    });
}


function showQuestion2() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q2radio2');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion04_147').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion04_147 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(8, result[0], result[1]);
                $('#propOverlayQuestion04_147 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion04_147').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion3();

                }, 1000);
            }

        }
    });
}

function showQuestion3() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q3radio4');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion04_150').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion04_150 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(9, result[0], result[1]);
                $('#propOverlayQuestion04_150 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion04_150').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion4();

                }, 1000);
            }

        }
    });
}

function showQuestion4() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q4radio4');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion04_153').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion04_153 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(10, result[0], result[1]);
                $('#propOverlayQuestion04_153 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion04_153').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    scene8_3();

                }, 1000);
            }

        }
    });
}

function sendResults(quesNo, NumberOfAttemps, Matrix) {
    userid = getQueryStringValue("userId");


    qno = quesNo;

    currentTime = new Date();
    timestamp = formatDate(currentTime);

    attempts = NumberOfAttemps;
    matrix = Matrix;


    setTimeout(function () {
        sendScore(userid, vignette, qno, timestamp, attempts, matrix, gameid, handlerURL);
    }, 1000);


}

function scene8_3() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene8_3');

    setTimeout(function () {

        $('#subTitle04_156_1').fadeIn(250);
        playSound(25);
    }, delay8);

    setTimeout(function () {
        $('#subTitle04_156_1').fadeOut(250);
        $('#subTitle04_156_2').fadeIn(250);
    }, 10000 + delay8);

    setTimeout(function () {
        setGameStatus('.per', '100', 'v4');
        $('#subTitle04_156_2').fadeOut(250);
    }, 14000 + delay8);

    setTimeout(function () {
        $("#TSscene").fadeOut(500);
        sessionStorage.setItem(userId + '_lastScene', 'scene1');    //save game
        $('.per').html('000');
        sessionStorage.setItem('status-line-li', ' ');
        localStorage.setItem('phonetext', '');
        localStorage.setItem(userId + '_phonetext', ' ');
        currentvignette = 'v5';                                             //change to v5 after deploying rest of the chapters.
        localStorage.setItem(userId + '_vignette', currentvignette);
        sessionStorage.setItem(userId + '_lastSceneSection', '');
    }, 15000 + delay8);

    setTimeout(function () {
        sendData();
    }, 16000 + delay8);


    setTimeout(function () {
        $("#endScreen").fadeIn(500);
    }, 17000 + delay8);

    setTimeout(function () {
        $("#endScreen").fadeOut(500);
        $("#preloaderGif").hide();

    }, 20000 + delay8);

    setTimeout(function () {
        window.location.href = '../vignette5/index.html';  //add link to next vignette after its deployment.

    }, 21000 + delay8);
}

