﻿$(document).ready(function () {
    sessionStorage.setItem(userId + '_lastScene', 'scene7');    //save game
    setTimeout(function () {
        playSound(41);
    }, delay7);

    setTimeout(function () {
        $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG04_background_15.jpg)').stop(true, true).hide().fadeIn(1000);

    }, 12000 + delay7);

    setTimeout(function () {

        $('#TScompleted,#TSprogress').show();
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_07.png)');

    }, 13000 + delay7);

    setTimeout(function () {
        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {

            case 'scene7_2': scene7_2();
                break;
            
            default: scene7_1();
        }
    }, 13000 + delay7);



    $('.propClose').click(function () {
        playSound('0');
        if (this.parentElement.id == 'propOverlayImage04_122') {
            $('#propOverlayImage04_122').fadeOut(500);
            $('#sceneOver').hide();
            sounds[42].pause();
            fadeloop('#objectHighlight04_124', 500, 500, 1000);
        }
        else if (this.parentElement.id == 'propOverlayImage04_127') {
            $('iframe').hide().attr('src', '');
            $('#propOverlayImage04_127').fadeOut(500);
            $('#sceneOver').hide();
            scene7_2();
        }
        else if (this.parentElement.id == 'propOverlayImage04_131') {
            
        }


    });
});



var clicked1 = false, clicked2 = false;

function scene7_1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene7_1');

    setTimeout(function () {
        updatePhoneTxt('You are on the old boat.', 'listIcon1');
    }, delay7);

    setTimeout(function () {

        $('#subTitle04_120').fadeIn();
        playSound(21);
    }, 1000 + delay7);

    setTimeout(function () {
        $('#subTitle04_120').fadeOut();
    }, 5000 + delay7);

    setTimeout(function () {
        //object highlight..
        fadeloop('#objectHighlight04_121', 500, 500, 1000);
    }, 6000 + delay7);


    $('#objectHighlight04_121').click(function () {
        playSound('0');
        $('#sceneOver').show();
        $('#propOverlayImage04_122').fadeIn(250);
        $('#cover1').hide();
        setTimeout(function () {
            playSound(42);
        }, delay7);

        setTimeout(function () {
            $('#propOverlayImage04_122').fadeOut(250);
        $('#sceneOver').hide();
        sounds[42].pause();
        fadeloop('#objectHighlight04_124', 500, 500, 1000);
        }, 11000 + delay7);
    });


   

    $('#objectHighlight04_124').click(function () {
        playSound('0');
        
        $('#cover2').hide();

        setTimeout(function () {

            $('#subTitle04_126').fadeIn(250);
            playSound(22);
        }, delay7);

        setTimeout(function () {
            $('#subTitle04_126').fadeOut(250);
          
            $('#sceneOver').show();
            $('#propOverlayImage04_127').fadeIn(250);
            $('iframe').css('display', 'block');
            $('iframe').attr('src', overlay_video_url);
        }, 7000 + delay7);

    });

    
  
    
}

function scene7_2() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene7_2');

    //setTimeout(function () {
    //    $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG04_background_15.jpg)').fadeOut(500);

    //    setTimeout(function () {
    //        $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG04_background_14.jpg)').fadeIn(500);
    //    }, 500);

    //}, delay7);



    setTimeout(function () {
        fadeloop('#objectHighlight04_130', 500, 500, 1000);
    }, delay7);


    $('#objectHighlight04_130').click(function () {
        playSound('0');
        $('#sceneOver').show();
        $('#propOverlayImage04_131').fadeIn(250);
        $('#cover3').hide();

        setTimeout(function () {
            $('#subTitle04_132').fadeIn(250);
        }, delay7);

        setTimeout(function () {
            $('#subTitle04_132').fadeOut(250);
        }, 3000 + delay7);

        setTimeout(function () {
            playSound(29);

            
        }, 4000 + delay7);

        setTimeout(function () {
            $('#propOverlayImage04_131').fadeOut(250);
            $('#sceneOver').hide();

            post();
        }, 11000 + delay7);

    });



}



function post() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));

    setTimeout(function () {
        $('#subTitle04_135').fadeIn(500);
        playSound(23);
    }, delay7);

    setTimeout(function () {
        $('#subTitle04_135').fadeOut(500);
        setGameStatus('.per', '090', 'v4');
    }, 4000 + delay7);

    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_08.png)');
        
        
    }, 5000 + delay7);
    setTimeout(function () {
        $("#TScontainer").fadeOut();
        $("#newContainer").load("scene8child.html");
    }, 6000 + delay7);

}

