﻿var nextdiv = '#Dummy';
var currentdiv = '#TScontainer';
$(document).ready(function () {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));

    sessionStorage.setItem(userId + '_lastScene', 'scene5');    //save game
    setTimeout(function () {
        playSound(34);
    }, delay5);

    setTimeout(function () {
        $('#subTitle04_074').fadeIn(250);
    }, 6000 + delay5);

    setTimeout(function () {
        $('#subTitle04_074').fadeOut(250);
    }, 13000 + delay5);

    setTimeout(function () {
        
        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG04_background_10.jpg)').stop(true, true).hide().fadeIn(1000);

    }, 14000 + delay5);

    setTimeout(function () {

        $('#TScompleted,#TSprogress').show();
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_05b.png)');

    }, 14000 + delay5);

    setTimeout(function () {
        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {

            case 'scene5_2': scene5_2();
                break;

            case 'scene5_3': scene5_3();
                break;

            default: scene5_1();
        }
    }, 14000 + delay5);


});

///
function scene5_1() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene5_1');
    setTimeout(function () {
        playSound(35);
    }, delay5);

    setTimeout(function () {

        $('#subTitle04_077_1').fadeIn(250);
        playSound(14);
    }, 12000 + delay5);

    setTimeout(function () {
        $('#subTitle04_077_1').fadeOut(250);
        $('#subTitle04_077_2').fadeIn(250);
    }, 25000 + delay5);

    setTimeout(function () {
        $('#subTitle04_077_2').fadeOut(250);
        $('#subTitle04_077_3').fadeIn(250);
    }, 36000 + delay5);

    setTimeout(function () {
        $('#subTitle04_077_3').fadeOut(250);
    }, 47500 + delay5);

    setTimeout(function () {
        scene5_2();
    }, 47500 + delay5);
}


function scene5_2() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene5_2');
    setTimeout(function () {
        //object highlight..
        fadeloop('#objectHighlight04_078_01', 500, 500, 1000);
        fadeloop('#objectHighlight04_078_02', 500, 500, 1000);
        fadeloop('#objectHighlight04_078_03', 500, 500, 1000);
    }, 0);



    $('#objectHighlight04_078_01').click(function () {
        playSound('0');
        $('#sceneOver').show();
        $('#propOverlayImage04_079').fadeIn(250);
        $('#cover1').hide();

    });

    $('#objectHighlight04_078_02').click(function () {
        playSound('0');
        $('#sceneOver').show();
        $('#propOverlayImage04_080').fadeIn(250);
        $('#cover2').hide();

    });

    $('#objectHighlight04_078_03').click(function () {
        playSound('0');
        $('#sceneOver').show();
        $('#propOverlayImage04_081').fadeIn(250);
        $('#cover3 , #cover2 , #cover1').hide();

        setTimeout(function () {
            playSound(15);
            $('#subTitle04_082').fadeIn(250);
        }, delay5);

        setTimeout(function () {
            $('#subTitle04_082').fadeOut(250);
        }, 7000 + delay5);

        setTimeout(function () {
            $('#subTitle04_083').fadeIn(250);
        }, 8000 + delay5);

        setTimeout(function () {
            $('#subTitle04_083').fadeOut(250);
        }, 13000 + delay5);

        setTimeout(function () {
            $('#sceneOver').hide();
            $('#propOverlayImage04_081').fadeOut(500);

        }, 14000 + delay5);

        setTimeout(function () {
            playSound(36);
        }, 15000 + delay5);

        setTimeout(function () {
            $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG04_background_10.jpg)').fadeOut(1500);

            setTimeout(function () {
                $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG04_background_11.jpg)').fadeIn(2000);

            }, 0);
            setTimeout(function () {
                swap();
            }, 2000);
        }, 20000 + delay5);

        setTimeout(function () {
            playSound(16);
            $('#subTitle04_086').fadeIn(250);
        }, 22000 + delay5);

        setTimeout(function () {
            $('#subTitle04_086').fadeOut(250);
        }, 29000 + delay5);

        setTimeout(function () {
            playSound(37);
        }, 30000 + delay5);

        setTimeout(function () {
            showQuestion4();
        }, 35000 + delay5);

    });

    $('.propClose').click(function () {
        playSound('0');
        $('#propOverlayImage04_079').fadeOut(500);
        $('#propOverlayImage04_080').fadeOut(500);
        $('#sceneOver').hide();

    });

}


function showQuestion4() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q4radio1');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion04_088').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion04_088 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(4, result[0], result[1]);
                $('#propOverlayQuestion04_088 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion04_088').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion5();

                }, 1000);
            }

        }
    });
}


function showQuestion5() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q5radio3');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion04_091').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion04_091 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(5, result[0], result[1]);
                $('#propOverlayQuestion04_091 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion04_091').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion6();

                }, 1000);
            }

        }
    });
}

function showQuestion6() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q6radio4');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion04_094').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion04_094 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(6, result[0], result[1]);
                $('#propOverlayQuestion04_094 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion04_094').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    scene5_3();

                }, 1000);
            }

        }
    });
}

function sendResults(quesNo, NumberOfAttemps, Matrix) {
    userid = getQueryStringValue("userId");


    qno = quesNo;

    currentTime = new Date();
    timestamp = formatDate(currentTime);

    attempts = NumberOfAttemps;
    matrix = Matrix;


    setTimeout(function () {
        sendScore(userid, vignette, qno, timestamp, attempts, matrix, gameid, handlerURL);
    }, 1000);


}

function scene5_3() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene5_3');
    setTimeout(function () {
        playSound(38);
    }, delay5);

    setTimeout(function () {
        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG04_background_11.jpg)').fadeOut(1500);

        setTimeout(function () {
            $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG04_background_17.png)').fadeIn(2000);
        }, 0);
        setTimeout(function () {
            swap();
        }, 2000);
    }, 6000 + delay5);

    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_06.png)');
    }, 7000 + delay5);

    setTimeout(function () {
        playSound(39);
        setGameStatus('.per', '070', 'v4');
    }, 7500 + delay5);

    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_06_.png)');
        $('#phoneMap04_099 , .phoneMap04_099_dot').show();
        fadeloop('.phoneMap04_099_dot', 200, 200, 500);
    }, 14500 + delay5);

    $('.phoneMap04_099_dot').click(function () {
        $('#phoneMap04_099 , .phoneMap04_099_dot').hide();
        playSound('0');
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_06.png)');
        $("#TScontainer, #Dummy").fadeOut();
        $("#newContainer").load("scene6child.html");

    });
}

function swap() {
    if (currentdiv == '#TScontainer') {
        currentdiv = '#Dummy';
        nextdiv = '#TScontainer';
    }
    else {
        nextdiv = '#Dummy';
        currentdiv = '#TScontainer';
    }
}