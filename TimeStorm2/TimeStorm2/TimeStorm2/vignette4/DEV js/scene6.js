﻿$(document).ready(function () {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));

    sessionStorage.setItem(userId + '_lastScene', 'scene6');    //save game
    setTimeout(function () {
        playSound(40);
    }, delay6);

    setTimeout(function () {
        $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG04_background_12.jpg)').stop(true, true).hide().fadeIn(1000);

    }, 11000 + delay6);

    setTimeout(function () {

        $('#TScompleted,#TSprogress').fadeIn(500);
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_06.png)');

    }, 12000 + delay6);

    setTimeout(function () {
        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {

            case 'scene6_2': scene6_2();
                break;
            
            default: scene6_1();
        }
    }, 12000 + delay6);


});
var clicked1 = false, clicked2 = false;

function scene6_1() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene6_1');
    updatePhoneTxt('You are on the jetty.', 'listIcon1');
    setTimeout(function () {

        $('#subTitle04_105').fadeIn(250);
        playSound(17);
    }, delay6);

    setTimeout(function () {
        $('#subTitle04_105').fadeOut(250);
    }, 6000 + delay6);

    setTimeout(function () {
        //object highlight..
        fadeloop('#objectHighlight04_106_01', 500, 500, 1000);
        fadeloop('#objectHighlight04_106_02', 500, 500, 1000);
    }, 7000 + delay6);


    $('#objectHighlight04_106_01').click(function () {
        playSound('0');
        $('#sceneOver').show();
        $('#propOverlayImage04_107').fadeIn(500);
        $('#cover1').hide();
        clicked1 = true;
        setTimeout(function () {

            $('#subTitle04_108_1').fadeIn(250);
            playSound(18);
        }, delay6);

        setTimeout(function () {
            $('#subTitle04_108_1').fadeOut(250);
            $('#subTitle04_108_2').fadeIn(250);
        }, 11000 + delay6);

        setTimeout(function () {
            $('#subTitle04_108_2').fadeOut(250);
            $('#propOverlayImage04_107').fadeOut(500);
            $('#sceneOver').hide();

            if (clicked1 && clicked2) {
                scene6_2();
            }
        }, 23000 + delay6);

    });

    $('#objectHighlight04_106_02').click(function () {
        playSound('0');
        $('#sceneOver').show();
        $('#propOverlayImage04_109').fadeIn(500);
        $('#cover2').hide();
        clicked2 = true;
        setTimeout(function () {

            $('#subTitle04_110_1').fadeIn(250);
            playSound(19);
        }, delay6);

        setTimeout(function () {
            $('#subTitle04_110_1').fadeOut(250);
            $('#subTitle04_110_2').fadeIn(250);
        }, 7000 + delay6);

        setTimeout(function () {
            $('#subTitle04_110_2').fadeOut(250);
            $('#propOverlayImage04_109').fadeOut(250);
            $('#sceneOver').hide();
            if (clicked1 && clicked2) {
                scene6_2();
            }
        }, 15000 + delay6);
        
    });

    $('.propClose').click(function () {
        playSound('0');
        $('#propOverlayImage04_107').fadeOut(250);
        $('#propOverlayImage04_109').fadeOut(250);
        

    });
    
}

function scene6_2() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene6_2');
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    setTimeout(function () {

        $('#subTitle04_111').fadeIn(250);
        playSound(20);
    }, delay6);

    setTimeout(function () {
        $('#subTitle04_111').fadeOut(250);
    }, 4000 + delay6);

    setTimeout(function () {
        $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG04_background_12.jpg)').fadeOut(1500);

        setTimeout(function () {
            $("#Dummy").hide().css('background-image', 'url(./images/timestorm_G02_VG04_background_14.jpg)').fadeIn(2000);
        }, 0);
        setTimeout(function () {
            $('#Dummy').attr('id', 'TScontainer');
            $("#TScontainer").attr('id', 'Dummy');
        }, 2000);
    }, 5000 + delay6);

    setTimeout(function () {
        $('#subTitle04_113').fadeIn(250);
    }, 7000 + delay6);

    setTimeout(function () {
        $('#subTitle04_113').fadeOut(250);
        setGameStatus('.per', '080', 'v4');
    }, 13000 + delay6);

    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_07_.png)');
        $('#phoneMap04_114 , .phoneMap04_114_dot').show();
        fadeloop('.phoneMap04_114_dot', 200, 200, 500);
    }, 14000 + delay6);

    $('.phoneMap04_114_dot').click(function () {
        $('#phoneMap04_114 , .phoneMap04_114_dot').hide();
        playSound('0');
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_07.png)');
        $("#TScontainer, #Dummy").fadeOut();
        $("#newContainer").load("scene7child.html");

    });
  

}

