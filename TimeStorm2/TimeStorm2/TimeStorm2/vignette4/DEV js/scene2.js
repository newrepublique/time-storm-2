﻿$(document).ready(function () {
    sessionStorage.setItem(userId + '_lastScene', 'scene2');    //save game
    setTimeout(function () {
        playSound(28);
    }, delay2);

    setTimeout(function () {
        $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG04_background_03.jpg)').stop(true,true).hide().fadeIn(1000);

    }, 2000 + delay2);

    setTimeout(function () {

        $('#TScompleted,#TSprogress').show();
    }, 3000 + delay2);

    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_03.png)');

    }, 3000 + delay2);

    setTimeout(function () {

        scene2_1();

    }, 3000 + delay2);


});

function scene2_1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    setTimeout(function () {
        $('#subTitle04_021_1').fadeIn(250);
        playSound(5);
    }, delay2);

    setTimeout(function () {
        $('#subTitle04_021_1').fadeOut(250);
        $('#subTitle04_021_2').fadeIn(250);
    }, 7000 + delay2);

    setTimeout(function () {
        $('#subTitle04_021_2').fadeOut(250);
    }, 12000 + delay2);

    setTimeout(function () {
        //object highlight..
        fadeloop('#objectHighlight04_022_01', 500, 500, 1000);

    }, 13000 + delay2);

    $('#objectHighlight04_022_01').click(function () {
        playSound('0');
        $('#cover').hide();
        $('#propOverlayImage04_024').fadeIn(250);
        $('#sceneOver').show();

    });

    $('.propClose').click(function () {
        playSound('0');
        $('#sceneOver').hide();
        $('#propOverlayImage04_024').fadeOut(250);
        setTimeout(function () {
            $('#subTitle04_025_1').fadeIn(250);
            playSound(6);
        }, delay2);

        setTimeout(function () {
            $('#subTitle04_025_1').fadeOut(250);
            $('#subTitle04_025_2').fadeIn(250);

        }, 8000 + delay2);

        setTimeout(function () {
            $('#subTitle04_025_2').fadeOut(250);
            $('#subTitle04_025_3').fadeIn(250);

        }, 16000 + delay2);
        setTimeout(function () {
            $('#subTitle04_025_3').fadeOut(250);
            $('#subTitle04_025_4').fadeIn(250);

        }, 23000 + delay2);

        setTimeout(function () {
            $('#subTitle04_025_4').fadeOut(250);

        }, 27000 + delay2);
        
        setTimeout(function () {
            playSound(29);
            setGameStatus('.per', '020', 'v4');
        }, 28000 + delay2);

        setTimeout(function () {
            $("#TScontainer").fadeOut(500);
            $("#newContainer").load("scene3child.html");
        }, 34000 + delay2);

    });

    

}