﻿$(document).ready(function () {
    $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG04_background_01.jpg)').stop(true,true).hide().fadeIn(1000);
    sessionStorage.setItem(userId + '_lastScene', 'scene1');    //save game

    
    setTimeout(function () {
        
        $('#TScompleted,#TSprogress').fadeIn(1000);
    }, 1000 + delay1);

    setTimeout(function () {
        
        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {

            case 'scene1_2': scene1_2();
                break;

            default: scene1_1();
        }

    }, 2000 + delay1);


});


function scene1_1() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene1_1');
    
    setTimeout(function () {
        playSound(26);
    },0);

    setTimeout(function () {
        updatePhoneTxt('You are in The Archive.', 'listIcon1');
    }, 6000 + delay1);
    setTimeout(function () {
        updatePhoneTxt('Retrieving map from cache.', 'listIcon1');
    }, 7500 + delay1);

    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_01.png)');
        
    }, 9000 + delay1);

    
    setTimeout(function () {
        $('#subTitle04_007').fadeIn(250);
        playSound(1);
    }, 10000 + delay1);

    setTimeout(function () {
        $('#subTitle04_007').fadeOut(250);
        
    }, 17000 + delay1);

    setTimeout(function () {
        $('#subTitle04_008').fadeIn(250);
        playSound(2);
    }, 18000 + delay1);

    setTimeout(function () {
        $('#subTitle04_008').fadeOut(250);
        
    }, 19000 + delay1);

    setTimeout(function () {
        playSound(27);
    }, 20000 + delay1);

    setTimeout(function () {
        $('#subTitle04_010_1').fadeIn(250);
        playSound(3);
    }, 23000 + delay1);

    setTimeout(function () {
        $('#subTitle04_010_1').fadeOut(250);
        setTimeout(function () {
            $('#subTitle04_010_2').fadeIn(250);
        }, 500);
    }, 33000 + delay1);

    setTimeout(function () {
        $('#subTitle04_010_2').fadeOut(250);

    }, 40000 + delay1);

    setTimeout(function () {
        scene1_2();

    }, 40000 + delay1);


}


function scene1_2() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene1_2');
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    setTimeout(function () {
        $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG04_background_01.jpg)').fadeOut(1500);

        setTimeout(function () {
            $("#Dummy").hide().css('background-image', 'url(./images/timestorm_G02_VG04_background_02.jpg)').fadeIn(2000);
            setTimeout(function () {
                $('#Dummy').attr('id', 'TScontainer');
                $("#TScontainer").attr('id', 'Dummy');
            }, 2000);
        }, 0);

    }, delay1);

    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_02.png)');

    }, 2000 + delay1);

    setTimeout(function () {
        $('#subTitle04_013').fadeIn(250);
        playSound(4);
    }, 3000 + delay1);

    setTimeout(function () {
        $('#subTitle04_013').fadeOut(250);
        setGameStatus('.per', '010', 'v4');
    }, 8000 + delay1);

    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_02_.png)');
        $('#phoneMap04_012 , .phoneMap04_012_dot').show();
        fadeloop('.phoneMap04_012_dot', 200, 200, 500);
    }, 9000 + delay1);
    

    $('.phoneMap04_012_dot').click(function () {
        $('#phoneMap04_012 , .phoneMap04_012_dot').hide();
        playSound('0');
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_02.png)');
        $("#TScontainer, #Dummy").fadeOut();
        $("#newContainer").load("scene2child.html");
       
    });


}

