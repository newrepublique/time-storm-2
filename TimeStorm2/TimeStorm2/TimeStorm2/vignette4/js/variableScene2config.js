﻿var gender = 'male';
if (localStorage.getItem(userId + '_gender') != null) {
    gender = localStorage.getItem(userId + '_gender');
}

var delay2 = 1000;

var voiceover05_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG04_VO_05.wav";
var voiceover05_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG04_VO_05.mp3";
var voiceover05_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG04_VO_05.ogg";

var voiceover06_sound_file_url_wav = "";//sounds/voiceovers/timestorm_G02_VG04_VO_06.wav";
var voiceover06_sound_file_url_mp3 = "sounds/voiceovers/timestorm_G02_VG04_VO_06.mp3";
var voiceover06_sound_file_url_ogg = "sounds/voiceovers/timestorm_G02_VG04_VO_06.ogg";

var scamper_sound_file_url_wav = "";//sounds/sfx/timestorm_G02_VG04_soundfx_03.wav";
var scamper_sound_file_url_mp3 = "sounds/sfx/timestorm_G02_VG04_soundfx_03.mp3";
var scamper_sound_file_url_ogg = "sounds/sfx/timestorm_G02_VG04_soundfx_03.ogg";

var vortex_sound_file_url_wav = "";//sounds/sfx/timestorm_G02_VG04_soundfx_04.wav";
var vortex_sound_file_url_mp3 = "sounds/sfx/timestorm_G02_VG04_soundfx_04.mp3";
var vortex_sound_file_url_ogg = "sounds/sfx/timestorm_G02_VG04_soundfx_04.ogg";
