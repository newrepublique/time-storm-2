﻿var nextdiv = '#Dummy';
var currentdiv = '#TScontainer';
$(document).ready(function () {

    sessionStorage.setItem(userId + '_lastScene', 'scene4');    //save game
    setTimeout(function () {
        playSound(31);
    }, delay4);

    setTimeout(function () {
        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG04_background_07.jpg)').stop(true, true).hide().fadeIn(1000);

    }, 10000 + delay4);

    setTimeout(function () {

        $('#TScompleted,#TSprogress').show();
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_04.png)');

    }, 11000 + delay4);

    setTimeout(function () {
        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {

            case 'scene4_2': scene4_2();
                break;
            case 'scene4_3': scene4_3();
                break;
            default: scene4_1();
        }
    }, 11000 + delay4);


});

function scene4_1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene4_1');
    setTimeout(function () {
        playSound(32);
    }, delay4);

    setTimeout(function () {
        updatePhoneTxt('You are at the protester’s camp.', 'listIcon1');
    }, 5000 + delay4);

    setTimeout(function () {

        $('#subTitle04_051_1').fadeIn(250);
        playSound(13);
    }, 6000 + delay4);

    setTimeout(function () {
        $('#subTitle04_051_1').fadeOut(250);
        $('#subTitle04_051_2').fadeIn(250);
    }, 13000 + delay4);

    setTimeout(function () {
        $('#subTitle04_051_2').fadeOut(250);
    }, 20000 + delay4);

    setTimeout(function () {
        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG04_background_07.jpg)').fadeOut(1500);

        setTimeout(function () {
            $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG04_background_08.jpg)').fadeIn(2000);
            setTimeout(function () {
                swap();
            }, 2000);
        }, 0);

    }, 21000 + delay4);

    setTimeout(function () {
        //object highlight..
        fadeloop('#objectHighlight04_053_01', 500, 500, 1000);
        fadeloop('#objectHighlight04_053_02', 500, 500, 1000);
    }, 23000 + delay4);

    var clicked = 0;
    $('#objectHighlight04_053_01').click(function () {
        playSound('0');
        $('#sceneOver').show();
        $('#propOverlayImage04_054').fadeIn(500);
        clicked++;
        $('#cover1').hide();

    });

    $('#objectHighlight04_053_02').click(function () {
        playSound('0');
        $('#sceneOver').show();
        $('#propOverlayImage04_055').fadeIn(500);
        clicked++;
        $('#cover2').hide();

    });

    $('.propClose').click(function () {
        playSound('0');
        $('#propOverlayImage04_054').fadeOut(500);
        $('#propOverlayImage04_055').fadeOut(500);
        $('#sceneOver').hide();
        if (clicked == 2) {
            scene4_2();
        }
    });

}

function scene4_2() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene4_2');
    setTimeout(function () {
        $('#propOverlayImage04_057').fadeIn(500);
    }, delay4);

    setTimeout(function () {
        playSound(33);
    }, 1000 + delay4);

    setTimeout(function () {
        $('#propOverlayImage04_057').fadeOut(500);
        $('#sceneOver').hide();

        setTimeout(function () {
            showQuestion1();
        }, 1000);
    }, 5000 + delay4);

}


function showQuestion1() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q1radio3');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion04_059').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion04_059 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(1, result[0], result[1]);
                $('#propOverlayQuestion04_059 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion04_059').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion2();

                }, 1000);
            }

        }
    });
}


function showQuestion2() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q2radio1');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion04_062').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion04_062 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(2, result[0], result[1]);
                $('#propOverlayQuestion04_062 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion04_062').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion3();

                }, 1000);
            }

        }
    });
}

function showQuestion3() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q3radio4');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion04_065').fadeIn(500);
    $('#sceneOver').show();

    $('#propOverlayQuestion04_065 .ansTitle ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(3, result[0], result[1]);
                $('#propOverlayQuestion04_065 .ansTitle ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion04_065').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    scene4_3();

                }, 1000);
            }

        }
    });
}

function sendResults(quesNo, NumberOfAttemps, Matrix) {
    userid = getQueryStringValue("userId");


    qno = quesNo;

    currentTime = new Date();
    timestamp = formatDate(currentTime);

    attempts = NumberOfAttemps;
    matrix = Matrix;


    setTimeout(function () {
        sendScore(userid, vignette, qno, timestamp, attempts, matrix, gameid, handlerURL);
    }, 1000);


}

function scene4_3() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene4_3');
    setTimeout(function () {
        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG04_background_08.jpg)').fadeOut(1500);

        setTimeout(function () {
            $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG04_background_09.jpg)').fadeIn(2000);
        }, 0);
        setTimeout(function () {
            swap();
        }, 2000);
    }, delay4);

    setTimeout(function () {
        playSound(7);
        $('#subTitle04_069').fadeIn(250);
    }, 2000 + delay4);

    setTimeout(function () {
        $('#subTitle04_069').fadeOut(250);
        setGameStatus('.per', '050', 'v4');
    }, 7000 + delay4);

    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_05_.png)');
        $('#phoneMap04_070 , .phoneMap04_070_dot').show();
        fadeloop('.phoneMap04_070_dot', 200, 200, 500);
    }, 7500 + delay4);

    $('.phoneMap04_070_dot').click(function () {
        playSound('0');
        $('#phoneMap04_070 , .phoneMap04_070_dot').hide();
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_05.png)');
        $("#TScontainer, #Dummy").fadeOut();
        $("#newContainer").load("scene5child.html");

    });
}

function swap() {
    if (currentdiv == '#TScontainer') {
        currentdiv = '#Dummy';
        nextdiv = '#TScontainer';
    }
    else {
        nextdiv = '#Dummy';
        currentdiv = '#TScontainer';
    }
}
