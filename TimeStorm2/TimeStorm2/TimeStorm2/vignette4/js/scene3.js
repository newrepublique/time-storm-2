﻿var currentdiv = '#TScontainer';
var nextdiv = '#Dummy';
$(document).ready(function () {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));

    $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG04_background_04.jpg)').stop(true, true).hide().fadeIn(1000);
    sessionStorage.setItem(userId + '_lastScene', 'scene3');    //save game


    setTimeout(function () {

        $('#TScompleted,#TSprogress').show();
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_03b.png)');
    }, 1000 + delay3);

    setTimeout(function () {

        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {

            case 'scene3_2': scene3_2();
                break;

            default: scene3_1();
        }

    }, 1000 + delay3);


});


function scene3_1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene3_1');

    setTimeout(function () {
        playSound(30);
    }, delay3);

    setTimeout(function () {
        $('#subTitle04_030').fadeIn(250);

    }, 10000 + delay3);
    setTimeout(function () {
        $('#subTitle04_030').fadeOut(250);

    }, 17000 + delay3);
    setTimeout(function () {

        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG04_background_04.jpg)').fadeOut(1500);

        setTimeout(function () {
            $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG04_background_05.jpg)').fadeIn(2000);
            setTimeout(function () {
                swap();
            }, 2000);
        }, 0);

    }, 18000 + delay3);

    setTimeout(function () {
        updatePhoneTxt('You are on The Franklin River in Tasmania.', 'listIcon1');
    }, 20000 + delay3);

    setTimeout(function () {
        $('#subTitle04_033').fadeIn(250);
        playSound(8);
    }, 21000 + delay3);
    setTimeout(function () {
        $('#subTitle04_033').fadeOut(250);

    }, 25000 + delay3);

    setTimeout(function () {
        $('#subTitle04_034').fadeIn(250);
        playSound(9);
    }, 26000 + delay3);
    setTimeout(function () {
        $('#subTitle04_034').fadeOut(250);

    }, 30000 + delay3);

    setTimeout(function () {
        $('#subTitle04_035_1').fadeIn(250);
        playSound(10);
    }, 31000 + delay3);

    setTimeout(function () {
        $('#subTitle04_035_1').fadeOut(250);
        $('#subTitle04_035_2').fadeIn(250);
    }, 37000 + delay3);

    setTimeout(function () {
        $('#subTitle04_035_2').fadeOut(250);
        $('#subTitle04_035_3').fadeIn(250);
    }, 43000 + delay3);

    setTimeout(function () {
        $('#subTitle04_035_3').fadeOut(250);
        $('#subTitle04_035_4').fadeIn(250);
    }, 51000 + delay3);

    setTimeout(function () {
        $('#subTitle04_035_4').fadeOut(250);
        $('#subTitle04_035_5').fadeIn(250);
    }, 60000 + delay3);
    setTimeout(function () {
        $('#subTitle04_035_5').fadeOut(250);
    }, 70000 + delay3);

    setTimeout(function () {
        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG04_background_05.jpg)').fadeOut(1500);

        setTimeout(function () {
            $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG04_background_06.jpg)').fadeIn(2000);
            setTimeout(function () {
                swap();
            }, 2000);
        }, 0);

    }, 71000 + delay3);
    setTimeout(function () {

        scene3_2();
    }, 73000 + delay3);

}


function scene3_2() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene3_2');
    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG04_background_06.jpg)');

    setTimeout(function () {
        $('#subTitle04_038').fadeIn(250);
        playSound(11);
    }, delay3);
    setTimeout(function () {
        $('#subTitle04_038').fadeOut(250);

    }, 4500 + delay3);

    setTimeout(function () {
        //object highlight..
        fadeloop('#objectHighlight04_037_01', 500, 500, 1000);

    }, 5000 + delay3);

    $('#objectHighlight04_037_01').click(function () {
        $('#subTitle04_038').hide();
        sounds[11].pause();
        playSound('0');
        $('#cover').hide();
        $('#propOverlayImage04_040').fadeIn(500);
        $('#sceneOver').show();

        setTimeout(function () {
            $('#subTitle04_041').fadeIn(250);
            playSound(12);
        }, delay3);
        setTimeout(function () {
            $('#subTitle04_041').fadeOut(250);
            $('#propOverlayImage04_040').fadeOut(500);
            $('#sceneOver').hide();
        }, 6500 + delay3);

        setTimeout(function () {
            $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG04_background_06.jpg)').fadeOut(1500);

            setTimeout(function () {
                $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG04_background_04.jpg)').fadeIn(2000);
            }, 0);
            setTimeout(function () {
                swap();
            }, 2000);
            setGameStatus('.per', '030', 'v4');
        }, 7500 + delay3);
        setTimeout(function () {
            $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_04_.png)');
            $('#phoneMap04_044 , .phoneMap04_044_dot').show();

            fadeloop('.phoneMap04_044_dot', 200, 200, 500);
        }, 9500 + delay3);

        $('.phoneMap04_044_dot').click(function () {
            playSound('0');
            $('#phoneMap04_044 , .phoneMap04_044_dot').hide();
            $("#TScontainer, #Dummy").fadeOut();
            $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG04_phonemap_04.png)');

            $("#newContainer").load("scene4child.html");

        });
    });

}


function swap() {
    if (currentdiv == '#TScontainer') {
        currentdiv = '#Dummy';
        nextdiv = '#TScontainer';
    }
    else {
        nextdiv = '#Dummy';
        currentdiv = '#TScontainer';
    }
}