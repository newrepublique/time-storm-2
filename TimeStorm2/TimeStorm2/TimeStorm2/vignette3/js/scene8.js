﻿$(document).ready(function () {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_02.jpg)').stop(true, true).hide().fadeIn(1000);
    sessionStorage.setItem(userId + '_lastScene', 'scene8');    //save game
   Time(localStorage.getItem(userId + '_timerval'));
    $('#TScompleted,#TSprogress').show();
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_01.png)');  //from scene7


    setTimeout(function () {
        updatePhoneTxt('You are back in The Lobby', 'listIcon1');
    
    }, 1000+delay8);


    setTimeout(function () {
        playSound(21);
        $('#subTitle03_103').fadeIn(250);
    }, 2000 + delay8);

    setTimeout(function () {

        $('#subTitle03_103').fadeOut(250);
        task_complete = true;

    }, 8000 + delay8);


    //announcement
    setTimeout(function () {
        playSound(42);
    }, 9000 + delay8);

    //people requesting
    setTimeout(function () {
        playSound(22);
        $('#subTitle03_105_1').fadeIn(250);
    }, 15500 + delay8);

    setTimeout(function () {

        $('#subTitle03_105_1').fadeOut(250);
    }, 23500 + delay8);

    //crossfades
    setTimeout(function () {


        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_02.jpg)').fadeOut(1500);
        setTimeout(function () {
            $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG03_background_01.jpg)').fadeIn(2000);
        }, 0);
        setTimeout(function () {
            swap();
        }, 2000);
    }, 24500 + delay8);

    setTimeout(function () {
        setGameStatus('.per', '070', 'v3');
    }, 26500 + delay8);



    setTimeout(function () {

        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {


            case 'scene8_2':



                scene8_2();
                break;


            default: scene8_1();

        }

    }, 27500 + delay8);
  


});


function scene8_1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene8_1');
    localStorage.setItem('timer', localStorage.getItem(userId + '_timerval'));
   
    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector2 = $('#ans1_3');
    var targetClickedSelector2, targetClickedId, result1;
    $('#propOverlayQuestion03_108').fadeIn(500);
    $('#sceneOver').show();
    $('#Queans1 ul li span').click(function (event) {
        
        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector2 = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');


            result1 = CalculateAttemptsAndAMtrix(correctAnswerSelector2, targetClickedSelector2);


            if (result1 != undefined && result1.length == 2) {
                sendResults(1, result1[0], result1[1]);
                $('#Queans1 ul li span').attr('readonly', 'readonly');
                setTimeout(function () {

                    $('#sceneOver').hide();
                    $('#propOverlayQuestion03_108').fadeOut(500);

                    displayque2();
                }, delay8);
            }

        }
    });

}

function sendResults(quesNo, NumberOfAttemps, Matrix) {
    userid = getQueryStringValue("userId");

    qno = quesNo;
    currentTime = new Date();
    timestamp = formatDate(currentTime);
    attempts = NumberOfAttemps;
    matrix = Matrix;


    setTimeout(function () {
        sendScore(userid, vignette, qno, timestamp, attempts, matrix, gameid, handlerURL);
    }, 1000);

}


function displayque2() {
    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector2 = $('#ans2_1');
    var targetClickedSelector2, targetClickedId, result1;
    $('#propOverlayQuestion03_109').fadeIn(250);
    $('#sceneOver').show();
    $('#Queans2 ul li span').click(function (event) {
        
        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector2 = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');


            result1 = CalculateAttemptsAndAMtrix(correctAnswerSelector2, targetClickedSelector2);


            if (result1 != undefined && result1.length == 2) {
                sendResults(2, result1[0], result1[1]);
                $('#Queans2 ul li span').attr('readonly', 'readonly');
                setTimeout(function () {

                    $('#sceneOver').hide();
                    $('#propOverlayQuestion03_109').fadeOut(250);

                    displayque3();
                }, delay8);
            }

        }
    });

}

function displayque3() {
    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector2 = $('#ans3_1');
    var targetClickedSelector2, targetClickedId, result1;
    $('#propOverlayQuestion03_110').fadeIn(250);
    $('#sceneOver').show();
    $('#Queans3 ul li span').click(function (event) {
        
        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector2 = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');


            result1 = CalculateAttemptsAndAMtrix(correctAnswerSelector2, targetClickedSelector2);


            if (result1 != undefined && result1.length == 2) {
                sendResults(3, result1[0], result1[1]);
                $('#Queans3 ul li span').attr('readonly', 'readonly');
                setTimeout(function () {

                    $('#sceneOver').hide();
                    $('#propOverlayQuestion03_110').fadeOut(250);

                    displayque4();
                }, delay8);
            }

        }
    });

}

function displayque4() {
    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector2 = $('#ans4_3');
    var targetClickedSelector2, targetClickedId, result1;
    $('#propOverlayQuestion03_111').fadeIn(250);
    $('#sceneOver').show();
    $('#Queans4 ul li span').click(function (event) {
        
        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector2 = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');


            result1 = CalculateAttemptsAndAMtrix(correctAnswerSelector2, targetClickedSelector2);


            if (result1 != undefined && result1.length == 2) {
                sendResults(4, result1[0], result1[1]);
                $('#Queans4 ul li span').attr('readonly', 'readonly');
                setTimeout(function () {

                    $('#sceneOver').hide();
                    $('#propOverlayQuestion03_111').fadeOut(250);

                    scene8_2();
                }, delay8);
            }

        }
    });
}

function scene8_2() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene8_2');
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_01.png)');  //from scene7
    localStorage.setItem('timer', localStorage.getItem(userId + '_timerval'));
    //crossfades
    setTimeout(function () {
        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_01.jpg)').fadeOut(1500);
        setTimeout(function () {
            $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG03_background_13.jpg)').fadeIn(2000);
        }, 0);
        setTimeout(function () {
            swap();
        }, 2000);
    }, delay8);

    setTimeout(function () {
        playSound(23);
        $('#subTitle03_113_1').fadeIn(250);

    }, 2000 + delay8);

    setTimeout(function () {

        $('#subTitle03_113_1').fadeOut(250);
    }, 13000 + delay8);

    setTimeout(function () {

        setGameStatus('.per', '100', 'v3');

    }, 14000 + delay8);

    setTimeout(function () {

        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_13.jpg)').fadeOut(500);

    }, 15000 + delay8);

    //remove the comments when the next vignette i.e vig 04 is ready
    //for next vignette 
    setTimeout(function () {
        $("#TSscene").fadeOut(500);
        sessionStorage.setItem(userId + '_lastScene', 'scene1');    //save game
        $('.per').html('000');
        sessionStorage.setItem('status-line-li', ' ');
        localStorage.setItem('phonetext', '');
        localStorage.setItem(userId + '_phonetext', ' ');
        currentvignette = 'v4';                                             //change to v4 after deploying rest of the chapters.
        localStorage.setItem(userId + '_vignette', currentvignette);
        sessionStorage.setItem(userId + '_lastSceneSection', '');
    }, 16000 + delay8);

    setTimeout(function () {
        sendData();
    }, 17000 + delay8);


    setTimeout(function () {
        $("#endScreen").fadeIn(500);
    }, 18000 + delay8);

    setTimeout(function () {
        $("#endScreen").fadeOut(500);
        $("#preloaderGif").hide();
       
    }, 21000 + delay8);

    setTimeout(function () {
        window.location.href = '../vignette4/index.html';  //add link to next vignette after its deployment.

    }, 22000 + delay8);
}