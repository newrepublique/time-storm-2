﻿$(document).ready(function () {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));

    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_06.jpg)').stop(true, true).hide().fadeIn(1000);
    sessionStorage.setItem(userId + '_lastScene', 'scene5');    //save game
    Time(localStorage.getItem(userId + '_timerval'));
    $('#TScompleted,#TSprogress').show();
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_05_.png)'); //from scene 4
    setTimeout(function () {


        setGameStatus('.per', '030', 'v3');
    }, 1000 + delay5);

    setTimeout(function () {
        playSound(34);

    }, 2000 + delay5);

    setTimeout(function () {
        updatePhoneTxt('You are in the press room', 'listIcon1');

    }, 8000 + delay5);


    setTimeout(function () {
        playSound(35);
    }, 9000 + delay5);

    setTimeout(function () {

        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {


            case 'scene5_2':


                scene5_2();
                break;
            case 'scene5_3':


                scene5_3();
                break;

            default: scene5_1();

        }

    }, 14500);



});


function scene5_1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene5_1');
    localStorage.setItem('timer', localStorage.getItem(userId + '_timerval'));
    //journos
    setTimeout(function () {
        playSound(9);
        $('#subTitle03_060').fadeIn(250);
    }, delay5);

    setTimeout(function () {

        $('#subTitle03_060').fadeOut(250);
    }, 4000 + delay5);


    //sorry
    setTimeout(function () {
        playSound(10);
        $('#subTitle03_061').fadeIn(250);

    }, 5000 + delay5);

    setTimeout(function () {

        $('#subTitle03_061').fadeOut(250);

    }, 7500 + delay5);


    //large1
    setTimeout(function () {
        playSound(11);
        $('#subTitle03_062_1').fadeIn(250);
    }, 8500 + delay5);

    setTimeout(function () {

        $('#subTitle03_062_1').fadeOut(250);
        $('#subTitle03_062_2').fadeIn(250);

    }, 13500 + delay5);



    setTimeout(function () {

        $('#subTitle03_062_2').fadeOut(250);
        $('#subTitle03_062_3').fadeIn(250);
    }, 19500 + delay5);



    setTimeout(function () {

        $('#subTitle03_062_3').fadeOut(250);
        $('#subTitle03_062_4').fadeIn(250);

    }, 29500 + delay5);


    setTimeout(function () {

        $('#subTitle03_062_4').fadeOut(250);
        $('#subTitle03_062_5').fadeIn(250);

    }, 34000 + delay5);


    setTimeout(function () {

        $('#subTitle03_062_5').fadeOut(250);
        $('#subTitle03_062_6').fadeIn(250);

    }, 42500 + delay5);



    setTimeout(function () {

        $('#subTitle03_062_6').fadeOut(250);

    }, 51500 + delay5);
    //large1 ended

    setTimeout(function () {
        scene5_2();

    }, 52500 + delay5);

}


function scene5_2() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene5_2');
    localStorage.setItem('timer', localStorage.getItem(userId + '_timerval'));
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_05_.png)'); //from scene 4

    $('#objectHighlight03_063_01 , #objectHighlight03_063_02 ').show();

    fadeloop('#objectHighlight03_063_01', 500, 500, 1000);      //book
    fadeloop('#objectHighlight03_063_02', 500, 500, 1000);      //computer



    // computer
    $('#objectHighlight03_063_02').click(function () {
        playSound('0');
        clicked1 = true;
        $('#sceneOver').show();

        $('#close2').hide();
        $('#propOverlayImage03_064').fadeIn(250);
        $('.propClose').show();
    });



    //book
    $('#objectHighlight03_063_01').click(function () {

        playSound('0');

        clicked2 = true;
        $('#sceneOver').show();
        $('#close1').hide();
        $('#propOverlayImage03_065').fadeIn(250);
        $('.propClose').show();
    });


    $('.propClose').click(function () {
        playSound('0');
        $('#propOverlayImage03_064').fadeOut(250);
        $('#propOverlayImage03_065').fadeOut(250);
        $('#sceneOver').hide();
        checkclick();

    });
}

function checkclick() {
    //check if 1 object is clicked

    if (clicked2) {

        $('#close1,#close2').hide();
        setTimeout(function () {

            playSound(12);
            $('#subTitle03_066').fadeIn(250);

        }, delay5);



        setTimeout(function () {

            $('#subTitle03_066').fadeOut(250);

        }, 4500 + delay5);


        //crossfades
        setTimeout(function () {


            $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_06.jpg)').fadeOut(1500);
            setTimeout(function () {
                $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG03_background_07.jpg)').fadeIn(2000);
            }, 0);
            setTimeout(function () {
                swap();
            }, 2000);
        }, 5500 + delay5);

        setTimeout(function () {

            scene5_3();

        }, 6500 + delay5);  //2 sec after crossfade,5_3() starts with 1 sec delay=total 2

    }

}


function scene5_3() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    localStorage.setItem('timer', localStorage.getItem(userId + '_timerval'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene5_3');
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_05_.png)'); //from scene 4
    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_07.jpg)');    //save crossfaded bg

    setTimeout(function () {
        playSound(13);
        $('#subTitle03_068').fadeIn(250);

    }, delay5);



    setTimeout(function () {

        $('#subTitle03_068').fadeOut(250);

    }, 7000 + delay5);


    setTimeout(function () {
        playSound(36);
    }, 8000 + delay5);

    setTimeout(function () {
        playSound(14);
        $('#subTitle03_070').fadeIn(250);

    }, 10500 + delay5);



    setTimeout(function () {

        $('#subTitle03_070').fadeOut(250);

    }, 17500 + delay5);


    //phonemap updates
    setTimeout(function () {
        $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_05_.png)');
        $('#phoneMap03_071').show();
        $('.phoneMap03_071_dot').show();
        fadeloop('.phoneMap03_071_dot', 200, 200, 500);
    }, 18500 + delay5);


    //QC

    $('.phoneMap03_071_dot').click(function () {
        $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_06_.png)');
        playSound('0');
        $(currentdiv).fadeOut(250);
        $('#phoneMap03_071').hide();
        $('.phoneMap03_071_dot').hide();
        setTimeout(function () {

            $('#subTitle03_072_1').fadeIn(250);

        }, 1000 + delay5);



        setTimeout(function () {

            $('#subTitle03_072_1').fadeOut(250);

        }, 10000 + delay5);


        //play walking in corridor

        setTimeout(function () {
            playSound(37);
        }, 1000 + delay5);

        setTimeout(function () {

            $('#newContainer').load('scene6child.html');

        }, 11000 + delay5);
    });


}