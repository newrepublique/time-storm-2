﻿$(document).ready(function () {

    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_01.jpg)').stop(true, true).hide().fadeIn(1000);
    sessionStorage.setItem(userId + '_lastScene', 'scene1');    //save game

    setTimeout(function () {
        $('#TScompleted,#TSprogress').fadeIn(1000);
    }, 1000 + delay1);


    setTimeout(function () {

        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {


            case 'scene1_2': 

                scene1_2();
                break;

            default: scene1_1();


        }


    }, 2000 + delay1);
});
function scene1_1() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene1_1');
    
    setTimeout(function () {
        playSound(24);
    }, delay1);

    setTimeout(function () {

        updatePhoneTxt('You are in the High Court Lobby', 'listIcon1');

    }, 8000 + delay1);


    setTimeout(function () {

        $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_01.png)');
    }, 9000 + delay1);

    setTimeout(function () {

        $('#subTitle03_006').fadeIn(250);
    }, 10000 + delay1);

    setTimeout(function () {

        $('#subTitle03_006').fadeOut(250);
    }, 14000 + delay1);


    //crossfades
    setTimeout(function () {
        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_01.jpg)').fadeOut(1500);

        setTimeout(function () {
            $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG03_background_02.jpg)').fadeIn(2000);
        }, 0);
        setTimeout(function () {
            swap();
        }, 2000);
    }, 15000 + delay1); 



    setTimeout(function () {
        playSound(1);
        $('#subTitle03_008_1').fadeIn(250);
    }, 17000 + delay1);         //2 sec delay after cross fading

    setTimeout(function () {

        $('#subTitle03_008_1').fadeOut(250);

    }, 28000 + delay1);


    setTimeout(function () {
        playSound(2);
        $('#subTitle03_009').fadeIn(250);
    }, 29000 + delay1);
    setTimeout(function () {

        $('#subTitle03_009').fadeOut(250);
      
    }, 33000 + delay1);


    //registrar1
    setTimeout(function () {
        playSound(3);
        $('#subTitle03_010_1').fadeIn(250);
    }, 34000 + delay1);

    setTimeout(function () {

        $('#subTitle03_010_1').fadeOut(250);
        $('#subTitle03_010_2').fadeIn(250);
    }, 42000 + delay1);



    setTimeout(function () {

        $('#subTitle03_010_2').fadeOut(250);

    }, 51000 + delay1);


    setTimeout(function () {
        playSound(25);

    }, 52000 + delay1);

    setTimeout(function () {
        scene1_2();

    }, 58000 + delay1); 

}

function scene1_2() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene1_2');
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_01.png)');  //save phonemap from 1_2()
    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_02.jpg)');    //save crossfaded background
    

    setTimeout(function () {
        playSound(4);
        $('#subTitle03_012_1').fadeIn(250);
    }, delay1);

    setTimeout(function () {

        $('#subTitle03_012_1').fadeOut(250);
        $('#subTitle03_012_2').fadeIn(250);
    }, 10500 + delay1);


    setTimeout(function () {

        $('#subTitle03_012_2').fadeOut(250);

    }, 19500 + delay1);



    setTimeout(function () {
        playSound(5);
        $('#subTitle03_013_01').fadeIn(250);
    }, 20500 + delay1);

    setTimeout(function () {

        $('#subTitle03_013_01').fadeOut(250);
       
    }, 22000 + delay1);


    //cctv
    setTimeout(function () {
        playSound(6);
        $('#subTitle03_013_02').fadeIn(250);
    }, 23000 + delay1);

    setTimeout(function () {

        $('#subTitle03_013_02').fadeOut(250);

    }, 28000 + delay1);

    setTimeout(function () {
        $("#newContainer").load("scene2child.html");
    }, 29000 + delay1);
}








