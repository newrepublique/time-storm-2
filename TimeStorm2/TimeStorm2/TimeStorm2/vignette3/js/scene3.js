﻿$(document).ready(function () {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    localStorage.setItem('timer', localStorage.getItem(userId + '_timerval'));
    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_03.jpg)').stop(true, true).hide().fadeIn(1000);
    sessionStorage.setItem(userId + '_lastScene', 'scene3');    //save game

    Time(localStorage.getItem(userId + '_timerval'));
    

    $('#TScompleted,#TSprogress').show();

    setTimeout(function () {
        $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_03_.png)');

        setGameStatus('.per', '020', 'v3');
    }, 1000 + delay3);

    setTimeout(function () {

        playSound(29);


    }, 2000 + delay3);
    setTimeout(function () {
        updatePhoneTxt('You are in the waiting room', 'listIcon1');

    }, 11000 + delay3);



    //excuse me
    setTimeout(function () {
        playSound(7);
        $('#subTitle03_033').fadeIn(250);


    }, 12000 + delay3);

    setTimeout(function () {
        $('#subTitle03_033').fadeOut(250);
    }, 16500 + delay3);


    setTimeout(function () {
        scene3_1();
    }, 17500 + delay3);


});

function scene3_1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    //Highlight object
    $('#objectHighlight03_034_01 , #objectHighlight03_034_02 , #objectHighlight03_034_03').show();
    fadeloop('#objectHighlight03_034_01', 500, 500, 1000);
    fadeloop('#objectHighlight03_034_02', 500, 500, 1000);
    fadeloop('#objectHighlight03_034_03', 500, 500, 1000);
    

    //book
    $('#objectHighlight03_034_01').click(function () {

        playSound('0');

        book = true;
        $('#sceneOver').show();
        $('#close1').hide();

        setTimeout(function () {
            playSound(8);
            $('#subTitle03_036_1').fadeIn(250);
        }, delay3);

        setTimeout(function () {
            $('#subTitle03_036_1').fadeOut(250);
            $('#subTitle03_036_2').fadeIn(250);
        }, 9000 + delay3);


        setTimeout(function () {
            $('#subTitle03_036_2').fadeOut(250);

            $('#sceneOver').hide();
            if (book && pass_flag === 1 && jacket) {
                propclosed = true;
                checkclick();
            }

        }, 14000 + delay3);


    });

    // jacket
    $('#objectHighlight03_034_02').click(function () {
        playSound('0');

        jacket = true;
        $('#sceneOver').show();

        $('#propOverlayImage03_035').fadeIn(250);
        $('.propClose').show();
        $('#close2').hide();
        //checkclick();

    });

    //security pass
    $('#objectHighlight03_034_03').click(function () {
        playSound('0');
        securitypass = true;
        $('#sceneOver').show();

        $('#propOverlayImage03_037').fadeIn(250);

        $('.propClose').show();
        $('#close3').hide();


        //checkclick();

    });


    $('.propClose').click(function () {
        playSound('0');

        $('#propOverlayImage03_035').fadeOut(250);
        $('#propOverlayImage03_037').fadeOut(250);
        $('#sceneOver').hide();

        if (securitypass) {
            $('#sceneOver').show();
            setTimeout(function () {

                $('#subTitle03_038').fadeIn(250);
            }, delay3);

            setTimeout(function () {
                $('#subTitle03_038').fadeOut(250);
                $('#sceneOver').hide();
                securitypass = false;
                pass_flag = 1;
                propclosed = true;
                checkclick();
            }, 3000 + delay3);
            
        }
        else if (book && pass_flag === 1 && jacket) {
            propclosed = true;
            checkclick();
        }
    });

}

function checkclick() {
    //check if 3 objects are clicked
    if (book && pass_flag === 1 && jacket && propclosed) {


        setTimeout(function () {
            $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_03_.png)');
            $('#phoneMap03_039').show();
            $('.phoneMap03_039_dot').show();
            fadeloop('.phoneMap03_039_dot', 200, 200, 500);
        }, delay3);
    }
}

function greendot() {
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_04_.png)');
    playSound('0');
    $('#phoneMap03_039, .phoneMap03_039_dot').hide();
    $(currentdiv).fadeOut(500);
    setTimeout(function () {
        playSound(30);
    }, delay3);

    setTimeout(function () {
        
        $('#newContainer').load('scene4child.html');
    }, 8000 + delay3);

}