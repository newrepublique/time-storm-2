﻿$(document).ready(function () {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_08.jpg)').stop(true, true).hide().fadeIn(1000);
    sessionStorage.setItem(userId + '_lastScene', 'scene7');    //save game
    Time(localStorage.getItem(userId + '_timerval'));
    $('#TScompleted,#TSprogress').show();
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_07_.png)'); //from scene6-->scene5

    setTimeout(function () {

        updatePhoneTxt('You are in The Claimant’s Chambers', 'listIcon1');

    }, 1000+delay7);

    setTimeout(function () {
        playSound(40);
    }, 2000 + delay7);


    setTimeout(function () {

        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {


            case 'scene7_2':


                scene7_2();
                break;


            default: scene7_1();

        }

    }, 10000 + delay7);

   


})

function scene7_1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene7_1');
    localStorage.setItem('timer', localStorage.getItem(userId + '_timerval'));

    setTimeout(function () {
        playSound(18);
        $('#subTitle03_091_1').fadeIn(250);
    }, delay7);

    setTimeout(function () {

        $('#subTitle03_091_1').fadeOut(250);
        $('#subTitle03_091_2').fadeIn(250);

    }, 10000 + delay7);


    setTimeout(function () {

        $('#subTitle03_091_2').fadeOut(250);
        $('#subTitle03_091_3').fadeIn(250);

    }, 23000 + delay7);


    setTimeout(function () {

        $('#subTitle03_091_3').fadeOut(250);
       
    }, 32000 + delay7);


    //crossfades
    setTimeout(function () {


        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_08.jpg)').fadeOut(1500);
        setTimeout(function () {
            $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG03_background_09.jpg)').fadeIn(2000);
        }, 0);
        setTimeout(function () {
            swap();
        }, 2000);
    }, 33000 + delay7);


    //great
    setTimeout(function () {
        playSound(19);
        $('#subTitle03_093').fadeIn(250);

    }, 35000 + delay7);

    setTimeout(function () {

        $('#subTitle03_093').fadeOut(250);


    }, 39500 + delay7);

    setTimeout(function () {

        scene7_2();


    }, 40500 + delay7);


}

function scene7_2() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    localStorage.setItem('timer', localStorage.getItem(userId + '_timerval'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene7_2');
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_07_.png)'); //from scene6-->scene5
    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_09.jpg)');    //save crossfaded BG


    $('#objectHighlight03_094').show();

    fadeloop('#objectHighlight03_094', 500, 500, 1000);      //evidence bag


    $('#objectHighlight03_094').click(function () {
        playSound('0');


        $('#sceneOver').show();

        $('#close1').hide();
        $('#propOverlayImage03_095').fadeIn(250);
        $('.propClose').show();

   });

    $('.propClose').click(function () {
        playSound('0');
        
        $('#propOverlayImage03_095').fadeOut(250);

        setTimeout(function () {
            $('#subTitle03_096').fadeIn(250);
        }, delay7);

        setTimeout(function () {
        $('#sceneOver').hide();
        $('#subTitle03_096').fadeOut(250);
        }, 3000+delay7);


        //good job
        setTimeout(function () {
            playSound(20);
            $('#subTitle03_097_1').fadeIn(250);

        }, 4000+delay7);

        setTimeout(function () {
            $('#subTitle03_097_1').fadeOut(250);

        }, 11500 + delay7);

        setTimeout(function () {

            setGameStatus('.per', '060', 'v3');

        }, 12500 + delay7);

        setTimeout(function () {
            $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_07_.png)');
            $('#phoneMap03_099').show();
            $('.phoneMap03_099_dot').show();
            fadeloop('.phoneMap03_099_dot', 200, 200, 500);

        }, 13500 + delay7);




    });

}

function greendot() {
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_01.png)');
    $('#phoneMap03_099').hide();
    $('.phoneMap03_099_dot').hide();
    playSound('0');
    $(currentdiv).fadeOut(500);

    setTimeout(function () {
        playSound(41);
    }, delay7);

    setTimeout(function () {
        
        $('#newContainer').load('scene8child.html');
    }, 8000 + delay7);

}