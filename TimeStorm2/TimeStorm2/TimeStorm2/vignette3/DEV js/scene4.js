﻿$(document).ready(function () {
    
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));

    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_04.jpg)').stop(true, true).hide().fadeIn(1000);
    sessionStorage.setItem(userId + '_lastScene', 'scene4');    //save game
    Time(localStorage.getItem(userId + '_timerval'));
    $('#TScompleted,#TSprogress').show();
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_04_.png)'); //from scene 3


    setTimeout(function () {
        updatePhoneTxt('You have found the library', 'listIcon1');

    }, 1000 + delay4);

    setTimeout(function () {

        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {


            case 'scene4_2':


                scene4_2();
                break;


            default: scene4_1();

        }

    }, 2000 + delay4);


});

function scene4_1() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene4_1');
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    localStorage.setItem('timer', localStorage.getItem(userId + '_timerval'));

    $('#objectHighlight03_043').show();

    fadeloop('#objectHighlight03_043', 500, 500, 1000);

    $('#objectHighlight03_043').click(function () {
        playSound('0');


        $('#close_043').hide();
        setTimeout(function () {
            playSound(31);
        }, delay4);


        //crossfades
        setTimeout(function () {


            $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_04.jpg)').fadeOut(1500);
            setTimeout(function () {
                $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG03_background_05.jpg)').fadeIn(2000);
            }, 0);

            setTimeout(function () {
                swap();
            }, 2000);
        }, 3500 + delay4);

        setTimeout(function () {

            updatePhoneTxt('You are in the library', 'listIcon1');


        }, 5500 + delay4);

        setTimeout(function () {

            scene4_2();


        }, 6500 + delay4);
    });

}



function scene4_2() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    localStorage.setItem('timer', localStorage.getItem(userId + '_timerval'));

    sessionStorage.setItem(userId + '_lastSceneSection', 'scene4_2');
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_04_.png)'); //from scene 3 (save)
    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_05.jpg)');    //save crossfaded bg


    $('#objectHighlight03_047_01 , #objectHighlight03_047_02, #objectHighlight03_047_03').show();

    fadeloop('#objectHighlight03_047_01', 500, 500, 1000);      //blue book
    fadeloop('#objectHighlight03_047_02', 500, 500, 1000);      //dictaphone
    fadeloop('#objectHighlight03_047_03', 500, 500, 1000);      //green book

    //blue book
    $('#objectHighlight03_047_01').click(function () {

        playSound('0');

        bluebook = true;
        $('#sceneOver').show();
        $('#propOverlayImage03_049').fadeIn(250);
        $('#close1').hide();

        $('.propClose').show();
      //  checkclick();

    });

    //dictaphone
    $('#objectHighlight03_047_02').click(function () {
        $('#sceneOver').show();
        
        playSound('0');
        dictaphone = true;
        $('#propOverlayImage03_050').fadeIn(250);

        setTimeout(function () {
            playSound(43);
        }, delay4);

        $('#close2').hide();
        $('.propClose').show();


       // checkclick();


    });



    //green book
    $('#objectHighlight03_047_03').click(function () {
        playSound('0');

        greenbook = true;
        $('#sceneOver').show();
        $('#propOverlayImage03_048').fadeIn(250);
        $('#close3').hide();


        $('.propClose').show();
       // checkclick();

    });


    $('.propClose').click(function () {
        playSound('0');

       
        $('#propOverlayImage03_048,#propOverlayImage03_050,#propOverlayImage03_049').fadeOut(250);
        $('#sceneOver').hide();


        if (dictaphone) {
            sounds[43].pause();
            sounds[43].currentTime = 0;
            $('#sceneOver').show();
            setTimeout(function () {

                $('#subTitle03_051').fadeIn(250);
            }, delay4);


            setTimeout(function () {
                $('#subTitle03_051').fadeOut(250);
            }, 3500 + delay4);

            setTimeout(function () {
                playSound(32);
                //dictaphone = false;
            }, 4500 + delay4);
            setTimeout(function () {
                $('#sceneOver').hide();
                dictaphone = false;
                flag = 1;
                propclosed = true;
                checkclick();


            }, 6500 + delay4);
          //  flag = 1;
        }
        else if (bluebook && flag === 1 && greenbook) {
            propclosed = true;
            checkclick();
        }
        //checkclick();

    });



}


function checkclick() {
    //check if all 3 are clicked


    if (bluebook && flag === 1 && greenbook && propclosed) {
        setTimeout(function () {
            $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_04_.png)');
            $('#phoneMap03_053').show();
            $('.phoneMap03_053_dot').show();
            fadeloop('.phoneMap03_053_dot', 200, 200, 500);     
        }, delay4);
    }

}

function greendot() {
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_05_.png)');
    playSound('0');
    $(currentdiv).fadeOut(500);
    $('#phoneMap03_053').hide();
    $('.phoneMap03_053_dot').hide();
    setTimeout(function () {
        playSound(33);
    }, delay4);

    setTimeout(function () {
        
        $('#newContainer').load('scene5child.html');
    }, 7500 + delay4);

}