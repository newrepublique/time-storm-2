﻿$(document).ready(function () {
    
    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_01.jpg)').stop(true, true).hide().fadeIn(1000);
    $('#sceneOver').css('opacity', '0');
    sessionStorage.setItem(userId + '_lastScene', 'scene2');    //save game
    $('#TScompleted,#TSprogress').show();
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_01.png)');  //from scene1
    $('#TStimer').hide();
    sounds[0].volume = 1;
    sounds[26].volume = 1;
    sounds[27].volume = 1;
    sounds[28].volume = 1;
    setTimeout(function () {
        playSound(26);
       
        
    }, 1000 + delay2);

    setTimeout(function () {
        //$('#TStimer').show().html('00:15:00');
    }, 8000 + delay2);


    setTimeout(function () {
        $('#subTitle03_017').fadeIn(250);


    }, 9000 + delay2);

    setTimeout(function () {
        $('#subTitle03_017').fadeOut(250);
    }, 12000 + delay2);

    setTimeout(function () {
     localStorage.setItem('timer', 900);
    
     Time(localStorage.getItem('timer'));
        scene2_1();
    }, 13000 + delay2);



    //pile of papers
    $('#objectHighlight03_019_01').click(function () {
        papers = true;
        playSound('0');

        $('#sceneOver').fadeTo(100, 0.5);
        $('#close1').hide();

        setTimeout(function () {
            $('#subTitle03_022').fadeIn(250);
        }, delay2);

        setTimeout(function () {
            $('#subTitle03_022').fadeOut(250);

        }, 2500 + delay2);



        setTimeout(function () {
            playSound(27);
        }, 3500 + delay2);

        setTimeout(function () {
            $('#sceneOver').hide();
            checkclick();
        }, 4500 + delay2);  //already 1 sec delay in checkclick function


    });

    // tray
    $('#objectHighlight03_019_02').click(function () {
        playSound('0');
        tray = true;
        $('#sceneOver').fadeTo(100, 0.5);
        $('#close2').hide();


        setTimeout(function () {
            $('#subTitle03_020').fadeIn(250);
        }, delay2);

        setTimeout(function () {
            $('#subTitle03_020').fadeOut(250);
            $('#sceneOver').hide();
            checkclick();
        }, 2500 + delay2);
    });

    //newspaper
    $('#objectHighlight03_019_03').click(function () {
        playSound('0');
        newspaper = true;
        $('#sceneOver').fadeTo(100, 0.5);
        $('#propOverlayImage03_021').fadeIn(250);

        $('#close3').hide();
        $('.propClose').show();
        checkclick();
    });


    $('.propClose').click(function () {
      
        propclosed = true;
  
        playSound('0');
        $('#propOverlayImage03_021').fadeOut(250);
        $('#sceneOver').hide();
        checkclick();


    });



});



function scene2_1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    var clicked1, clicked2, clicked3;
    
    
    $('#objectHighlight03_019_01 , #objectHighlight03_019_02 , #objectHighlight03_019_03').show();

    fadeloop('#objectHighlight03_019_01', 500, 500, 1000);
    fadeloop('#objectHighlight03_019_02', 500, 500, 1000);
    fadeloop('#objectHighlight03_019_03', 500, 500, 1000);



}

function checkclick() {
    if (tray && newspaper && papers && propclosed) {

        setTimeout(function () {
            $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_02_.png)');
            $('#phoneMap03_024').show();
            $('.phoneMap03_024_dot').show();
            fadeloop('.phoneMap03_024_dot', 200, 200, 500);
        }, delay2);


        setTimeout(function () {
            $('#subTitle03_025').fadeIn(250);
        }, 1000 + delay2);
    }

}



function greendot() {
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_03_.png)');
    $('#phoneMap03_024').hide();
    $('.phoneMap03_024_dot').hide();
    $('#subTitle03_025').fadeOut(250);
    for (i = 0; i < sounds.length; i++) {

        sounds[i].volume = 1;
    }
    playSound('0');
    $(currentdiv).fadeOut(500);

    setTimeout(function () {

        $('#subTitle03_027').fadeIn(250);

    }, delay2);

    setTimeout(function () {

        $('#subTitle03_027').fadeOut(250);

    }, 4000 + delay2);
    setTimeout(function () {
        playSound(28);

    }, delay2);

    setTimeout(function () {
        
        $('#newContainer').load('scene3child.html');
    }, 10000 + delay2);

}