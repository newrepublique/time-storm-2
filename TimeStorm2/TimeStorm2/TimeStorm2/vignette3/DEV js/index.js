﻿//Javascript file for index page


var userId ;

var task_complete = false;

sessionStorage.setItem('status-line-li', '');
var timerTime = 900;
//if (localStorage.getItem(userId + '_timer') != null) {
//    timerTime = localStorage.getItem(userId + '_timer');
//    if (timerTime == 'null') {
//        timerTime = 900;
        
//}
//}
//localStorage.setItem(userId + '_timerval', timerTime);
var currentdiv = '#TScontainer';
var nextdiv = '#Dummy';

var delay2 = 1000;
var isiPad = false;
var scene = '';
var section = '';
var phonetextdata = '';
var gendervalue;
var mutesoundvalue;
var progressvalue;
var scene;
var section;
var currentscene = '';
var muteSound;
var currentvignette = '';
var timer;

$(document).ready(function () {
    $('#TSframe3').css('display', 'none');
    $('#pangoText a').css('display', 'none');
    $('#phoneText ul').css('display', 'none');
    


    userId = localStorage.getItem('userId');

    if (userId == null) {
        if (navigator && navigator.platform && navigator.userAgent.match('MSIE')) {
            $('body').append('<div id="continueIE" class="stretchBG" style="width: 21%; height: 3.76%; font-size: 30px; position: fixed; z-index: 6; top: 44%; color: #f7dac2; cursor: pointer; text-align: center;left: 40%;border: 4px solid #f7dac2;">Click here to Continue</div>');
            $('#continueIE').click(function () {
                window.location.href = 'index.html';
            });

        }
        $('#Apple').hide();
        //login code..
        signIn('v3');
        //window.stop();
    }
    else {
       
        
        getPlatform();
        
        $('#Apple').click(function () {
            $('#Apple').hide();
            getResults();
            
            //initVignette();
        });



        var firstTime = true;
        $('#TSmute').click(function () {
            playSound('0');
            if (firstTime) {
                for (i = 0; i < sounds.length; i++) {
                    sounds[i].volume = 0;
                }
                sessionStorage.setItem('muteSound', true);
                firstTime = false;
            }
            else {
                setAudio(!(JSON.parse(sessionStorage.getItem('muteSound'))));
                sessionStorage.setItem('muteSound', !(JSON.parse(sessionStorage.getItem('muteSound'))));
            }
        });


        $('#TSsave').click(function () {
            sendData();
            //$('#saved').fadeTo('slow', 0.5).fadeTo('slow', 1.0);
            //setTimeout(function () {

            //    $('#saved').hide();
            //}, 2000);
        });
    }
});

function getPlatform() {


    macCSS();
    if (navigator && navigator.platform && navigator.userAgent.match(/iPad/i)) {
        isiPad = true;
        console.log('this');
    }
    if (!isiPad) {

        getResults();
        $('#Apple').hide();
        //initVignette();
    }
}

function swap() {
    if (currentdiv == '#TScontainer') {
        currentdiv = '#Dummy';
        nextdiv = '#TScontainer';
    }
    else {
        nextdiv = '#Dummy';
        currentdiv = '#TScontainer';
    }
}

function sendData() {


    localStorage.setItem('userId', userId);
    localStorage.setItem(userId + '_gender', gender);


    
    localStorage.setItem(userId + '_mutesound', sessionStorage.getItem('muteSound'));
    localStorage.setItem(userId + '_progress', $('.per').html());
    localStorage.setItem(userId + '_lastScene', sessionStorage.getItem(userId + '_lastScene'));
    localStorage.setItem(userId + '_lastSceneSection', sessionStorage.getItem(userId + '_lastSceneSection'));
    localStorage.setItem(userId + '_vignette', currentvignette);


    gendervalue = localStorage.getItem(userId + '_gender');
    mutesoundvalue = sessionStorage.getItem('muteSound');

    localStorage.setItem(userId + '_phonetext', localStorage.getItem('phonetext'));
    phonetextdata = localStorage.getItem(userId + '_phonetext');
    

    if (localStorage.getItem(userId + '_phonetext') == null) {
       
        phonetextdata = '';
    }
   

    phonetextdata = htmlEscape(phonetextdata);
    progressvalue = $('.per').html();
    scene = sessionStorage.getItem(userId + '_lastScene');
    section = sessionStorage.getItem(userId + '_lastSceneSection');

    localStorage.setItem(userId + '_timer', localStorage.getItem('timer'));

    timer = localStorage.getItem(userId + '_timer');
    if (timer == null) {
        timer = 900;
        localStorage.setItem(userId + '_timer', 900);
    }
    var userdetails = {

        vignette: currentvignette,
        timer: timer,

        gender: gendervalue,
        phonetext: phonetextdata,
        mutesound: mutesoundvalue,
        progress: progressvalue,
        scene: scene,
        section: section

    }

    var details = JSON.stringify(userdetails);

    $.ajax({
        type: 'GET',
        url: '../Handlers/UserDetails.ashx',
        data: {
            userid: userId,
            details: details,
            action: "save"
        },
        context: this,
        dataType: 'text',
        contentType: "application/json; charset=utf-8",
        success: function (msg) {
           
        },
        error: function () {
            
        }
    });

}

function getResults() {

    gendervalue = localStorage.getItem(userId + '_gender');
    mutesoundvalue = localStorage.getItem(userId + '_mutesound');
    phonetextdata = localStorage.getItem(userId + '_phonetext');
    if(phonetextdata == 'null'){
        phonetextdata = '';
    }
    progressvalue = localStorage.getItem(userId + '_progress');
    scene = localStorage.getItem(userId + '_lastScene');
    section = localStorage.getItem(userId + '_lastSceneSection');
    currentvignette = localStorage.getItem(userId + '_vignette');
    timer = localStorage.getItem(userId + '_timer');
    localStorage.setItem(userId + '_timerval', localStorage.getItem(userId + '_timer'));
    if (gendervalue != null && mutesoundvalue != null && phonetextdata != null && progressvalue != null && scene != null && section != null && timer != null) {
    
        currentscene = scene;

        ///value to be set here....................................

        setGameStatus('.per', progressvalue, 'v1');
        phonetextdata = htmlUnescape(phonetextdata);
        $('#list').html(phonetextdata);
        sessionStorage.setItem('status-line-li', phonetextdata);
        //setAudio(mutesoundvalue);
        sessionStorage.setItem('muteSound', mutesoundvalue);
        initVignette();
        
    }
    else {


        $.ajax({
            type: 'GET',
            url: '../Handlers/UserDetails.ashx',
            data: {
                userid: userId,
                action: "get"

            },
            context: this,
            dataType: 'text',
            contentType: "application/json; charset=utf-8",
            success: function (detailsdata) {

                
                if (detailsdata != undefined && detailsdata != null && jQuery.isEmptyObject(detailsdata) == false) {

                    var parsedData = JSON.parse(detailsdata);

                    localStorage.setItem(userId + '_gender', parsedData.gender);
                    localStorage.setItem(userId + '_phonetext', parsedData.phonetext);
                    localStorage.setItem(userId + '_mutesound', parsedData.mutesound);
                    localStorage.setItem(userId + '_progress', parsedData.progress);
                    localStorage.setItem(userId + '_lastScene', parsedData.scene);
                    currentscene = parsedData.scene;
                    localStorage.setItem(userId + '_lastSceneSection', parsedData.section);

                    localStorage.setItem(userId + '_vignette', parsedData.vignette);
                    currentvignette = parsedData.vignette;

                    localStorage.setItem(userId + '_timer', parsedData.timer);
                    localStorage.setItem(userId + '_timerval', localStorage.getItem(userId + '_timer'));
                    //values to be set here...
                    setGameStatus('.per', parsedData.progress, 'v1');

                    parsedData.phonetext = htmlUnescape(parsedData.phonetext);
                    $('#list').html(parsedData.phonetext);
                    sessionStorage.setItem('status-line-li', parsedData.phonetext);

                    //setAudio(parsedData.mutesound);
                    sessionStorage.setItem('muteSound', parsedData.mutesound);
                    initVignette();
                }
                else {
                    localStorage.setItem(userId + '_timer', 900);
                    initVignette();

                }
            },
            error: function () {

            }
        });

    }

}

function htmlEscape(str) {
    return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
}

function htmlUnescape(value) {
    return String(value)
        .replace(/&quot;/g, '"')
        .replace(/&#39;/g, "'")
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&amp;/g, '&');
}


function Time(timerTime) {
    var intervalID;
    var result = [];
    intervalID = setInterval(function () {
        timerTime = timerTime - 1;
    
        localStorage.setItem(userId + '_timerval', timerTime);

        result = currentTimerValues(timerTime);
        $('#TStimer').html("00:" + result[0] + ":" + result[1]).show();

        if (timerTime == 0) {
            timeOver();
            clearInterval(intervalID);
            
        }
        if (task_complete) {

            clearInterval(intervalID);
            $('#TStimer').hide();

        }

      

    }, 1000);

}

function timeOver() {
    $(currentdiv).fadeOut();
    $('.orangeText, .whiteText, .propOverlay').hide();
    $('#newContainer').html('');
    $('audio').prop('volume', '0');
    for (i = 0; i < sounds.length; i++) {
        sounds[i].volume = 0.000000001;
        sounds[i].pause();
    }
    setTimeout(function () {
        $('#timertext').show();
        $('#TStimer').hide();
    }, delay2);

    setTimeout(function () {
        $('#timertext').hide();

    }, 3000 + delay2);

    setTimeout(function () {
        timerTime = 900;
        localStorage.setItem('timer', 900);
        setGameStatus('.per', '000', 'v3');
        $('#newContainer').load('scene2child.html');
        
    }, 3500 + delay2);

}

var imgArray = Array(77);
var filesArray = Array(119);
var sounds = Array(44);
var sndArray = Array(44);

function preload() {
    var gender = 'male';
    if (localStorage.getItem(userId + '_gender') != null) {
        gender = localStorage.getItem(userId + '_gender');
    }
    document.getElementById('loading').style.display = 'block';

    var path = 'sounds/';
    var ext = '.mp3';
    if (BrowserDetect.browser == 'Safari') ext = '.mp3';
    if (BrowserDetect.browser == 'Firefox') ext = '.ogg';
    //filling in the sndArray
    sndArray = [
    path + 'timestorm_G02_VG01_click_01' + ext,
	path + 'confero/TimeStorm__G02_VG01_VO_01' + ext,
    path + gender + '/TimeStorm__G02_VG02_VO_02' + ext,
    path + 'confero/TimeStorm__G02_VG02_VO_03' + ext,
    path + 'confero/TimeStorm__G02_VG02_VO_04' + ext,
    path + gender + '/TimeStorm__G02_VG02_VO_05' + ext,
    path + 'confero/TimeStorm__G02_VG02_VO_06' + ext,
    path + 'confero/TimeStorm__G02_VG02_VO_07' + ext,
    path + 'confero/TimeStorm__G02_VG02_VO_08' + ext,
    path + 'confero/TimeStorm__G02_VG02_VO_09' + ext,
    path + gender + '/TimeStorm__G02_VG02_VO_10' + ext,
    path + 'confero/TimeStorm__G02_VG02_VO_11' + ext,
    path + 'confero/TimeStorm__G02_VG02_VO_12' + ext,
    path + 'confero/TimeStorm__G02_VG02_VO_13' + ext,
    path + 'confero/TimeStorm__G02_VG02_VO_14' + ext,
    path + 'confero/TimeStorm__G02_VG02_VO_15' + ext,
    path + 'confero/TimeStorm__G02_VG02_VO_16' + ext,
    path + 'confero/TimeStorm__G02_VG02_VO_17' + ext,
    path + 'confero/TimeStorm__G02_VG02_VO_18' + ext,
    path + 'confero/TimeStorm__G02_VG02_VO_19' + ext,
    path + 'confero/TimeStorm__G02_VG02_VO_20' + ext,
    path + 'confero/TimeStorm__G02_VG02_VO_21' + ext,
    path + 'confero/TimeStorm__G02_VG02_VO_22' + ext,
    path + 'confero/TimeStorm__G02_VG02_VO_23' + ext,
    path + 'timestorm_G02_VG03_soundfx_01' + ext, //24
    path + 'timestorm_G02_VG03_soundfx_02' + ext,
    path + 'timestorm_G02_VG03_soundfx_03' + ext,
    path + 'timestorm_G02_VG03_soundfx_04' + ext,
    path + 'timestorm_G02_VG03_soundfx_05' + ext,
    path + 'timestorm_G02_VG03_soundfx_06' + ext,
    path + 'timestorm_G02_VG03_soundfx_07' + ext,
    path + 'timestorm_G02_VG03_soundfx_08' + ext,//31
    path + 'timestorm_G02_VG03_soundfx_10' + ext,
    path + 'timestorm_G02_VG03_soundfx_11' + ext,
    path + 'timestorm_G02_VG03_soundfx_12' + ext,
    path + 'timestorm_G02_VG03_soundfx_13' + ext,//35
    path + 'timestorm_G02_VG03_soundfx_14' + ext,
    path + 'timestorm_G02_VG03_soundfx_15' + ext,
    path + 'timestorm_G02_VG03_soundfx_16' + ext,
    path + 'timestorm_G02_VG03_soundfx_17' + ext,
    path + 'timestorm_G02_VG03_soundfx_18' + ext,
    path + 'timestorm_G02_VG03_soundfx_19' + ext,
    path + 'timestorm_G02_VG03_soundfx_20' + ext,//42
    path + 'timestorm_G02_VG03_eddie_mabo_speech_01' + ext

    ];

    path = 'images/';
    imgArray = [
    path + '.png',
    path + 'hand.png',
	path + 'icon.png',
	path + 'icon1.png',
	path + 'icon2.png',
	path + 'icon3.png',
	path + 'iconAns.png',
	path + 'loading.gif',
	path + 'mute-1.png',
	path + 'mute-2.png',
	path + 'save-1.png',
	path + 'save-2.png',
	path + 'scroll.png',
	path + 'scroll1.png',
	path + 'scroll2.png',
	path + 'scroll3.png',
	path + 'timer.png',
	path + 'timestorm_G02_VG01_answer_01.png',
    path + 'timestorm_G02_VG01_bulletpoint_01.png',
	path + 'timestorm_G02_VG01_questiontitle_01.png',
	path + 'timestorm_G02_VG03_background_01.jpg',
	path + 'timestorm_G02_VG03_background_02.jpg',
    path + 'timestorm_G02_VG03_background_03.jpg',
    path + 'timestorm_G02_VG03_background_04.jpg',
    path + 'timestorm_G02_VG03_background_05.jpg',
    path + 'timestorm_G02_VG03_background_06.jpg',
    path + 'timestorm_G02_VG03_background_07.jpg',
    path + 'timestorm_G02_VG03_background_08.jpg',
    path + 'timestorm_G02_VG03_background_09.jpg',
    path + 'timestorm_G02_VG03_background_10.jpg',
    path + 'timestorm_G02_VG03_background_11.jpg',
    path + 'timestorm_G02_VG03_background_12.jpg',
    path + 'timestorm_G02_VG03_background_13.jpg',
	path + 'timestorm_G02_VG03_headsupdisplay_01.png',
    path + 'timestorm_G02_VG03_objecthighlight_01.png',
    path + 'timestorm_G02_VG03_objecthighlight_02.png',
    path + 'timestorm_G02_VG03_objecthighlight_03.png',
    path + 'timestorm_G02_VG03_objecthighlight_04.png',
    path + 'timestorm_G02_VG03_objecthighlight_05.png',
    path + 'timestorm_G02_VG03_objecthighlight_06.png',
    path + 'timestorm_G02_VG03_objecthighlight_07.png',
    path + 'timestorm_G02_VG03_objecthighlight_08.png',
    path + 'timestorm_G02_VG03_objecthighlight_09.png',
    path + 'timestorm_G02_VG03_objecthighlight_10.png',
    path + 'timestorm_G02_VG03_objecthighlight_11.png',
    path + 'timestorm_G02_VG03_objecthighlight_12.png',
    path + 'timestorm_G02_VG03_objecthighlight_13.png',
    path + 'timestorm_G02_VG03_objecthighlight_14.png',
	path + 'timestorm_G02_VG03_phonemap_01.png',
    path + 'timestorm_G02_VG03_phonemap_02.png',
    path + 'timestorm_G02_VG03_phonemap_02_.png',
    path + 'timestorm_G02_VG03_phonemap_03.png',
    path + 'timestorm_G02_VG03_phonemap_03_.png',
    path + 'timestorm_G02_VG03_phonemap_04.png',
    path + 'timestorm_G02_VG03_phonemap_04_.png',
    path + 'timestorm_G02_VG03_phonemap_05.png',
    path + 'timestorm_G02_VG03_phonemap_05_.png',
    path + 'timestorm_G02_VG03_phonemap_06.png',
    path + 'timestorm_G02_VG03_phonemap_06_.png',
    path + 'timestorm_G02_VG03_phonemap_07.png',
    path + 'timestorm_G02_VG03_phonemap_07_.png',
    path + 'timestorm_G02_VG03_phonemap_08.png',
	path + 'timestorm_G02_VG03_propoverlay_01.png',
    path + 'timestorm_G02_VG03_propoverlay_02.png',
    path + 'timestorm_G02_VG03_propoverlay_03.png',
    path + 'timestorm_G02_VG03_propoverlay_04.png',
    path + 'timestorm_G02_VG03_propoverlay_05.png',
    path + 'timestorm_G02_VG03_propoverlay_06.png',
    path + 'timestorm_G02_VG03_propoverlay_07.png',
    path + 'timestorm_G02_VG03_propoverlay_08.png',
    path + 'timestorm_G02_VG03_propoverlay_09.png',
    path + 'timestorm_G02_VG03_propoverlay_10.png',
    path + 'timestorm_G02_VG03_propoverlay_11.png',
	path + 'timestorm_G02_VG03_question_01.png',
	path + 'timestorm_G02_VG03_timer_01.png'

    ];

    //loading sounds
    for (i = 0; i < 44; i++) {
        filesArray[i] = sndArray[i];

        sounds[i] = loadSound(filesArray[i]);
    }

    for (i = 44; i < 119; i++) {
        filesArray[i] = imgArray[i - 44];
        //console.log('loading '+filesArray[i]);
        loadImg(filesArray[i]);
    }


}


function loadImg(which) {
    var img = new Image();
    img.src = which;
    img.onload = function () {
        rm4Array(which);
    }
}
var count = filesArray.length;

function loadSound(which) {

    var snd = new Audio();
    snd.src = which;

    if (navigator.userAgent.match(/iPad/i)) {
        document.getElementById('loadingPercent').innerHTML = Math.floor((119 - count) * 100 / 119);
        count--;
        if (which == 'sounds/timestorm_G02_VG03_eddie_mabo_speech_01.mp3') {

            document.getElementById('loading').style.display = 'none';
            init();
        }
    }
    else {

        snd.addEventListener('canplaythrough', function () {
            rm4Array(which);
        });
        snd.onerror = function (e) {//document.getElementById('temp').innerHTML=snd.src+' gave error';
            console.log(snd.src + ' gave an error');
        }
    }

    return snd;
}

function rm4Array(what) {
    for (i = 0; i < filesArray.length; i++) {
        if (filesArray[i] == what) {
            filesArray.splice(i, 1);

            document.getElementById('loadingPercent').innerHTML = Math.floor((119 - filesArray.length) * 100 / 119);
        }
        if (filesArray.length < 1) {
            init();
            //alert('loaded');
            document.getElementById('loading').style.display = 'none';
        }
    }

}

function initVignette() {

    switch (currentvignette) {

        case 'v2': window.location.href = '../vignette2/index.html';
           
            break;
        case 'v3': preload();
            break;
        case 'v4': window.location.href = '../vignette4/index.html';
            break;
        case 'v5': window.location.href = '../vignette5/index.html';
            break;
        default:
            window.location.href = '../vignette1/index.html';
    }
}

function init() {
    $('#startScreen').fadeIn(1000);
    setTimeout(function () {
        $('#startScreen').fadeOut(1000);

    }, 5000);



    setTimeout(function () {
        currentvignette = 'v3';
        switch (currentscene) {

            case 'scene2': $('#newContainer').load('scene2child.html');
                break;
            case 'scene3': $('#newContainer').load('scene3child.html');
                break;
            case 'scene4': $('#newContainer').load('scene4child.html');
                break;
            case 'scene5': $('#newContainer').load('scene5child.html');
                break;
            case 'scene6': $('#newContainer').load('scene6child.html');
                break;
            case 'scene7': $('#newContainer').load('scene7child.html');
                break;
            case 'scene8': $('#newContainer').load('scene8child.html');
                break;

            default: $('#newContainer').load('scene1child.html');

        }
    }, 6000);
    setTimeout(function () {

        $('#TSframe3').css('display', 'block');
        $('#pangoText a').css('display', 'block');
        $('#phoneText ul').css('display', 'block');
        $('#TSscene').css('opacity', '1');
    }, 6500);
}

var BrowserDetect = {
    init: function () {
        this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
        this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
        this.OS = this.searchString(this.dataOS) || "an unknown OS";
    },
    searchString: function (data) {
        for (var i = 0; i < data.length; i++) {
            var dataString = data[i].string;
            var dataProp = data[i].prop;
            this.versionSearchString = data[i].versionSearch || data[i].identity;
            if (dataString) {
                if (dataString.indexOf(data[i].subString) != -1)
                    return data[i].identity;
            }
            else if (dataProp)
                return data[i].identity;
        }
    },
    searchVersion: function (dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index == -1) return;
        return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
    },
    dataBrowser: [
		{
		    string: navigator.userAgent,
		    subString: "Chrome",
		    identity: "Chrome"
		},
		{
		    string: navigator.userAgent,
		    subString: "OmniWeb",
		    versionSearch: "OmniWeb/",
		    identity: "OmniWeb"
		},
		{
		    string: navigator.vendor,
		    subString: "Apple",
		    identity: "Safari",
		    versionSearch: "Version"
		},
		{
		    prop: window.opera,
		    identity: "Opera",
		    versionSearch: "Version"
		},
		{
		    string: navigator.vendor,
		    subString: "iCab",
		    identity: "iCab"
		},
		{
		    string: navigator.vendor,
		    subString: "KDE",
		    identity: "Konqueror"
		},
		{
		    string: navigator.userAgent,
		    subString: "Firefox",
		    identity: "Firefox"
		},
		{
		    string: navigator.vendor,
		    subString: "Camino",
		    identity: "Camino"
		},
		{		// for newer Netscapes (6+)
		    string: navigator.userAgent,
		    subString: "Netscape",
		    identity: "Netscape"
		},
		{
		    string: navigator.userAgent,
		    subString: "MSIE",
		    identity: "Explorer",
		    versionSearch: "MSIE"
		},
		{
		    string: navigator.userAgent,
		    subString: "Gecko",
		    identity: "Mozilla",
		    versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
		    string: navigator.userAgent,
		    subString: "Mozilla",
		    identity: "Netscape",
		    versionSearch: "Mozilla"
		}
    ],
    dataOS: [
		{
		    string: navigator.platform,
		    subString: "Win",
		    identity: "Windows"
		},
		{
		    string: navigator.platform,
		    subString: "Mac",
		    identity: "Mac"
		},
		{
		    string: navigator.userAgent,
		    subString: "iPhone",
		    identity: "iPhone/iPod"
		},
		{
		    string: navigator.platform,
		    subString: "Linux",
		    identity: "Linux"
		}
    ]

};
BrowserDetect.init();

function playSound(which) {
    if (which == '' || typeof (which) == 'undefined') { return; }
    sounds[which].play();
}
function macCSS() {

    if (navigator.userAgent.toUpperCase().indexOf('MAC') >= 0) {
        console.log('mac');
        $('body').addClass('mac');
    }

}