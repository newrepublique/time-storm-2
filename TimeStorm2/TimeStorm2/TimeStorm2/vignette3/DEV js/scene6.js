﻿$(document).ready(function () {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_10.jpg)').stop(true, true).hide().fadeIn(1000);
    sessionStorage.setItem(userId + '_lastScene', 'scene6');    //save game
    Time(localStorage.getItem(userId + '_timerval'));
    $('#TScompleted,#TSprogress').show();

    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_06_.png)');  //from scene 5


    setTimeout(function () {

        setGameStatus('.per', '040', 'v3');


    }, 1000 + delay6);


    setTimeout(function () {
        playSound(38);

    }, 2000 + delay6);


    setTimeout(function () {

        updatePhoneTxt('You are in The Respondent’s chambers', 'listIcon1');


    }, 13000 + delay6);

    setTimeout(function () {

        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {


            case 'scene6_2':

                scene6_2();
                break;


            default: scene6_1();

        }

    }, 13000 + delay6);



})

function scene6_1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene6_1');
    localStorage.setItem('timer', localStorage.getItem(userId + '_timerval'));

    //Queen's council


    setTimeout(function () {
        playSound(15);
        $('#subTitle03_078_1').fadeIn(250);
    }, delay6);

    setTimeout(function () {

        $('#subTitle03_078_1').fadeOut(250);
        $('#subTitle03_078_2').fadeIn(250);

    }, 8000 + delay6);



    setTimeout(function () {

        $('#subTitle03_078_2').fadeOut(250);
        $('#subTitle03_078_3').fadeIn(250);
    }, 14000 + delay6);



    setTimeout(function () {

        $('#subTitle03_078_3').fadeOut(250);
        $('#subTitle03_078_4').fadeIn(250);

    }, 20000 + delay6);


    setTimeout(function () {

        $('#subTitle03_078_4').fadeOut(250);
        $('#subTitle03_078_5').fadeIn(250);

    }, 28000 + delay6);


    setTimeout(function () {

        $('#subTitle03_078_5').fadeOut(250);
        $('#subTitle03_078_6').fadeIn(250);

    }, 34500 + delay6);

    setTimeout(function () {

        $('#subTitle03_078_6').fadeOut(250);

    }, 44500 + delay6);



    //crossfades
    setTimeout(function () {
        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_10.jpg)').fadeOut(1500);

        setTimeout(function () {
            $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG03_background_11.jpg)').fadeIn(2000);
        }, 0);
        setTimeout(function () {
            swap();
        }, 2000);


    }, 45500 + delay6);


    setTimeout(function () {
        playSound(16);
        $('#subTitle03_080').fadeIn(250);
    }, 47500 + delay6);

    setTimeout(function () {

        $('#subTitle03_080').fadeOut(250);

    }, 51500 + delay6);

    setTimeout(function () {

        scene6_2();

    }, 52500 + delay6);


}
function scene6_2() {

    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    localStorage.setItem('timer', localStorage.getItem(userId + '_timerval'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene6_2');
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_06_.png)');  //from scene 5
    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG03_background_11.jpg)');    //save crossfaded BG

    PlaySoundWithDelay($divElementRenderAudioTags, 0, voiceover17_sound_file_url_mp3, voiceover17_sound_file_url_ogg, '');


    $('#objectHighlight03_081').show();

    fadeloop('#objectHighlight03_081', 500, 500, 1000);      //brief


    $('#objectHighlight03_081').click(function () {
        playSound('0');


        $('#sceneOver').show();
        $('#close1').hide();

        $('#propOverlayImage03_082').fadeIn(250);

        $('.propClose').show();

    });

    $('.propClose').click(function () {
        playSound('0');

        $('#propOverlayImage03_082').fadeOut(250);

        setTimeout(function () {
            $('#subTitle03_083').fadeIn(250);
        }, delay6);

        setTimeout(function () {
            $('#subTitle03_083').fadeOut(250);
            $('#sceneOver').hide();

        }, 3000 + delay6);


        //hello
        setTimeout(function () {
            playSound(17);
            $('#subTitle03_084').fadeIn(250);
        }, 4000 + delay6);

        setTimeout(function () {

            $('#subTitle03_084').fadeOut(250);

        }, 10000 + delay6);

        setTimeout(function () {

            $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_06_.png)'); //from scene5
            $('#phoneMap03_085').show();
            $('.phoneMap03_085_dot').show();
            fadeloop('.phoneMap03_085_dot', 200, 200, 500);

        }, 11000 + delay6);


        //enter


        $('.phoneMap03_085_dot').click(function () {
            $('#Map').css('background-image', 'url(./images/timestorm_G02_VG03_phonemap_07_.png)');
            playSound('0');
            $(currentdiv).fadeOut(500);
            $('#phoneMap03_085').hide();
            $('.phoneMap03_085_dot').hide();
            setTimeout(function () {
                $('#subTitle03_086').fadeIn(250);
            }, 2000 + delay6);

            setTimeout(function () {

                $('#subTitle03_086').fadeOut(250);

            }, 11500 + delay6);


            setTimeout(function () {
                playSound(39);

            }, 2000 + delay6);

            setTimeout(function () {
                $('#newContainer').load('scene7child.html');


            }, 13000 + delay6);
        });
    });

}