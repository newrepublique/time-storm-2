﻿$(document).ready(function () {

    sessionStorage.setItem(userId + '_lastScene', 'scene8');
    $(currentdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG01_background_12.jpg)').stop(true, true).hide().fadeIn(1000);

    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_10.png)');
    $('#TScompleted,#TSprogress').show();

    setTimeout(function () {


        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {

            case 'scene8_1': scene8_1();
                break;
            case 'scene8_2': showQuestion7();
                break;

            case 'scene8_3': postQues();
                break;

            case 'scene8_4': showMiniQues();
                $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_14.png)');
                break;
            default: scene8_1();

        }


    }, 2000);
});

function scene8_1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene8_1');

    setTimeout(function () {
        playSound(33);
        $('#subTitle134_1').fadeIn(250);


    }, 1000);

    setTimeout(function () {

        $('#subTitle134_1').fadeOut(250);
        $('#subTitle134_2').fadeIn(250);


    }, 13000);

    setTimeout(function () {

        $('#subTitle134_2').fadeOut(250);
        $('#subTitle134_3').fadeIn(250);


    }, 23000);


    setTimeout(function () {

        $('#subTitle134_3').fadeOut(250);
    }, 31000);

    setTimeout(function () {

        $('#subTitle135').fadeIn(250);
        playSound(34);

    }, 32000);

    setTimeout(function () {

        $('#subTitle135').fadeOut(250);
    }, 34000);

    setTimeout(function () {


        $('#subTitle136').fadeIn(250);
        playSound(35);

    }, 35000);

    setTimeout(function () {

        $('#subTitle136').fadeOut(250);

    }, 40000);

    setTimeout(function () {


        $('#subTitle137').fadeIn(250);
        playSound(36);

    }, 41000);

    setTimeout(function () {

        $('#subTitle137').fadeOut(250);

    }, 43000);

    setTimeout(function () {


        $('#subTitle138').fadeIn(250);
        playSound(37);

    }, 44000);

    setTimeout(function () {

        $('#subTitle138').fadeOut(250);



    }, 51500);

    setTimeout(function () {
        setGameStatus('.per', '080', 'v1');
    }, 52000);

    setTimeout(function () {
        showQuestion7();
    }, 53000);


}

function showQuestion7() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene8_2');
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q7radio2');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion140 , #QueAnsDiv1').fadeIn(500);
    $('#sceneOver').show();
    // $('.QueAnsTitle1').show();
    $('#QueAnsDiv1 ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(7, result[0], result[1]);
                $('#QueAnsDiv1 ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion140 , #QueAnsDiv1').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion8();

                }, 1000);
            }

        }
    });
}

function showQuestion8() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q8radio3');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion143 , #QueAnsDiv2').fadeIn(500);
    $('#sceneOver').show();
    $('#QueAnsDiv2 ul li span').click(function (event) {

        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');

            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(8, result[0], result[1]);
                $('#QueAnsDiv2 ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion143 , #QueAnsDiv2').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    showQuestion9();
                }, 1000);

            }

        }
    });
}

function showQuestion9() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector = $('#q9radio1');

    var targetClickedSelector, targetClickedId, result;
    $('#propOverlayQuestion146 , #QueAnsDiv3').fadeIn(500);
    $('#sceneOver').show();
    $('#QueAnsDiv3 ul li span').click(function (event) {

        if ($(this).attr('readonly') != 'readonly') {

            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector, targetClickedSelector);
            //optional

            if (result != undefined && result.length == 2) {

                sendResults(9, result[0], result[1]);
                $('#QueAnsDiv3 ul li span').attr('readonly', 'readonly');

                setTimeout(function () {
                    $('#propOverlayQuestion146 , #QueAnsDiv3').fadeOut(500);
                    $('#sceneOver').hide();
                    result = '';

                    postQues();
                }, 1000);

            }

        }
    });
}
function sendResults(quesNo, NumberOfAttemps, Matrix) {
    userid = getQueryStringValue("userId");


    qno = quesNo;

    currentTime = new Date();
    timestamp = formatDate(currentTime);

    attempts = NumberOfAttemps;
    matrix = Matrix;


    setTimeout(function () {
        sendScore(userid, vignette, qno, timestamp, attempts, matrix, gameid, handlerURL);
    }, 1000);


}

function postQues() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene8_3');
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    setTimeout(function () {
        playSound(38);
        $('#subTitle152').fadeIn(500);
    }, 2000);

    setTimeout(function () {

        $('#subTitle152').fadeOut(500);
    }, 8000);

    setTimeout(function () {
        playSound(63);
    }, 9000);

    setTimeout(function () {

        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG01_background_12.jpg)').fadeOut(1500);

        setTimeout(function () {
            $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG01_background_13.jpg)').fadeIn(2000);
        }, 0);
        setTimeout(function () {
            swap();
        }, 2000);

    }, 10000);

    setTimeout(function () {
        $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_13.png)');
        $('#phoneMap155').show();
        $('.phoneMap155blueDot').show();
        fadeloop('.phoneMap155blueDot', 200, 200, 500);

    }, 12000);

    setTimeout(function () {
        playSound(64);

    }, 13000);

    setTimeout(function () {
        $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_14.png)');
        $('#phoneMap157').show();
        $('.phoneMap157blueDot').show();
        fadeloop('.phoneMap157blueDot', 200, 200, 500);
    }, 15000);

    setTimeout(function () {
        showMiniQues();
    }, 16000);
}


function showMiniQues() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene8_4');
    localStorage.setItem(userId + '_phonetext', sessionStorage.getItem('status-line-li'));
    $('#propOverlayQuestion158').fadeIn(500);
    $('#sceneOver').show();

}


$('#split').click(function () {
    playSound('0');

    setTimeout(function () {
        playSound(65);
        $('#propOverlayQuestion158').fadeOut(500);
        $('#sceneOver').hide();
        setGameStatus('.per', '090', 'v1');


    }, 1000);

    setTimeout(function () {
        $('#subTitle159').fadeIn(500);

    }, 3000);

    setTimeout(function () {
        $('#subTitle159').fadeOut(500);

    }, 5000);
    setTimeout(function () {

        $('#sceneOver').show();
        $(currentdiv).css('background-image', '').css('background-color', 'black');
        setTimeout(function () {
            $("#newContainer").load("scene9child.html");
        }, 1000);
    }, 6000);

});
$('#join').click(function () {
    playSound('0');
    $('#propOverlayQuestion158').fadeOut(500);
    setGameStatus('.per', '090', 'v1');
    //$('#sceneOver').show();
    $(currentdiv).fadeOut(500).css('background-image', '').css('background-color', 'black');
    setTimeout(function () {
        $("#newContainer").load("scene9child.html");
    }, 1000);
});
