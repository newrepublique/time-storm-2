﻿
//Javascript file for scene2
$(document).ready(function () {
    
    setTimeout(function () {
        $('#TScompleted,#TSprogress').show();
        
    }, 1000);

    setTimeout(function () {        
        playSound(49);
    }, 2000);

    setTimeout(function () {
        textToUpdate = "Retrieving map from cache...";
        classToRenderForLiPhoneText = "listIcon1";
        updatePhoneTxt(textToUpdate, classToRenderForLiPhoneText);

    }, 8500); //6000+1000

    setTimeout(function () {

        playSound(50);

    }, 9000);

    setTimeout(function () {
        textToUpdate = "Map retrieved. You are in The Archive.";
        classToRenderForLiPhoneText = "listIcon1";
        updatePhoneTxt(textToUpdate, classToRenderForLiPhoneText);
    }, 17000);//7000+1000

    setTimeout(function () {
        $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_01.png)');
    }, 18000);

    setTimeout(function () {        
        fadeloop('#objectHighlight8', 500, 500, 1000);
    }, 19000);

    setTimeout(function () {
        $('#subTitle9').fadeIn(250);
        playSound(1);
    }, 20000);

    setTimeout(function () {
        $('#subTitle9').fadeOut(250);
    }, 22000);

    var overlayClosed = false;
    $('#objectHighlight8').click(function () {
        
        playSound('0');
        $('#propOverlayImage11').fadeIn(250);
        $('#objectHighlight8Container').hide();
        setTimeout(function () {
            $('#propOverlayImage11').fadeOut(250);
            $('#propOverlayImage12').fadeIn(250);
            setTimeout(function () {
                if (!overlayClosed) {
                    hideOverlays();
                }
            }, 10000);
        }, 500);
    })

    $('#propOverlayClose').click(function () {

        playSound('0');

        hideOverlays();
        overlayClosed = true;
    })


});

function hideOverlays() {
    $('#propOverlayImage11,#propOverlayImage12').fadeOut(500);

    
    setTimeout(function () {
        playSound(51);
        setTimeout(function () {
            $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG01_background_01.jpg)').fadeOut(1500);
            setTimeout(function () {
                $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG01_background_02.jpg)').fadeIn(2000);
            }, 0);
            setTimeout(function () {
                $('#newContainer').load('scene2child.html');
                swap();
            }, 2000);
        }, 2000);
    }, 500);
}

