﻿//Javascript file for index page


var userId = localStorage.getItem('userId');
//var userId = getQueryStringValue("userId");
//localStorage.setItem('userId', userId);
var isiPad = false;
sessionStorage.setItem('status-line-li', '');
var scene = '';
var section = '';
var phonetextdata = '';
var gendervalue;
var mutesoundvalue;
var progressvalue;
var scene;
var section;
var currentscene = '';
var muteSound;
var currentvignette = '';
var currentdiv = '#TScontainer';
var nextdiv = '#Dummy';
$(document).ready(function () {
    $('#TSframe').css('display', 'none');
    $('#pangoText a').css('display', 'none');
    $('#phoneText ul').css('display', 'none');

    //$('#clicksound').html("<audio id='audioControlClick'><source src='sounds/timestorm_G02_VG01_click_01.wav' type='audio/wav'><source src='sounds/timestorm_G02_VG01_click_01.mp3' type='audio/mpeg'><source src='sounds/timestorm_G02_VG01_click_01.ogg' type='audio/ogg'><embed height='50' width='100' hidden=true autostart=true loop=false src='sounds/timestorm_G02_VG01_click_01.mp3'></audio>");
    
    if (window.location.href.indexOf('userId') > -1) {
        var key = 'userId';
        var value = RegExp("" + key + "[^&]+").exec(window.location.search);
        var queryUserId = unescape(!!value ? value.toString().replace(/^[^=]+./, "") : "");
        if ($.isNumeric(queryUserId) && queryUserId > 0) {
            userId = queryUserId;
            localStorage.setItem('userId', userId);
        }
        else {
            userId = 0;
            localStorage.setItem('userId', userId);
        }

    }
    userId = localStorage.getItem('userId');
    if (userId == null || userId == 0) {
        
        if (navigator && navigator.platform && navigator.userAgent.match('MSIE')) {
            $('body').append('<div id="continueIE" class="stretchBG" style="width: 21%; height: 3.76%; font-size: 30px; position: fixed; z-index: 6; top: 44%; color: #f7dac2; cursor: pointer; text-align: center;left: 40%;border: 4px solid #f7dac2;">Click here to Continue</div>');
            $('#continueIE').click(function () {
                window.location.href = 'index.html';
            });

        }
        $('#Apple').hide();
        //login code here..
        signIn('v1');
       // window.stop();
    }
    else {
        
        getPlatform();
        
        $('#Apple').click(function () {
            $('#Apple').hide();
            getResults();
            //initVignette();

        });


        var firstTime = true;
        $('#TSmute').click(function () {
            playSound('0');
            if (firstTime) {
                for (i = 0; i < sounds.length; i++) {
                    sounds[i].volume = 0;
                }
                sessionStorage.setItem('muteSound', true);
                firstTime = false;
            }
            else {
                setAudio(!(JSON.parse(sessionStorage.getItem('muteSound'))));
                sessionStorage.setItem('muteSound', !(JSON.parse(sessionStorage.getItem('muteSound'))));
            }
        });


        $('#TSsave').click(function () {
            playSound('0');
            sendData();
        });
    }
});

function getPlatform() {

    macCSS();
    if (navigator && navigator.platform && navigator.userAgent.match(/iPad/i)) {
        isiPad = true;
    }
    if (!isiPad) {
        getResults();
        $('#Apple').hide();
        //initVignette();
    }
}

function swap() {
    if (currentdiv == '#TScontainer') {
        currentdiv = '#Dummy';
        nextdiv = '#TScontainer';
    }
    else {
        nextdiv = '#Dummy';
        currentdiv = '#TScontainer';
    }
}

function sendData() {


    localStorage.setItem('userId', userId);
    localStorage.setItem(userId + '_gender', gender);

    localStorage.setItem(userId + '_mutesound', sessionStorage.getItem('muteSound'));
    localStorage.setItem(userId + '_progress', $('.per').html());
    localStorage.setItem(userId + '_lastScene', sessionStorage.getItem(userId + '_lastScene'));
    localStorage.setItem(userId + '_lastSceneSection', sessionStorage.getItem(userId + '_lastSceneSection'));
    localStorage.setItem(userId + '_vignette', currentvignette);

    gendervalue = localStorage.getItem(userId + '_gender');
    mutesoundvalue = sessionStorage.getItem('muteSound');
    localStorage.setItem(userId + '_phonetext', localStorage.getItem('phonetext'));
    phonetextdata = localStorage.getItem(userId + '_phonetext');

    phonetextdata = htmlEscape(phonetextdata);
    progressvalue = $('.per').html();
    scene = sessionStorage.getItem(userId + '_lastScene');
    section = sessionStorage.getItem(userId + '_lastSceneSection');


    var userdetails = {

        vignette: currentvignette,
        gender: gendervalue,
        phonetext: phonetextdata,
        mutesound: mutesoundvalue,
        progress: progressvalue,
        scene: scene,
        section: section

    }

    var details = JSON.stringify(userdetails);

    $.ajax({
        type: 'GET',
        url: '../Handlers/UserDetails.ashx',
        data: {
            userid: userId,
            details: details,
            action: "save"
        },
        context: this,
        dataType: 'text',
        contentType: "application/json; charset=utf-8",
        success: function (msg) {

        },
        error: function () {

        }
    });

}

function getResults() {

    gendervalue = localStorage.getItem(userId + '_gender');
    mutesoundvalue = localStorage.getItem(userId + '_mutesound');
    phonetextdata = localStorage.getItem(userId + '_phonetext');
    progressvalue = localStorage.getItem(userId + '_progress');
    scene = localStorage.getItem(userId + '_lastScene');
    section = localStorage.getItem(userId + '_lastSceneSection');
    currentvignette = localStorage.getItem(userId + '_vignette');

    if (gendervalue != null && mutesoundvalue != null && phonetextdata != null && progressvalue != null && scene != null && section != null) {

        currentscene = scene;

        ///value to be set here....................................

        setGameStatus('.per', progressvalue, 'v1');
        phonetextdata = htmlUnescape(phonetextdata);
        $('#list').html(phonetextdata);
        sessionStorage.setItem('status-line-li', phonetextdata);
        //setAudio(mutesoundvalue);
        sessionStorage.setItem('muteSound', mutesoundvalue);
        initVignette();

    }
    else {


        $.ajax({
            type: 'GET',
            url: '../Handlers/UserDetails.ashx',
            data: {
                userid: userId,
                action: "get"

            },
            context: this,
            dataType: 'text',
            contentType: "application/json; charset=utf-8",
            success: function (detailsdata) {


                if (detailsdata != undefined && detailsdata != null && jQuery.isEmptyObject(detailsdata) == false) {

                    var parsedData = JSON.parse(detailsdata);

                    localStorage.setItem(userId + '_gender', parsedData.gender);
                    localStorage.setItem(userId + '_phonetext', parsedData.phonetext);
                    localStorage.setItem(userId + '_mutesound', parsedData.mutesound);
                    localStorage.setItem(userId + '_progress', parsedData.progress);
                    localStorage.setItem(userId + '_lastScene', parsedData.scene);
                    currentscene = parsedData.scene;
                    localStorage.setItem(userId + '_lastSceneSection', parsedData.section);

                    localStorage.setItem(userId + '_vignette', parsedData.vignette);
                    currentvignette = parsedData.vignette;
                    //values to be set here...
                    setGameStatus('.per', parsedData.progress, 'v1');

                    parsedData.phonetext = htmlUnescape(parsedData.phonetext);
                    $('#list').html(parsedData.phonetext);
                    sessionStorage.setItem('status-line-li', parsedData.phonetext);

                    //setAudio(parsedData.mutesound);
                    sessionStorage.setItem('muteSound', parsedData.mutesound);
                    initVignette();

                } else {
                    initVignette();
                }

            },
            error: function () {
                console.log('error');
            }
        });
        //initVignette();
    }

}

function htmlEscape(str) {
    return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
}

function htmlUnescape(value) {
    return String(value)
        .replace(/&quot;/g, '"')
        .replace(/&#39;/g, "'")
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&amp;/g, '&');
}
var imgArray = Array(74);
var filesArray = Array(141);
var sounds = Array(68);
var sndArray = Array(68);

function preload() {
    var gender = 'male';
    if (localStorage.getItem(userId + '_gender') != null) {
        gender = localStorage.getItem(userId + '_gender');
    }
    document.getElementById('loading').style.display = 'block';

    var path = 'sounds/';
    var ext = '.mp3';
    if (BrowserDetect.browser == 'Safari') ext = '.mp3';
    if (BrowserDetect.browser == 'Firefox') ext = '.ogg';
    //filling in the sndArray
    sndArray = [
    path + 'timestorm_G02_VG01_click_01' + ext,
	path + gender + '/timestorm_G02_VG01_voiceover_01' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_02' + ext,
    path + gender + '/timestorm_G02_VG01_voiceover_03' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_04' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_05' + ext,
    path + gender + '/timestorm_G02_VG01_voiceover_06' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_07' + ext,
    path + gender + '/timestorm_G02_VG01_voiceover_08' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_09' + ext,
    path + gender + '/timestorm_G02_VG01_voiceover_10' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_11' + ext,
    path + gender + '/timestorm_G02_VG01_voiceover_12' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_13' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_14' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_15' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_16' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_17' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_18' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_19' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_20' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_21' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_22' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_23' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_24' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_25' + ext,
    path + gender + '/timestorm_G02_VG01_voiceover_26' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_27' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_28' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_29' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_30' + ext,
    path + gender + '/timestorm_G02_VG01_voiceover_31' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_32' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_33' + ext,
    path + gender + '/timestorm_G02_VG01_voiceover_34' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_35' + ext,
    path + gender + '/timestorm_G02_VG01_voiceover_36' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_37' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_38' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_39' + ext,
    path + gender + '/timestorm_G02_VG01_voiceover_40' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_41' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_42' + ext,
    path + gender + '/timestorm_G02_VG01_voiceover_43' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_44' + ext,
    path + gender + '/timestorm_G02_VG01_voiceover_45' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_46' + ext,
    path + gender + '/timestorm_G02_VG01_voiceover_47' + ext,
    path + 'confero/timestorm_G02_VG01_voiceover_48' + ext,
    path + 'timestorm_G02_VG01_soundfx_01' + ext, //49
    path + 'timestorm_G02_VG01_soundfx_02' + ext,
    path + 'timestorm_G02_VG01_soundfx_03' + ext,
    path + 'timestorm_G02_VG01_soundfx_04' + ext,
    path + 'timestorm_G02_VG01_soundfx_05' + ext,
    path + 'timestorm_G02_VG01_soundfx_07' + ext,
    path + 'timestorm_G02_VG01_soundfx_08' + ext,//55
    path + 'timestorm_G02_VG01_soundfx_09' + ext,
    path + 'timestorm_G02_VG01_soundfx_10' + ext,
    path + 'timestorm_G02_VG01_soundfx_11' + ext,
    path + 'timestorm_G02_VG01_soundfx_12' + ext,
    path + 'timestorm_G02_VG01_soundfx_13' + ext,//60
    path + 'timestorm_G02_VG01_soundfx_14' + ext,
    path + 'timestorm_G02_VG01_soundfx_15' + ext,
    path + 'timestorm_G02_VG01_soundfx_16' + ext,
    path + 'timestorm_G02_VG01_soundfx_17' + ext,
    path + 'timestorm_G02_VG01_soundfx_18' + ext,
    path + 'timestorm_G02_VG01_soundfx_19' + ext,
    path + 'timestorm_G02_VG01_soundfx_20' + ext

    ];

    path = 'images/';
    imgArray = [
        path + 'btnStart.jpg',
        path + 'chapter1.jpg',
        path + 'gameTitle.png',
    path + 'greenDot.png',
    path + 'hand.png',
	path + 'icon.png',
	path + 'icon1.png',
	path + 'icon2.png',
	path + 'icon3.png',
	path + 'iconAns.png',
	path + 'loading.gif',
	path + 'mute-1.png',
	path + 'mute-2.png',
	path + 'save-1.png',
	path + 'save-2.png',
    path + 'save.png',
	path + 'scroll.png',
	path + 'scroll1.png',
	path + 'scroll2.png',
	path + 'scroll3.png',
	path + 'timer.png',
	path + 'timestorm_G02_VG01_answer_01.png',
	path + 'timestorm_G02_VG01_background_01.jpg',
	path + 'timestorm_G02_VG01_background_02.jpg',
    path + 'timestorm_G02_VG01_background_03.jpg',
    path + 'timestorm_G02_VG01_background_04.jpg',
    path + 'timestorm_G02_VG01_background_05.jpg',
    path + 'timestorm_G02_VG01_background_06.jpg',
    path + 'timestorm_G02_VG01_background_07.jpg',
    path + 'timestorm_G02_VG01_background_08.jpg',
    path + 'timestorm_G02_VG01_background_09.jpg',
    path + 'timestorm_G02_VG01_background_10.jpg',
    path + 'timestorm_G02_VG01_background_11.jpg',
    path + 'timestorm_G02_VG01_background_12.jpg',
    path + 'timestorm_G02_VG01_background_13.jpg',
    path + 'timestorm_G02_VG01_background_14.jpg',
    path + 'timestorm_G02_VG01_background_15.jpg',
    path + 'timestorm_G02_VG01_bulletpoint_01.png',
    path + 'timestorm_G02_VG01_miniquestion_01.png',
	path + 'timestorm_G02_VG01_headsupdisplay_01.png',
    path + 'timestorm_G02_VG01_Objecthighlight_01.png',
    path + 'timestorm_G02_VG01_Objecthighlight_02.png',
    path + 'timestorm_G02_VG01_Objecthighlight_03.png',
    path + 'timestorm_G02_VG01_Objecthighlight_04.png',
    path + 'timestorm_G02_VG01_Objecthighlight_05.png',
    path + 'timestorm_G02_VG01_Objecthighlight_06.png',
    path + 'timestorm_G02_VG01_Objecthighlight_07.png',
	path + 'timestorm_G02_VG01_phonemap_01.png',
    path + 'timestorm_G02_VG01_phonemap_02.png',
    path + 'timestorm_G02_VG01_phonemap_03.png',
    path + 'timestorm_G02_VG01_phonemap_04.png',
    path + 'timestorm_G02_VG01_phonemap_05.png',
    path + 'timestorm_G02_VG01_phonemap_06.png',
    path + 'timestorm_G02_VG01_phonemap_07.png',
    path + 'timestorm_G02_VG01_phonemap_08.png',
    path + 'timestorm_G02_VG01_phonemap_09.png',
    path + 'timestorm_G02_VG01_phonemap_10.png',
    path + 'timestorm_G02_VG01_phonemap_11.png',
    path + 'timestorm_G02_VG01_phonemap_12.png',
    path + 'timestorm_G02_VG01_phonemap_13.png',
    path + 'timestorm_G02_VG01_phonemap_14.png',
	path + 'timestorm_G02_VG01_propoverlay_01.png',
    path + 'timestorm_G02_VG01_propoverlay_02.png',
    path + 'timestorm_G02_VG01_propoverlay_03.png',
    path + 'timestorm_G02_VG01_propoverlay_04.png',
    path + 'timestorm_G02_VG01_propoverlay_05.png',
    path + 'timestorm_G02_VG01_propoverlay_06.png',
    path + 'timestorm_G02_VG01_propoverlay_07.png',
	path + 'timestorm_G02_VG01_Question_01.png',
    path + 'timestorm_G02_VG01_Question_02.png',
    path + 'timestorm_G02_VG01_Question_03.png',
	path + 'timestorm_G02_VG01_timer_01.png',
    path + 'timestorm_G02_VG01_speaker_01.png'
    ];

    //loading sounds
    for (i = 0; i < 68; i++) {
        filesArray[i] = sndArray[i];

        sounds[i] = loadSound(filesArray[i]);
    }

    for (i = 68; i < 141; i++) {
        filesArray[i] = imgArray[i - 68];
        //console.log('loading '+filesArray[i]);
        loadImg(filesArray[i]);
    }


}


function loadImg(which) {
    var img = new Image();
    img.src = which;
    img.onload = function () {
        rm4Array(which);
    }
}

var count = filesArray.length;

function loadSound(which) {

    var snd = new Audio();
    snd.src = which;

    if (navigator.userAgent.match(/iPad/i)) {
        document.getElementById('loadingPercent').innerHTML = Math.floor((141 - count) * 100 / 141);
        count--;
        if (which == 'sounds/timestorm_G02_VG01_soundfx_20.mp3') {

            document.getElementById('loading').style.display = 'none';
            init();
        }
    }
    else {

        snd.addEventListener('canplaythrough', function () {
            rm4Array(which);
        });
        snd.onerror = function (e) {//document.getElementById('temp').innerHTML=snd.src+' gave error';
            console.log(snd.src + ' gave an error');
        }
    }

    return snd;
}

function rm4Array(what) {

    for (i = 0; i < filesArray.length; i++) {
        if (filesArray[i] == what) {
            filesArray.splice(i, 1);

            document.getElementById('loadingPercent').innerHTML = Math.floor((141 - filesArray.length) * 100 / 141);
        }
        if (filesArray.length < 1) {
            init();
            document.getElementById('loading').style.display = 'none';
        }
    }


}

function initVignette() {
    
    switch (currentvignette) {
        case 'v2': window.location.href = '../vignette2/index.html';
            break;
        case 'v3': window.location.href = '../vignette3/index.html';
            break;
        case 'v4': window.location.href = '../vignette4/index.html';
            break;
        case 'v5': window.location.href = '../vignette5/index.html';
            break;
        default:
            preload();
    }
}

function init() {

    currentvignette = 'v1';
    $('#startScreen').fadeIn(1000);
    setTimeout(function () {
        $('#startScreen').fadeOut(1000);

    }, 5000);


    setTimeout(function () {
        localStorage.setItem(userId + '_vignette', currentvignette);
        switch (currentscene) {

            case 'scene2': $('#newContainer').load('scene2child.html');
                break;
            case 'scene3': $('#newContainer').load('scene3child.html');
                break;
            case 'scene4': $('#newContainer').load('scene4child.html');
                break;
            case 'scene5': $('#newContainer').load('scene5child.html');
                break;
            case 'scene6': $('#newContainer').load('scene6child.html');
                break;
            case 'scene7': $('#newContainer').load('scene7child.html');
                break;
            case 'scene8': $('#newContainer').load('scene8child.html');
                break;
            case 'scene9': $('#newContainer').load('scene9child.html');
                break;
            default: $('#newContainer').load('scene1child.html'); $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG01_background_01.jpg)').stop(true, true).hide().fadeIn(1000);
        }
    }, 6000);

    setTimeout(function () {

        $('#TSframe').css('display', 'block');
        $('#pangoText a').css('display', 'block');
        $('#phoneText ul').css('display', 'block');
        $('#TSscene').css('opacity', '1');
    }, 7500);
}

var BrowserDetect = {
    init: function () {
        this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
        this.version = this.searchVersion(navigator.userAgent)
            || this.searchVersion(navigator.appVersion)
            || "an unknown version";
        this.OS = this.searchString(this.dataOS) || "an unknown OS";
    },
    searchString: function (data) {
        for (var i = 0; i < data.length; i++) {
            var dataString = data[i].string;
            var dataProp = data[i].prop;
            this.versionSearchString = data[i].versionSearch || data[i].identity;
            if (dataString) {
                if (dataString.indexOf(data[i].subString) != -1)
                    return data[i].identity;
            }
            else if (dataProp)
                return data[i].identity;
        }
    },
    searchVersion: function (dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index == -1) return;
        return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
    },
    dataBrowser: [
        {
            string: navigator.userAgent,
            subString: "Chrome",
            identity: "Chrome"
        },
        {
            string: navigator.userAgent,
            subString: "OmniWeb",
            versionSearch: "OmniWeb/",
            identity: "OmniWeb"
        },
        {
            string: navigator.vendor,
            subString: "Apple",
            identity: "Safari",
            versionSearch: "Version"
        },
        {
            prop: window.opera,
            identity: "Opera",
            versionSearch: "Version"
        },
        {
            string: navigator.vendor,
            subString: "iCab",
            identity: "iCab"
        },
        {
            string: navigator.vendor,
            subString: "KDE",
            identity: "Konqueror"
        },
        {
            string: navigator.userAgent,
            subString: "Firefox",
            identity: "Firefox"
        },
        {
            string: navigator.vendor,
            subString: "Camino",
            identity: "Camino"
        },
        {		// for newer Netscapes (6+)
            string: navigator.userAgent,
            subString: "Netscape",
            identity: "Netscape"
        },
        {
            string: navigator.userAgent,
            subString: "MSIE",
            identity: "Explorer",
            versionSearch: "MSIE"
        },
        {
            string: navigator.userAgent,
            subString: "Gecko",
            identity: "Mozilla",
            versionSearch: "rv"
        },
        { 		// for older Netscapes (4-)
            string: navigator.userAgent,
            subString: "Mozilla",
            identity: "Netscape",
            versionSearch: "Mozilla"
        }
    ],
    dataOS: [
        {
            string: navigator.platform,
            subString: "Win",
            identity: "Windows"
        },
        {
            string: navigator.platform,
            subString: "Mac",
            identity: "Mac"
        },
        {
            string: navigator.userAgent,
            subString: "iPhone",
            identity: "iPhone/iPod"
        },
        {
            string: navigator.platform,
            subString: "Linux",
            identity: "Linux"
        }
    ]

};
BrowserDetect.init();

function playSound(which) {
    if (which == '' || typeof (which) == 'undefined') { return; }
    sounds[which].play();
}

function macCSS() {

    if (navigator.userAgent.toUpperCase().indexOf('MAC') >= 0) {
        console.log('mac');
        $('body').addClass('mac');
    }

}