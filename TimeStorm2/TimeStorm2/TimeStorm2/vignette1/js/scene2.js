﻿function getFrameID(id) {
    var elem = document.getElementById(id);
    if (elem) {
        if (/^iframe$/i.test(elem.tagName)) return id; //Frame, OK
        // else: Look for frame
        var elems = elem.getElementsByTagName("iframe");
        if (!elems.length) return null; //No iframe found, FAILURE
        for (var i = 0; i < elems.length; i++) {
            if (/^https?:\/\/(?:www\.)?youtube(?:-nocookie)?\.com(\/|$)/i.test(elems[i].src)) break;
        }
        elem = elems[i]; //The only, or the best iFrame
        if (elem.id) return elem.id; //Existing ID, return it
        // else: Create a new ID
        do { //Keep postfixing `-frame` until the ID is unique
            id += "-frame";
        } while (document.getElementById(id));
        elem.id = id;
        return id;
    }
    // If no element, return null.
    return null;
}

// Define YT_ready function.
var YT_ready = (function () {
    var onReady_funcs = [], api_isReady = false;
    /* @param func function     Function to execute on ready
     * @param func Boolean      If true, all qeued functions are executed
     * @param b_before Boolean  If true, the func will added to the first
                                 position in the queue*/
    return function (func, b_before) {
        if (func === true) {
            api_isReady = true;
            while (onReady_funcs.length) {
                // Removes the first func from the array, and execute func
                onReady_funcs.shift()();
            }
        } else if (typeof func == "function") {
            if (api_isReady) func();
            else onReady_funcs[b_before ? "unshift" : "push"](func);
        }
    }
})();
// This function will be called when the API is fully loaded
function onYouTubePlayerAPIReady() { YT_ready(true) }

// Load YouTube Frame API
(function () { // Closure, to not leak to the scope
    var s = document.createElement("script");
    s.src = (location.protocol == 'https:' ? 'https' : 'http') + "://www.youtube.com/player_api";
    var before = document.getElementsByTagName("script")[0];
    before.parentNode.insertBefore(s, before);
})();
var player; //Define a player object, to enable later function calls, without
// having to create a new class instance again.

// Add function to execute when the API is ready
YT_ready(function () {
    var frameID = getFrameID("propOverlayImage36");
    if (frameID) { //If the frame exists
        player = new YT.Player(frameID, {
            events: {
                "onReady": onPlayerReady,
                "onStateChange": stopCycle
            }
        });
    }
});
function onPlayerReady(event) {

}
// Example: function stopCycle, bound to onStateChange
function stopCycle(event) {
    if (event.data == YT.PlayerState.ENDED || event.data == 0) {
        debugger;
        setTimeout(function () {
            if ($('#propOverlayImage36').is(':visible')) {
                sceneChange();
            }
        }, 10000);
    }
}

var videoOverlayClicked = false;
$(document).ready(function () {


    $('#TSprogress,#TScompleted').show();
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_01.png)');  //from scene1
    setTimeout(function () {
        $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_02.png)'); ///scene2
    }, 1000);

    setTimeout(function () {
        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG01_background_02.jpg)');
        sessionStorage.setItem(userId + '_lastScene', 'scene2');    //save game

        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {


            case 'scene2_2': scene2_2();
                break;

            case 'scene2_3': scene2_3();

                break;

            default: scene2_1();

        }



    }, 2000);

    $('.propClose').click(function () {
        videoOverlayClicked = true;
        playSound('0');
        sceneChange();
    })
});

function scene2_1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene2_1');
    //Orange-1 New recruit dialogue

    setTimeout(function () {

        playSound(2);
        $('#subTitle17_1').fadeIn(250);

    }, 500);

    setTimeout(function () {

        $('#subTitle17_1').fadeOut(250);
    }, 10500 + delay2_1);


    ////White-1 who are you
    setTimeout(function () {

        playSound(3);
        $('#subTitle18').fadeIn(250);

    }, 11000 + delay2_1);

    setTimeout(function () {

        $('#subTitle18').fadeOut(250);

    }, 13000 + delay2_1);
    ///////who are you ended


    //////Orange-2 rude
    setTimeout(function () {
        playSound(4);

        $('#subTitle19').fadeIn(250);

    }, 13500 + delay2_1);

    setTimeout(function () {

        $('#subTitle19').fadeOut(250);
    }, 16500 + delay2_1);
    ////rude ended


    setTimeout(function () {
        playSound(52);
    }, 17000 + delay2_1);

    setTimeout(function () {

    }, 18000 + delay2_1);

    //////Orange-3 Confero
    setTimeout(function () {

        playSound(5);
        $('#subTitle21_1').fadeIn(250);

    }, 18000 + delay2_1);

    setTimeout(function () {

        $('#subTitle21_1').fadeOut(250);
    }, 28500 + delay2_1);
    ///////Confero ended




    setTimeout(function () {
        updatePhoneTxt('You have met Confero, the Archivist', 'listIcon1');

    }, 29000 + delay2_1);

    //Play sound dog barks
    setTimeout(function () {
        playSound(53);

    }, 29500 + delay2_1);


    //Modified dialogue
    setTimeout(function () {
        playSound(6);
        $('#subTitle24').fadeIn(250);

    }, 30500 + delay2_1);
    setTimeout(function () {

        $('#subTitle24').fadeOut(250);
    }, 34000 + delay2_1);

    ////Modified dialogue completed

    setTimeout(function () {
        playSound(7);
        $('#subTitle25_1').fadeIn(250);

    }, 34500 + delay2_1); //36500
    setTimeout(function () {

        $('#subTitle25_1').fadeOut(250);
        $('#subTitle25_2').fadeIn(250);

    }, 46500 + delay2_1);


    setTimeout(function () {

        $('#subTitle25_2').fadeOut(250);
        $('#subTitle25_3').fadeIn(250);

    }, 57500 + delay2_1);


    setTimeout(function () {

        $('#subTitle25_3').fadeOut(250);

    }, 71500 + delay2_1);

    ///////Inquisitive ended



    //why am I here
    setTimeout(function () {
        playSound(8);
        $('#subTitle26').fadeIn(250);

    }, 72000 + delay2_1);
    setTimeout(function () {

        $('#subTitle26').fadeOut(250);

    }, 75500 + delay2_1);
    //why am I here ended


    setTimeout(function () {
        setGameStatus('.per', '010', 'v1');
    }, 76000 + delay2_1);

    setTimeout(function () {
        scene2_2();
    }, 76500 + delay2_1);


    // scene2_2();
}




function scene2_2() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene2_2');

    //Questions questions
    setTimeout(function () {
        playSound(9);

        $('#subTitle28_1').fadeIn(250);

    }, 0);
    setTimeout(function () {

        $('#subTitle28_1').fadeOut(250);

    }, 5500 + delay2_1);

    //Questions questions ended


    ///dog bark corrected file
    setTimeout(function () {

        playSound(53);

    }, 6000 + delay2_1);


    ////Work?
    setTimeout(function () {
        playSound(10);
        $('#subTitle30').fadeIn(250);

    }, 7000 + delay2_1);
    setTimeout(function () {

        $('#subTitle30').fadeOut(250);
    }, 8000 + delay2_1);
    //work? ended


    //Yes child
    setTimeout(function () {

        playSound(11);
        $('#subTitle31_1').fadeIn(250);

    }, 8500 + delay2_1);
    setTimeout(function () {

        $('#subTitle31_1').fadeOut(250);
        $('#subTitle31_2').fadeIn(250);

    }, 17500 + delay2_1);


    setTimeout(function () {

        $('#subTitle31_2').fadeOut(250);
    }, 24000 + delay2_1);

    //Yes child ended


    //Why?
    setTimeout(function () {
        playSound(12);
        $('#subTitle32').fadeIn(250);

    }, 24500 + delay2_1);
    setTimeout(function () {

        $('#subTitle32').fadeOut(250);
    }, 25500 + delay2_1);

    //Why ended


    //question of survival
    setTimeout(function () {
        playSound(13);
        $('#subTitle33_1').fadeIn(250);

    }, 26000 + delay2_1);
    setTimeout(function () {

        $('#subTitle33_1').fadeOut(250);

    }, 35500 + delay2_1);

    //question of survival ended

    //Clock sound
    setTimeout(function () {
        playSound(54);

    }, 36000 + delay2_1);    //41000


    setTimeout(function () {

        playSound(14);
        $('#subTitle35_1').fadeIn(250);

    }, 46500 + delay2_1); //39500
    setTimeout(function () {

        $('#subTitle35_1').fadeOut(250);

    }, 55500 + delay2_1);


    //    //Test ended incomplete
    setTimeout(function () {

        scene2_3();

    }, 56000 + delay2_1);
    //scene2_3();
}
function scene2_3() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene2_3');
    $('#propOverlayImage36, .propClose').fadeIn(250);
    $('#sceneOver').show();

    $('#subTitle36').fadeIn(250);
    $('iframe').css('display', 'block');
    player.playVideo();
    //$('iframe').attr('src', $('iframe').attr('src')+'&autoplay=1');


    //setTimeout(function () {
    //    if (!videoOverlayClicked) {
    //        sceneChange();
    //    }
    //}, 100000);   

}

function sceneChange() {

    $('#propOverlayImage36, #subTitle36, .propClose').fadeOut(250);

    setTimeout(function () {
        $('#newContainer').load('scene3child.html');
    }, 1000);

}



