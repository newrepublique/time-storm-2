﻿$(document).ready(function () {
    $('#TSprogress,#TScompleted').show();
 
    sessionStorage.setItem(userId + '_lastScene', 'scene6');    //save game

    $("#TScontainer").css('background-image', 'url(./images/timestorm_G02_VG01_background_07.jpg)').stop(true, true).hide().fadeIn(1000);
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_05.png)');  //from scene5

        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {


            case 'large_dialogue': large_dialogue();
                $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_07.png)'); //from scene6_1

                break;

            case 'scene6_next': scene6_next();
                $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_07.png)'); //from scene6_1

                break;

            default: scene6_1();
               

        }
    
    
   
});


//Ceiling fan play
function scene6_1() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene6_1');
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));

    setTimeout(function () {
        
        playSound(60);
    }, delay6);


    setTimeout(function () {

        $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_07.png)');
      
    }, 9000 + delay6);


    //1942 dialogue
    setTimeout(function () {
        playSound(25);
        $('#subTitle111_1').fadeIn(250);

    }, 10000 + delay6);

    setTimeout(function () {

        $('#subTitle111_1').fadeOut(250);
    }, 21000 + delay6);
    //1942 ended


    //Why stop
    setTimeout(function () {
        playSound(26);

        $('#subTitle112').fadeIn(250);

    }, 22000 + delay6);
    setTimeout(function () {

        $('#subTitle112').fadeOut(250);

    }, 24000 + delay6);

    //Why stop ended


    //Large dialogue
    setTimeout(function () {
       

        large_dialogue();

    }, 25000 + delay6);
}



function large_dialogue() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'large_dialogue');
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    setTimeout(function () {
        playSound(27);
        $('#subTitle113_1').fadeIn(250);

    }, 0);
    setTimeout(function () {

        $('#subTitle113_1').fadeOut(250);
        $('#subTitle113_2').fadeIn(250);

    }, 11000 + delay6);
   
    setTimeout(function () {

        $('#subTitle113_2').fadeOut(250);
        $('#subTitle113_3').fadeIn(250);

    }, 26000 + delay6);
   
    setTimeout(function () {

        $('#subTitle113_3').fadeOut(250);
        $('#subTitle113_4').fadeIn(250);

    }, 39000 + delay6);
   
    setTimeout(function () {

        $('#subTitle113_4').fadeOut(250);
        $('#subTitle113_5').fadeIn(250);

    }, 51000 + delay6);
   
    setTimeout(function () {

        $('#subTitle113_5').fadeOut(250);
        $('#subTitle113_6').fadeIn(250);

    }, 57000 + delay6);
  
    setTimeout(function () {

        $('#subTitle113_6').fadeOut(250);
        $('#subTitle113_7').fadeIn(250);

    }, 71000 + delay6);
  
    setTimeout(function () {

        $('#subTitle113_7').fadeOut(250);
        $('#subTitle113_8').fadeIn(250);

    }, 84000 + delay6);
   
    setTimeout(function () {

        $('#subTitle113_8').fadeOut(500);
        $('#subTitle113_9').fadeIn(500);

    }, 92000 + delay6);
   
    setTimeout(function () {

        $('#subTitle113_9').fadeOut(250);
    }, 106000 + delay6);

   
    setTimeout(function () {

        setGameStatus('.per', '060', 'v1');     
        playSound(62);
       
    }, 106500 + delay6);    //106000
    //Large dialogue ended


    setTimeout(function () {
        scene6_next();
    }, 115000 + delay6);
 
  
}

function scene6_next() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene6_next');
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));

    setTimeout(function () {
        playSound(29);

        $('#subTitle115').fadeIn(250);

    }, delay6);

    setTimeout(function () {
       
        $('#subTitle115').fadeOut(250);

    }, 2000 + delay6);
    setTimeout(function () {
       
        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG01_background_07.jpg)').fadeOut(1500);

        setTimeout(function () {
            $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG01_background_09.jpg)').fadeIn(2000);
        }, 0);
    
        setTimeout(function () {
            swap();

        }, 2000);
    }, 4000 + delay6);

    setTimeout(function () {
        playSound(30);

        $('#subTitle118').fadeIn(250);

    }, 6500+delay6);

    setTimeout(function () {

        $('#subTitle118').fadeOut(250);

    }, 10000 + delay6);

    setTimeout(function () {
        $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_08.png)');
        $('#phoneMap119').fadeIn(500);
        $('.phoneMap119blueDot').show();
        fadeloop('.phoneMap119blueDot', 200, 200, 500);
    }, 11000 + delay6);


    setTimeout(function () {
        $('#subTitle120').fadeIn(250);
    }, 12000 + delay6);

    

}

function bluedot() {

   
       // PlaySoundWithDelay($divElementRenderAudioTags, 0, click_sound_file_url_mp3, click_sound_file_url_ogg, click_sound_file_url_wav);
    playSound('0');
    $(currentdiv).fadeOut(250);
  
    setTimeout(function () {
        $('#newContainer').load('scene7child.html');
    }, 1000);

}