﻿
/* This will be used to define common static variables within the application. */


var count = 0;
var start = new Date().getTime();
var matrix = "";
var $divElementRenderAudioTags = $('#soundForAllBrowsers');
var NumberOfAttemps = 0;
var gender = 'male';
if (localStorage.getItem(userId + '_gender') != null) {
    gender = localStorage.getItem(userId + '_gender');
}

var userid = '';

var vignette = 1;
var qno = 0;
var currentTime;
var timestamp;
var attempts;
var gameid = "TS2";
var handlerURL = '../Handlers/SendResults.ashx';
var click_sound_file_url_wav = "";//sounds/timestorm_G02_VG01_click_01.wav";
var click_sound_file_url_ogg = "sounds/timestorm_G02_VG01_click_01.ogg";
var click_sound_file_url = "sounds/timestorm_G02_VG01_click_01.mp3";


var confero_voiceover_15_wav = '';//sounds/confero/timestorm_G02_VG01_voiceover_15.wav';
var confero_voiceover_16_wav = '';//sounds/confero/timestorm_G02_VG01_voiceover_16.wav';
var confero_voiceover_17_wav = '';//sounds/confero/timestorm_G02_VG01_voiceover_17.wav';
var confero_voiceover_18_wav = '';//sounds/confero/timestorm_G02_VG01_voiceover_18.wav';

var dog_scamper_sound_wav = '';//sounds/Sounds/timestorm_G02_VG01_soundfx_08.wav';

var confero_voiceover_15 = 'sounds/confero/timestorm_G02_VG01_voiceover_15.mp3';
var confero_voiceover_16 = 'sounds/confero/timestorm_G02_VG01_voiceover_16.mp3';
var confero_voiceover_17 = 'sounds/confero/timestorm_G02_VG01_voiceover_17.mp3';
var confero_voiceover_18 = 'sounds/confero/timestorm_G02_VG01_voiceover_18.mp3';

var dog_scamper_sound = 'sounds/timestorm_G02_VG01_soundfx_08.mp3';

var confero_voiceover_15_ogg = 'sounds/confero/timestorm_G02_VG01_voiceover_15.ogg';
var confero_voiceover_16_ogg = 'sounds/confero/timestorm_G02_VG01_voiceover_16.ogg';
var confero_voiceover_17_ogg = 'sounds/confero/timestorm_G02_VG01_voiceover_17.ogg';
var confero_voiceover_18_ogg = 'sounds/confero/timestorm_G02_VG01_voiceover_18.ogg';

var dog_scamper_sound_ogg = 'sounds/timestorm_G02_VG01_soundfx_08.ogg';


var overlay_video_url = 'http://www.youtube.com/embed/XGSy3_Czz8k?autoplay=1';