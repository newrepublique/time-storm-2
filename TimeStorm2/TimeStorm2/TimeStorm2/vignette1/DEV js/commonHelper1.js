﻿

/*---------------------------------------------------------------------- */


/*Calculate Number of attempts and Matrix:
Variables start, matrix and Number of attempts need to be passed everytime this method is call for a Q&A 
---------------------------------------------------------------------- */
var start = new Date().getTime();
var matrix = "";
var NumberOfAttemps = 0;
function CalculateAttemptsAndAMtrix1(correctAnswerSelector, targetClickedSelector) {
    var elapsed = new Date().getTime() - start;
    if (matrix == "") {
        matrix++;
        matrix = elapsed;
    }
    else {
        matrix = matrix + ":" + elapsed;
    }

    NumberOfAttemps++;

    if (correctAnswerSelector.is(targetClickedSelector)) {
        $(targetClickedSelector).css('color', 'green');
        return [NumberOfAttemps, "[" + matrix + "]"];
    }
    else {
        $(targetClickedSelector).attr('disabled', 'disabled');
        $(targetClickedSelector).css('color', 'red');
    }
}

/*---------------------------------------------------------------------- */

