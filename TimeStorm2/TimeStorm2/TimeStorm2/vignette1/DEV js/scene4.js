﻿

$(document).ready(function () {

    sessionStorage.setItem(userId + '_lastScene', 'scene4');
    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG01_background_04.jpg)').stop(true, true).hide().fadeIn(1000);
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_04.png)');
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    setTimeout(function () {
        $('#TScompleted,#TSprogress').show();
    }, 1000);

    setTimeout(function () {
        setGameStatus('.per', '030', 'v1');
        
    }, 2000);

    setTimeout(function () {
        $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_05.png)');
       
    }, 3000);

    setTimeout(function () {
        playSound(56);
    }, 4000);
    
    setTimeout(function () {
        

            var section = localStorage.getItem(userId + '_lastSceneSection');
            switch (section) {

                case 'scene4_0': scene4_0();
                    break;
                case 'scene4_1': scene4_1();
                    break;
                
                case 'scene4_2': postQuestion();
                    break;

                default: scene4_0();

            }

        
    }, 6000);
});



function scene4_0() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene4_0');
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    setTimeout(function () {

        $('#subTitle72_1').fadeIn(250);
        playSound(19);
        
    }, 1000);

    setTimeout(function () {

        $('#subTitle72_1').fadeOut(250);
    }, 13000);
    
    setTimeout(function () {
        scene4_1();
    }, 14000);

}

function scene4_1() {

    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene4_1');
    var count = 0;
    $('#objectHighlight73-1','#objectHighlight73-2','#objectHighlight73-3').show();
    fadeloop('#objectHighlight73-1', 500, 500, 1000);
    fadeloop('#objectHighlight73-2', 500, 500, 1000);
    fadeloop('#objectHighlight73-3', 500, 500, 1000);

    $('#objectHighlight73-3').click(function () {

        playSound('0');
        $('#sceneOver').show();
        $('#cover3').hide();
        setTimeout(function () {
            
            $('#subTitle74').fadeIn(250);
        }, 1000);
        setTimeout(function () {
            $('#subTitle74').fadeOut(250);
            $('#sceneOver').hide();

        }, 5000);

        

    });
    $('#objectHighlight73-1').click(function () {

        playSound('0');
        $('#cover1').hide();
        $('#propOverlayQuestion75').fadeIn(250);
        $('#sceneOver').show();

        $('#yes1').click(function () {
            $('#propOverlayQuestion75').fadeOut(250);
           
            setTimeout(function () {
                $('#minisubtitle').fadeIn(250);
                playSound(57);
            }, 1000);
            setTimeout(function () {
                $('#minisubtitle').fadeOut(250);
                
                $('#sceneOver').hide();
                

            }, 3000);
        });

        $('#no1').click(function () {
            $('#propOverlayQuestion75').fadeOut(250);
           
            setTimeout(function () {
                playSound(58);
                
                $('#sceneOver').hide();
            }, 2000);
            

        });


    });
    $('#objectHighlight73-2').click(function () {
        playSound('0');
        $('#cover2').hide();
        $('#cover1').hide();
        $('#cover3').hide();
        $('#sceneOver').show();
       
        setTimeout(function () {
            $('#subTitle78').fadeIn(250);
            playSound(20);
        }, 1000);

        setTimeout(function () {
            $('#subTitle78').fadeOut(250);
            
        }, 5500);

        setTimeout(function () {

            $('#propOverlayQuestion79').fadeIn(250);

            $('#yes2').click(function () {
                $('#propOverlayQuestion79').fadeOut(250);
                setTimeout(function () {
                    
                    $('#propOverlayImage81').fadeIn(250);

                }, 1000);
                setTimeout(function () {

                    $('#propOverlayImage81').fadeOut(250);
                    $('#sceneOver').hide();
                   
                        postQuestion();
                   

                }, 4000);
            });

            $('#no2').click(function () {
               

                $('#propOverlayQuestion79').fadeOut(250);
                $('#sceneOver').hide();

                setTimeout(function () {
                    playSound(59);
               
                }, 1000);
                setTimeout(function () {
                    postQuestion();
                }, 6000);
            });
        }, 6500);

    });
}



function postQuestion() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene4_2');
    var delay = 1000;
    setTimeout(function () {

        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG01_background_04.jpg)').fadeOut(1500);

        setTimeout(function () {
            $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG01_background_05.jpg)').fadeIn(2000);
        }, 0);
        setTimeout(function () {

            swap();

        }, 2000);
    }, 1000 + delay);
    

    setTimeout(function () {

        $('#subTitle83_1').fadeIn(250);
        playSound(21);
        
    }, 3000 + delay);

    setTimeout(function () {

        $('#subTitle83_1').fadeOut(250);
      
    }, 14000 + delay);

    setTimeout(function () {
        $(currentdiv).fadeOut();
        $(currentdiv).css('background-image', '').css('background-color', 'black').fadeIn();

    }, 14500 + delay);

    setTimeout(function () {
        $('#subTitle85_1').fadeIn(250);

        playSound(22);
    }, 15500 + delay);

    setTimeout(function () {

        $('#subTitle85_1').fadeOut(250);
    }, 20500 + delay);
    setTimeout(function () {

        $('#subTitle85_2').fadeIn(250);

    
    }, 20500 + delay);

    setTimeout(function () {

        $('#subTitle85_2').fadeOut(250);

       
    }, 30500 + delay);

    setTimeout(function () {
        $(currentdiv).fadeOut();
        setGameStatus('.per', '040', 'scene4');
        $("#newContainer").load("scene5child.html");
    }, 31000 + delay);

}