﻿var count = 0;
var start = new Date().getTime();
var matrix = "";
var $divElementRenderAudioTags = $('#soundForAllBrowsers');
var NumberOfAttemps = 0;

var gender = 'male';
if (localStorage.getItem(userId + '_gender') != null) {
    gender = localStorage.getItem(userId + '_gender');
}

var userid = '';

var vignette = 1;
var qno = 0;
var currentTime;
var timestamp;
var attempts;
var gameid = "TS2";
var handlerURL = '../Handlers/SendResults.ashx';


var click_sound_file_url_wav = "";//sounds/timestorm_G02_VG01_click_01.wav";
var click_sound_file_url_ogg = "sounds/timestorm_G02_VG01_click_01.ogg";
var click_sound_file_url = "sounds/timestorm_G02_VG01_click_01.mp3";

var confero_voiceover_33 = 'sounds/confero/timestorm_G02_VG01_voiceover_33.mp3';
var confero_voiceover_33_ogg = 'sounds/confero/timestorm_G02_VG01_voiceover_33.ogg';
var confero_voiceover_33_wav = ' ' ;//sounds/confero/timestorm_G02_VG01_voiceover_33.wav';

var confero_voiceover_34 = 'sounds/'+gender+'/timestorm_G02_VG01_voiceover_34.mp3';
var confero_voiceover_34_ogg = 'sounds/' + gender + '/timestorm_G02_VG01_voiceover_34.ogg';
var confero_voiceover_34_wav = ' ' ;//sounds/confero/timestorm_G02_VG01_voiceover_34.wav';

var confero_voiceover_35 = 'sounds/confero/timestorm_G02_VG01_voiceover_35.mp3';
var confero_voiceover_35_ogg = 'sounds/confero/timestorm_G02_VG01_voiceover_35.ogg';
var confero_voiceover_35_wav = ' ' ;//sounds/confero/timestorm_G02_VG01_voiceover_35.wav';
var confero_voiceover_36 = 'sounds/' + gender + '/timestorm_G02_VG01_voiceover_36.mp3';
var confero_voiceover_36_ogg = 'sounds/' + gender + '/timestorm_G02_VG01_voiceover_36.ogg';
var confero_voiceover_36_wav = ' ' ;//sounds/confero/timestorm_G02_VG01_voiceover_36.wav';
var confero_voiceover_37 = 'sounds/confero/timestorm_G02_VG01_voiceover_37.mp3';
var confero_voiceover_37_ogg = 'sounds/confero/timestorm_G02_VG01_voiceover_37.ogg';
var confero_voiceover_37_wav = ' ' ;//sounds/confero/timestorm_G02_VG01_voiceover_37.wav';

var confero_voiceover_38 = 'sounds/confero/timestorm_G02_VG01_voiceover_38.mp3';
var confero_voiceover_38_ogg = 'sounds/confero/timestorm_G02_VG01_voiceover_38.ogg';
var confero_voiceover_38_wav = ' ' ;//sounds/confero/timestorm_G02_VG01_voiceover_38.wav';

var alarm_sound = 'sounds/timestorm_G02_VG01_soundfx_16.mp3';
var alarm_sound_doubled = 'sounds/timestorm_G02_VG01_soundfx_17.mp3';
var dog_barking = 'sounds/timestorm_G02_VG01_soundfx_18.mp3';

var alarm_sound_ogg = 'sounds/timestorm_G02_VG01_soundfx_16.ogg';
var alarm_sound_doubled_ogg = 'sounds/timestorm_G02_VG01_soundfx_17.ogg';
var dog_barking_ogg = 'sounds/timestorm_G02_VG01_soundfx_18.ogg';

var alarm_sound_wav = '';//sounds/Sounds/timestorm_G02_VG01_soundfx_16.mp3';
var alarm_sound_doubled_wav = ' ' ;//sounds/Sounds/timestorm_G02_VG01_soundfx_17.ogg';
var dog_barking_wav = ' ' ;//sounds/Sounds/timestorm_G02_VG01_soundfx_18.wav';


