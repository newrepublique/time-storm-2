﻿
$(document).ready(function () {
    sessionStorage.setItem(userId + '_lastScene', 'scene3');
    $(nextdiv).fadeOut();
    $(currentdiv).css('backgroundImage', 'url(images/timestorm_G02_VG01_background_03.jpg)').stop(true, true).hide().fadeIn(1000);
    $('#Map').show().css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_02.png)');
    $('#TScompleted,#TSprogress').show();



        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {


            case 'scene3_1': scene3_1();
                break;
            case 'scene3_2': scene3_2();
                $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_04.png)');
                break;
            case 'scene3_3': postQuestion();
                $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_04.png)');
                break;

            default: scene3_1();

        }

});

function scene3_1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    var delay = 500;
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene3_1');
    
    setTimeout(function () {
        
        $('#subTitle39').fadeIn(250);
        playSound(15);
        $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_04.png)');
    }, 1000 + delay );

    setTimeout(function () {

        $('#subTitle39').fadeOut(250);

    }, 7500 + delay);

    setTimeout(function () {

        $('#subTitle40_1').fadeIn(250);
        playSound(16);
        
    }, 8000 + delay );

    setTimeout(function () {

        $('#subTitle40_1').fadeOut(250);
        $('#subTitle40_2').fadeIn(250);


    }, 18000 + delay );

    setTimeout(function () {

        $('#subTitle40_2').fadeOut(250);
    }, 29500 + delay );
    

    setTimeout(function () {
        scene3_2();

    }, 30000 + delay );

    //scene3_2();
  

}


function scene3_2() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    $('#objectHighlight41-1 , #objectHighlight41-2 , #objectHighlight41-3').show();
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene3_2');
    fadeloop('#objectHighlight41-1', 500, 500, 1000);
    fadeloop('#objectHighlight41-2', 500, 500, 1000);
    fadeloop('#objectHighlight41-3', 500, 500, 1000);

    $('#objectHighlight41-1').click(function () {
        playSound('0');
        $('#cover1').hide();
        $('#sceneOver').show();
        $('#propOverlayImage47').fadeIn(250);

        $('.propClose').show();

    });
    $('#objectHighlight41-2').click(function () {
        playSound('0');
        $('#cover2').hide();
        $('#sceneOver').show();
        $('#propOverlayImage44').fadeIn(250);
        $('.propClose').show();
    });
    $('#objectHighlight41-3').click(function () {
        playSound('0');
        $('#cover3').hide();
        $('#sceneOver').show();
        $('#propOverlayImage50').fadeIn(250);
        $('iframe').css('display', 'block');
        $('iframe').attr('src', overlay_video_url);
        $('.propClose').show();
    });

    $('.propClose').click(function () {
        playSound('0');
        count++;
        $('iframe').hide().attr('src', '');
        $('#propOverlayImage44 , #propOverlayImage47 , #propOverlayImage50').fadeOut(250);
        $('#sceneOver').hide();
        
        if (count == 3) {

            setTimeout(function () {
                showQuestion1();
            }, 1000);
        }
    });
   
}

function showQuestion1() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector1 = $('#q1radio2');

    var targetClickedSelector1, targetClickedId, result;
    $('#propOverlayQuestion53,#QueAnsDiv , .QueAnsTitle, #sceneOver').fadeIn(250);
    $('#QueAns ul li span').click(function (event) {
        
        if($(this).attr('readonly') != 'readonly') {

            playSound('0');

            targetClickedId = event.target.parentNode.id;
            targetClickedSelector1 = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');
            
            result = CalculateAttemptsAndAMtrix(correctAnswerSelector1, targetClickedSelector1);

                if (result != undefined && result.length == 2) {
                    
                    sendResults(1, result[0], result[1]);

                    $('#QueAns ul li span').attr('readonly', 'readonly');

                    setTimeout(function () {
                        $('#propOverlayQuestion53 , #leftCol , #rightCol , #QueAnsDiv').fadeOut(250);
                    $('.QueAnsTitle').hide();
                    $('#sceneOver').hide();
                   
                    showQuestion2();

                    }, 1000);
                }
            
        }
    });
}

function showQuestion2() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector2 = $('#q2radio2');
    var targetClickedSelector2, targetClickedId, result1;
    $('#propOverlayQuestion56 , .QueAnsTitle1 , #QueAnsDiv1').fadeIn(250);
    $('#sceneOver').show();
    $('#QueAnsDiv1 ul li span').click(function (event) {
       
        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector2 = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');
            

            result1 = CalculateAttemptsAndAMtrix(correctAnswerSelector2, targetClickedSelector2);

           
                if (result1 != undefined && result1.length == 2) {
                    sendResults(2, result1[0], result1[1]);
                    $('#QueAnsDiv1 ul li span').attr('readonly', 'readonly');
                    setTimeout(function () {
                    
                    $('#sceneOver').hide();
                    $('#propOverlayQuestion56 , .QueAnsTitle1 , #QueAnsDiv1').fadeOut(250);
                   
                    postQuestion();
                    }, 1000);
                }
           
        }
    });
}

function sendResults(quesNo,NumberOfAttemps,Matrix) {
    userid = getQueryStringValue("userId");           

    qno = quesNo;
    currentTime = new Date();
    timestamp = formatDate(currentTime);
    attempts = NumberOfAttemps;
    matrix = Matrix;
    

    setTimeout(function () {
        sendScore(userid, vignette, qno, timestamp, attempts, matrix, gameid, handlerURL);
    }, 1000);

}

function postQuestion() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    var delay = 500;
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene3_3');

    setTimeout(function () {
        updatePhoneTxt('You have completed the Interpretation test.','listIcon1');

    }, delay);
    setTimeout(function () {
        
        $('#subTitle62_1').fadeIn(250);
        playSound(17);
    }, 500 + delay);

    setTimeout(function () {

        $('#subTitle62_1').fadeOut(250);
    
        setGameStatus('.per', '020', 'v1');
        $(currentdiv).fadeOut(250);
    }, 14000 + delay);



    setTimeout(function () {

        playSound(55);

    }, 14500 + delay);

    setTimeout(function () {

        
        $(currentdiv).css('backgroundImage', '').css('background-color', 'black').fadeIn();
        playSound(18);
        $('#subTitle67_1').fadeIn(250);

    }, 16500 + delay);
    setTimeout(function () {

        $('#subTitle67_1').fadeOut(250);
    
        
    }, 24000 + delay);

    setTimeout(function () {
        $(currentdiv).fadeOut(250);
        $("#newContainer").load("scene4child.html");
    }, 24500 + delay);
}