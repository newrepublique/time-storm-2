﻿$(document).ready(function () {
    $('#TSprogress,#TScompleted').show();
    sessionStorage.setItem(userId + '_lastScene', 'scene9');    //save game
    
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_14.png)');  //from scene 8


    
        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {


            case 'scene9_2': scene9_2();
                $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_13.png)');//from scene9_1
                $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG01_background_15.jpg)').stop(true, true).hide().fadeIn(1000);
                break;


            default: scene9_1(); $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG01_background_14.jpg)').stop(true, true).hide().fadeIn(1000);

        }
   


    
});

function scene9_1() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene9_1');
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    setTimeout(function () {

        $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_12.png)');      

    }, delay9);


    setTimeout(function () {
        playSound(39);
        $('#subTitle164').fadeIn(250);

    }, 1000 + delay9);

    setTimeout(function () {

        $('#subTitle164').fadeOut(250);

    }, 3000 + delay9);

    setTimeout(function () {
        playSound(66);
    }, 4000 + delay9);


    //crossfades
    setTimeout(function () {

      
            $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG01_background_14.jpg)').fadeOut(1500);
            setTimeout(function () {
                $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG01_background_15.jpg)').fadeIn(2000);
            }, 0);
            setTimeout(function () {
                swap();
            }, 2000);
     
       
    }, 12000 + delay9);


    //phonemap_13 no id
    setTimeout(function () {

        $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_13.png)');
      

    }, 14000 + delay9);


    //whats going on
    setTimeout(function () {
        playSound(40);
        $('#subTitle168').fadeIn(250);

    }, 15000 + delay9);

    setTimeout(function () {

        $('#subTitle168').fadeOut(250);

    }, 18000 + delay9);

    //trainee
    setTimeout(function () {
        playSound(41);
        $('#subTitle169').fadeIn(250);

    }, 19000 + delay9);

    setTimeout(function () {

        $('#subTitle169').fadeOut(250);

    }, 24000 + delay9);


    //crash again
   
    setTimeout(function () {
        playSound(67);

    }, 25000 + delay9);

    //destroyed orange
    setTimeout(function () {
        playSound(42);
        $('#subTitle171').fadeIn(250);

    }, 28500 + delay9);

    setTimeout(function () {

        $('#subTitle171').fadeOut(250);

    }, 32500 + delay9);

    //destroyed white
    setTimeout(function () {
        playSound(43);
        $('#subTitle172').fadeIn(250);

    }, 34000 + delay9);

    setTimeout(function () {

        $('#subTitle172').fadeOut(250);
    }, 35500 + delay9);


    //intelligent structure
    setTimeout(function () {
        playSound(44);
        $('#subTitle173_1').fadeIn(250);

    }, 36500 + delay9);

    setTimeout(function () {

        $('#subTitle173_1').fadeOut(250);
        $('#subTitle173_2').fadeIn(250);

    }, 44000 + delay9);

    setTimeout(function () {

        $('#subTitle173_2').fadeOut(250);

    }, 52000 + delay9);


    setTimeout(function () {

        scene9_2();

    }, 53000 + delay9);
}



function scene9_2() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene9_2');
    
    //when things do
    setTimeout(function () {
        playSound(45);
        $('#subTitle174').fadeIn(250);

    }, 500);

    setTimeout(function () {

        $('#subTitle174').fadeOut(250);
    }, 1000 + delay9);
   
    
    //orange replacement dialgue
    setTimeout(function () {
        playSound(46);
        $('#subTitle175_1').fadeIn(250);

    }, 2000+delay9);

    setTimeout(function () {

        $('#subTitle175_1').fadeOut(250);
        $('#subTitle175_2').fadeIn(250);

    }, 9000 + delay9);

   

    setTimeout(function () {

        $('#subTitle175_2').fadeOut(250);
    }, 25500 + delay9);


    //filling
    setTimeout(function () {
        playSound(47);
        $('#subTitle176').fadeIn(250);

    }, 26500 + delay9); //25500

    setTimeout(function () {

        $('#subTitle176').fadeOut(250);
    }, 29000 + delay9); //28000



    //learning fast
    setTimeout(function () {
        playSound(48);
        $('#subTitle177').fadeIn(250);

    }, 30000 + delay9); //29000

    setTimeout(function () {

        $('#subTitle177').fadeOut(250);
        setGameStatus('.per', '100', 'v1');
        
    }, 35500 + delay9); //34500

    setTimeout(function () {
        $("#TSscene").fadeOut(500);
        sessionStorage.setItem(userId + '_lastScene', 'scene1');    //save game
        $('.per').html('000');
        sessionStorage.setItem('status-line-li', ' ');
        localStorage.setItem('phonetext', '');
        localStorage.setItem(userId + '_phonetext', ' ');
        currentvignette = 'v2';                                             //change to v2 after deploying rest of the chapters.
        localStorage.setItem(userId + '_vignette', currentvignette);
        sessionStorage.setItem(userId + '_lastSceneSection', '');
    }, 36500 + delay9);

    setTimeout(function () {
        sendData();
    }, 37500 + delay9);

   
    setTimeout(function () {
        $("#endScreen").fadeIn(250);
    }, 38500 + delay9);

    setTimeout(function () {
        $("#endScreen").fadeOut(250);
        $("#preloaderGif").hide();
        window.location.href = '../vignette2/index.html';
        //add link to next vignette after its deployment.
    }, 41500 + delay9); 
}