﻿var $divElementRenderAudioTags = $('#soundForAllBrowsers');

var gender = 'male';

var delay9 = 1000;
//var muteSound = false;

if (localStorage.getItem(userId + '_gender') != null) {
    gender = localStorage.getItem(userId + '_gender');
}


var voiceover_39_wav = ''; //2
var voiceover_39_mp3 = 'sounds/confero/timestorm_G02_VG01_voiceover_39.mp3';
var voiceover_39_ogg = 'sounds/confero/timestorm_G02_VG01_voiceover_39.ogg';

var crash_wav = ''; //3
var crash_mp3 = 'sounds/timestorm_G02_VG01_soundfx_19.mp3';
var crash_ogg = 'sounds/timestorm_G02_VG01_soundfx_19.ogg';

var voiceover_40_wav = ''; //2
var voiceover_40_mp3 = 'sounds/' + gender + '/timestorm_G02_VG01_voiceover_40.mp3';
var voiceover_40_ogg = 'sounds/' + gender + '/timestorm_G02_VG01_voiceover_40.ogg';

var voiceover_41_wav = ''; //2
var voiceover_41_mp3 = 'sounds/confero/timestorm_G02_VG01_voiceover_41.mp3';
var voiceover_41_ogg = 'sounds/confero/timestorm_G02_VG01_voiceover_41.ogg';

var distantcrash_wav = ''; //3
var distantcrash_mp3 = 'sounds/timestorm_G02_VG01_soundfx_20.mp3';
var distantcrash_ogg = 'sounds/timestorm_G02_VG01_soundfx_20.ogg';

var voiceover_42_wav = ''; //2
var voiceover_42_mp3 = 'sounds/confero/timestorm_G02_VG01_voiceover_42.mp3';
var voiceover_42_ogg = 'sounds/confero/timestorm_G02_VG01_voiceover_42.ogg';

var voiceover_43_wav = ''; //2
var voiceover_43_mp3 = 'sounds/' + gender + '/timestorm_G02_VG01_voiceover_43.mp3';
var voiceover_43_ogg = 'sounds/' + gender + '/timestorm_G02_VG01_voiceover_43.ogg';

var voiceover_44_wav = ''; //2
var voiceover_44_mp3 = 'sounds/confero/timestorm_G02_VG01_voiceover_44.mp3';
var voiceover_44_ogg = 'sounds/confero/timestorm_G02_VG01_voiceover_44.ogg';

var voiceover_45_wav = ''; //2
var voiceover_45_mp3 = 'sounds/' + gender + '/timestorm_G02_VG01_voiceover_45.mp3';
var voiceover_45_ogg = 'sounds/' + gender + '/timestorm_G02_VG01_voiceover_45.ogg';

var voiceover_46_wav = ''; //2
var voiceover_46_mp3 = 'sounds/confero/timestorm_G02_VG01_voiceover_46.mp3';
var voiceover_46_ogg = 'sounds/confero/timestorm_G02_VG01_voiceover_46.ogg';

var voiceover_47_wav = ''; //2
var voiceover_47_mp3 = 'sounds/' + gender + '/timestorm_G02_VG01_voiceover_47.mp3';
var voiceover_47_ogg = 'sounds/' + gender + '/timestorm_G02_VG01_voiceover_47.ogg';

var voiceover_48_wav = ''; //2
var voiceover_48_mp3 = 'sounds/confero/timestorm_G02_VG01_voiceover_48.mp3';
var voiceover_48_ogg = 'sounds/confero/timestorm_G02_VG01_voiceover_48.ogg';

