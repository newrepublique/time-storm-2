﻿var $divElementRenderAudioTags = $('#soundForAllBrowsers');

var gender = 'male';
if (localStorage.getItem(userId + '_gender') != null) {
    gender = localStorage.getItem(userId + '_gender');
}

var delay7 = 1000;
//var muteSound = false;

var voiceover_28_wav = ''; //2
var voiceover_28_mp3 = 'sounds/confero/timestorm_G02_VG01_voiceover_28.mp3';
var voiceover_28_ogg = 'sounds/confero/timestorm_G02_VG01_voiceover_28.ogg';

var whoosh_wav = ''; //3
var whoosh_mp3 = 'sounds/timestorm_G02_VG01_soundfx_14.mp3';
var whoosh_ogg = 'sounds/timestorm_G02_VG01_soundfx_14.ogg';

var voiceover_31_wav = ''; //2
var voiceover_31_mp3 = 'sounds/' + gender + '/timestorm_G02_VG01_voiceover_31.mp3';
var voiceover_31_ogg = 'sounds/' + gender + '/timestorm_G02_VG01_voiceover_31.ogg';

var voiceover_32_wav = ''; //2
var voiceover_32_mp3 = 'sounds/confero/timestorm_G02_VG01_voiceover_32.mp3';
var voiceover_32_ogg = 'sounds/confero/timestorm_G02_VG01_voiceover_32.ogg';

