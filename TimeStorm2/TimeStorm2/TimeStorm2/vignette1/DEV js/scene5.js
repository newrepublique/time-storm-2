﻿


$(document).ready(function () {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastScene', 'scene5');
    
    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG01_background_06.jpg)').stop(true, true).hide().fadeIn(1000);
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_05.png)');
    setTimeout(function () {
        $('#TScompleted,#TSprogress').show();
    }, 1000);

    setTimeout(function () {
        updatePhoneTxt('You’ve found the Immigration section.', 'listIcon1');
    }, 2000);


    setTimeout(function () {


        var section = localStorage.getItem(userId + '_lastSceneSection');
        switch (section) {

            case 'scene5_1': scene5_1();
                break;
            case 'scene5_2': showQuestion3();
                break;

            case 'scene5_3': postQues();
                break;

            default: scene5_1();

        }


    }, 3000);


});



function scene5_1() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene5_1');
    
    setTimeout(function () {

        $('#subTitle89_1').fadeIn(250);
        playSound(23);
        
    }, 1000);

    setTimeout(function () {

        $('#subTitle89_1').fadeOut(250);
        $('#subTitle89_2').fadeIn(250);
    }, 9000);

    setTimeout(function () {

        $('#subTitle89_2').fadeOut(250);
    }, 18000);



    setTimeout(function () {

        showQuestion3();
    }, 19000);

}
function showQuestion3() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene5_2');
    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector3 = $('#q3radio4');

    var targetClickedSelector3, targetClickedId, result;
    $('#propOverlayQuestion90 , #QueAnsDiv1').fadeIn(250);
    $('#sceneOver').show();
   // $('.QueAnsTitle1').show();
    $('#QueAnsDiv1 ul li span').click(function (event) {

        

        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');
            
            targetClickedId = event.target.parentNode.id;
            targetClickedSelector3= $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector3, targetClickedSelector3);
//optional
           
                if (result != undefined && result.length == 2) {
                    sendResults(3, result[0], result[1]);
                    $('#QueAnsDiv1 ul li span').attr('readonly', 'readonly');
                    setTimeout(function () {
                    
                        $('#propOverlayQuestion90 , #QueAnsDiv1').fadeOut(500);
                        $('#sceneOver').hide();
                        result = '';
                        showQuestion4();

                    }, 1000);
                }
            
        }
    });
}

function showQuestion4() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector4 = $('#q4radio2');

    var targetClickedSelector4, targetClickedId, result;
    $('#propOverlayQuestion93 , #QueAnsDiv2').fadeIn(500);
    $('#sceneOver').show();
    $('#QueAnsDiv2 ul li span').click(function (event) {
        

        if ($(this).attr('readonly') != 'readonly') {
            playSound('0');

            targetClickedId = event.target.parentNode.id;
            targetClickedSelector4 = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector4, targetClickedSelector4);
            //optional
            
                if (result != undefined && result.length == 2) {

                    sendResults(4, result[0], result[1]);
                    $('#QueAnsDiv2 ul li span').attr('readonly', 'readonly');

                    setTimeout(function () {
                        $('#propOverlayQuestion93 , #QueAnsDiv2').fadeOut(500);
                        $('#sceneOver').hide();
                        result = '';
                        showQuestion5();
                    }, 1000);

                }
           
        }
    });
}

function showQuestion5() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector5 = $('#q5radio3');

    var targetClickedSelector5, targetClickedId, result;
    $('#propOverlayQuestion96 , #QueAnsDiv3').fadeIn(500);
    $('#sceneOver').show();
    $('#QueAnsDiv3 ul li span').click(function (event) {

        if ($(this).attr('readonly') != 'readonly') {

            playSound('0');

            targetClickedId = event.target.parentNode.id;
            targetClickedSelector5 = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector5, targetClickedSelector5);
            //optional
            
                if (result != undefined && result.length == 2) {

                    sendResults(5, result[0], result[1]);
                        $('#QueAnsDiv3 ul li span').attr('readonly', 'readonly');
                        setTimeout(function () {
                            $('#propOverlayQuestion96 , #QueAnsDiv3').fadeOut(500);
                        $('#sceneOver').hide();
                        result = '';
                        showQuestion6();
                    }, 1000);

                }
           
        }
    });
}

function showQuestion6() {

    start = new Date().getTime();
    matrix = "";
    NumberOfAttemps = 0;
    var correctAnswerSelector6 = $('#q6radio3');

    var targetClickedSelector6, targetClickedId, result;
    $('#propOverlayQuestion99 , #QueAnsDiv4').fadeIn(500);
    $('#sceneOver').show();
    $('#QueAnsDiv4 ul li span').click(function (event) {


        if ($(this).attr('readonly') != 'readonly') {

            playSound('0');

            targetClickedId = event.target.parentNode.id;
            targetClickedSelector6 = $("#" + targetClickedId);
            $(this).css('background-image', 'url(images/iconAns.png)');

            result = CalculateAttemptsAndAMtrix(correctAnswerSelector6, targetClickedSelector6);
            //optional
            
                if (result != undefined && result.length == 2) {

                    sendResults(6, result[0], result[1]);
                    $('#QueAnsDiv4 ul li span').attr('readonly', 'readonly');

                    setTimeout(function () {
                        $('#propOverlayQuestion99 , #QueAnsDiv4').fadeOut(500);
                        $('#sceneOver').hide();
                        result = '';
                        
                        postQues();
                    }, 1000);
                }
            
        }
    });
}
function sendResults(quesNo, NumberOfAttemps, Matrix) {
    userid = getQueryStringValue("userId");

    
    qno = quesNo;

    currentTime = new Date();
    timestamp = formatDate(currentTime);

    attempts = NumberOfAttemps;
    matrix = Matrix;
    
    setTimeout(function () {
        sendScore(userid, vignette, qno, timestamp, attempts, matrix, gameid, handlerURL);
    }, 1000);

}

function postQues() {
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene5_3');
    delay = 1000;

    setTimeout(function () {

        $('#subTitle105_1').fadeIn(250);
        playSound(24);
        
    }, 500 + delay);

    setTimeout(function () {

        $('#subTitle105_1').fadeOut(250);
    }, 12500 + delay);

    setTimeout(function () {

        $('#subTitle105_2').fadeIn(250);


    }, 12500 + delay);

    setTimeout(function () {

        $('#subTitle105_2').fadeOut(250);
    }, 28500 + delay);

    

    setTimeout(function () {

        $(currentdiv).fadeOut();
        $(currentdiv).css('background-image', '').css('background-color', 'black').fadeIn();

    }, 29000 + delay);

    setTimeout(function () {
        $('#subTitle107').fadeIn(250);


    }, 30000 + delay);

    setTimeout(function () {

        $('#subTitle107').fadeOut(250);
        $("#newContainer").load("scene6child.html");
    }, 35000 + delay);

}