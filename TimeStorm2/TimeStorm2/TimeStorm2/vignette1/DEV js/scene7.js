﻿$(document).ready(function () {
    $('#TSprogress,#TScompleted').show();
    sessionStorage.setItem(userId + '_lastScene', 'scene7');    //save game
    $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG01_background_08.jpg)').stop(true, true).hide().fadeIn(1000);
    $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_08.png)');  //from scene6_next


    setTimeout(function () {
        scene7_1();
    }, 1000);

});

function scene7_1() {
    sessionStorage.setItem(userId + '_lastSceneSection', 'scene7_1');
    localStorage.setItem('phonetext', sessionStorage.getItem('status-line-li'));
    setTimeout(function () {

        $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_09.png)');
       
    }, delay7);


    //stop
    setTimeout(function () {
        playSound(28);
        $('#subTitle125').fadeIn(250);
    }, 1000 + delay7);
    setTimeout(function () {
     
        $('#subTitle125').fadeOut(250);
        
    }, 3000 + delay7);

    //whoosh sound
    setTimeout(function () {
        playSound(61);
    }, 4000 + delay7);


    //Crossfades1
    setTimeout(function () {
        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG01_background_08.jpg)').fadeOut(1500);
     
        setTimeout(function () {
            $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG01_background_10.jpg)').fadeIn(2000);
        }, 0);
        setTimeout(function () {
            swap();
        }, 2000);
    }, 6000 + delay7);


    setTimeout(function () {

      
        setGameStatus('.per', '070', 'v1');
    }, 10000 + delay7);



    ////Crossfades2
    setTimeout(function () {
        
        $(currentdiv).css('background-image', 'url(./images/timestorm_G02_VG01_background_10.jpg)').fadeOut(1500);
            setTimeout(function () {
                $(nextdiv).hide().css('background-image', 'url(./images/timestorm_G02_VG01_background_11.jpg)').fadeIn(2000);
            }, 0);
            setTimeout(function () {
                swap();
            }, 2000);

    }, 11000 + delay7);


    //who was that
    setTimeout(function () {
        playSound(31);
        $('#subTitle130').fadeIn(500);
    }, 13000 + delay7);
    setTimeout(function () {

        $('#subTitle130').fadeOut(500);
        
    }, 14500 + delay7);


    //suspicions
    setTimeout(function () {
        playSound(32);

        $('#subTitle131').fadeIn(500);
    }, 15500 + delay7);
    setTimeout(function () {

        $('#subTitle131').fadeOut(500);
    }, 20000 + delay7);


    //phonemap
    setTimeout(function () {
       
        $('#Map').css('background-image', 'url(./images/timestorm_G02_VG01_phonemap_10.png)');
    }, 20500 + delay7);

    setTimeout(function () {
        $('#newContainer').load('scene8child.html');
    }, 21500);
}